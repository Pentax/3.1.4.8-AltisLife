class DefaultEventhandlers;
class CfgPatches 
{
	class life_server
	{
		units[] = {"C_man_1"};
		weapons[] = {};
		requiredAddons[] = {"A3_Data_F","A3_Soft_F","A3_Soft_F_Offroad_01","A3_Characters_F"};
		fileName = "life_server.pbo";
		author[]= {"TAW_Tonic"}; 
	};
};

class CfgFunctions
{
	class BIS_Overwrite
	{
		tag = "BIS";
		class MP
		{
			file = "\life_server\Functions\MP";
			class initMultiplayer{};
			class call{};
			class spawn{};
			class execFSM{};
			class execVM{};
			class execRemote{};
			class addScore{};
			class setRespawnDelay{};
			class onPlayerConnected{};
			class initPlayable{};
			class missionTimeLeft{};
		};
	};
	
	class MySQL_Database
	{
		tag = "DB";
		class MySQL
		{
			file = "\life_server\Functions\MySQL";
			class numberSafe {};
			class mresArray {};
			class queryRequest{};
			class asyncCall{};
			class insertRequest{};
			class updateRequest{};
			class mresToArray {};
			class insertVehicle {};
			class bool{};
			class mresString {};
			class updatePartial {};
		};
	};
	
	class Life_System
	{
		tag = "life";
		class Wanted_Sys
		{
			file = "\life_server\Functions\WantedSystem";
			class wantedAdd {};
			class wantedBounty {};
			class wantedCrimes {};
			class wantedDisplay {};
			class wantedFetch {};
			class wantedPardon {};
			class payCopDeathOff {};
			class wantedPerson {};
			class wantedProfUpdate {};
			//class wantedPunish {};
			class wantedRemove {};
			//class wantedTicket {};
		};
		
		class Jail_Sys
		{
			file = "\life_server\Functions\Jail";
			class jailSys {};
		};
		
		
		class Client_Code
		{
			file = "\life_server\Functions\Client";
		};
		class Safe_Code
		{
			file = "\life_server\Functions\Systems";
			class initSafeCode {postInit=1;};
		};
		class Config
		{
			file = "\life_server\Functions\Config";
			//Master Config
			class configuration {};
			//Shop Config
			class weaponShopCfg {};
			class virt_shops {};
			class clothing_cop {};
			class clothing_bruce {};
			class clothing_reb {};
			class clothing_dive {};
			class clothing_donator {};
			class clothing_bountyHunter {};

			//License Config
			class vehShopLicenses {};
			class licenseType {};

			//Vehicle Config
			class vehicleColorCfg {};
			class vehicleColorStr {};
			class vehicleListCfg {};
			class vehicleAnimate {};
			class vehicleWeightCfg {};
			class impoundPrice {};

			//Other Configs
			class eatFood {};
			class itemWeight {};
			class taxRate {};
			class shopMenus {};
			
		};
		class Actions
		{
			file = "\life_server\Functions\Actions";
			class replaceOrgan {};
			class selfbloodbag{};
			class organTransplantKit{};
			class scalpel {};
			class bloodbag {};
			class spawnGoldLoop {};
		};
	};
	
	class TON_System
	{
		tag = "TON";
		class Systems
		{
			file = "\life_server\Functions\Systems";
			class managesc {};
			class cleanup {};
			class huntingZone {};
			class getID {};
			class vehicleCreate {};
			class vehicleDead {};
			class spawnVehicle {};
			class getVehicles {};
			class vehicleStore {};
			class vehicleDelete {};
			class spikeStrip {};
			class logIt {};
			class federalUpdate {};
			class chopShopSell {};
			class clientDisconnect {};
			class cleanupRequest {};
			class robShops {};
			class shopState {};
			class goldUpdate {};
			class handleMessages {};
			class msgRequest {};
			class setObjVar {};
			class keyManagement {};
			class spawnJail {};
		};
		
		
		class Gangs
		{
			file = "\life_server\Functions\Gangs";
			class insertGang {};
			class queryPlayerGang {};
			class removeGang {};
			class updateGang {};
		};
		
	};
};

class CfgVehicles
{
	class Car_F;
	class CAManBase;
	class Civilian;
	class Civilian_F : Civilian
	{
		class EventHandlers;
	};
	
	class C_man_1 : Civilian_F
	{
		class EventHandlers: EventHandlers
		{
			init = "(_this select 0) execVM ""\life_server\fix_headgear.sqf""";
		};
	};
};