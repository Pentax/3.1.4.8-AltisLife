private["_name","_bounty","_name2","_bounty2","_text","_text2","_ret","_list","_result","_queryResult","_uid"];
_ret = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
if(isNull _ret) exitWith {};
_result = format["SELECT wantedName, wantedBounty, wantedID FROM wanted WHERE active='1' ORDER BY wantedBounty DESC LIMIT 5"];
waitUntil{!DB_Async_Active};
_tickTime = diag_tickTime;
_queryResult = [_result,2,true] call DB_fnc_asyncCall;
//_queryResult example: [["charlie",520000,[]],["Commodore",185000,[]]]

{
	_name = format ["<t align='left'><t color = '#FFCC00'>%1 - </t></t>",_x select 0];
	_bounty = format ["<t align='right'><t color = '#FFFFFF'>$%1</t></t>",[_x select 1]call life_fnc_numberText];
	if (isNil "_text") then {
		_text = _name + " " + _bounty;
	} else {
		_text = _text + "<br/>" + _name + " " + _bounty;
	};
} forEach _queryResult;


{
	_uid = _x select 2;
	if([_uid] call life_fnc_isUIDActive) then
	{
		_name2 = format ["<t align='left'><t color = '#FFCC00'>%1 - </t></t>",_x select 0];
		_bounty2 = format ["<t align='right'><t color = '#FFFFFF'>$%1</t></t>",[_x select 1]call life_fnc_numberText];
		if (isNil "_text2") then {
			_text2 = _name2 + " " + _bounty2;
		} else {
			_text2 = _text2 + "<br/>" + _name2 + " " + _bounty2;
		};
	};
} forEach _queryResult;

if (isNil "_text") then {_text = "Nobody is wanted, better commit some crimes";};
if (isNil "_text2") then {_text2 = "No one online is wanted at this time";};

diag_log "------------- Client Query Request wantedDisplay -------------";
diag_log format["QUERY: %1",_result];
diag_log format["Req By: %1",name _ret];
diag_log format["Time to complete: %1 (in seconds)",(diag_tickTime - _tickTime)];
diag_log "------------------------------------------------";

_ret = owner _ret;

[[5,format["<t size = '3'><t color='#FFFF00'>Outlaws</t></t><br/><t size = '1.65'><t color = '#00FF00'>Most Wanted Online</t></t><br/>%1 <br/><br/><br/>
			<t size = '1.75'><t color = '#FF0000'>Most Wanted </t></t><br/>%2",
_text2, _text]],"life_fnc_broadcast",_ret,false] spawn life_fnc_MP; 