/*
	File: fn_wantedPardon.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Unwants / pardons a person from the wanted list.
*/
private["_uid","_query","_queryRequest"];
_uid = [_this,0,"",[""]] call BIS_fnc_param;
if(_uid == "") exitWith {};

_query = format["UPDATE wanted SET active = 0, wantedCrimes = '[]', wantedBounty = 0 WHERE wantedID='%1'",_uid];
waitUntil{!DB_Async_Active};
[_query,2] call DB_fnc_asyncCall;