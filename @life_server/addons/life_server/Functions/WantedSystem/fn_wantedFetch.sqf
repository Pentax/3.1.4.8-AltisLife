/*
	File: fn_wantedFetch.sqf
	Author: Bryan "Tonic" Boardwine"
	Database Persistence By: ColinM
	
	Description:
	Displays wanted list information sent from the server.
*/
//current working lines^^
private["_ret","_list","_result","_queryResult","_units"];
_ret = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
if(isNull _ret) exitWith {};
_ret = owner _ret;

_list = [];
_units = [];
_tickTime = diag_tickTime;
{if((side _x) == east) then {_units pushBack (getPlayerUID _x)};} foreach playableUnits;

{
	_result = format["SELECT wantedID, wantedName FROM wanted WHERE active='1' AND wantedID='%1'",_x];
	waitUntil{!DB_Async_Active};
	_queryResult = [_result,2,true] call DB_fnc_asyncCall;
	_list pushBack (_queryResult select 0);
} forEach _units;

if(count _list == 0) exitWith {[[_list],"life_fnc_wantedList",_ret,false] spawn life_fnc_MP;};

diag_log "------------- Client Query Request -------------";
diag_log format["QUERY: %1",_result];
diag_log format["Time to complete: %1 (in seconds)",(diag_tickTime - _tickTime)];
diag_log format["Result: %1",_list];
diag_log "------------------------------------------------";

[[_list],"life_fnc_wantedList",_ret,false] spawn life_fnc_MP;