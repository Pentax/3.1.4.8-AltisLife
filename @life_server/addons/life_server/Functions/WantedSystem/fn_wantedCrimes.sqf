/*
	File: fn_wantedCrimes.sqf
	Author: ColinM
	
	Description:
	Grabs a list of crimes committed by a person.
*/
private["_display","_criminal","_tab","_queryResult","_result","_ret","_crimesDb","_crimesArr","_type"];
disableSerialization;
_ret = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
_criminal = [_this,1,[],[]] call BIS_fnc_param;

_result = format["SELECT wantedCrimes, wantedBounty FROM wanted WHERE active='1' AND wantedID='%1'",(_criminal select 0)];
waitUntil{!DB_Async_Active};
_tickTime = diag_tickTime;
_queryResult = [_result,2] call DB_fnc_asyncCall;

_ret = owner _ret;
_crimesArr = [];

_crimesDB = [(_queryResult select 0)] call DB_fnc_mresToArray;
if(typeName _crimesDb == "STRING") then {_crimesDb = call compile _crimesDb;};
//if(typeName _crimesDb == "STRING") then {_crimesDb = call compile format["%1", _crimesDb];};
_queryResult set[0,_crimesDb];
_type = _queryResult select 0;
{
	switch(_x) do
	{
		
		case "187V": {_x = "Vehicular Manslaughter"};
		case "187": {_x = "Manslaughter"};
		case "901": {_x = "Escaping Jail"};
		case "261": {_x = "Rape"};
		case "261A": {_x = "Attempted Rape"};
		case "215": {_x = "Attempted Auto Theft"};
		case "213": {_x = "Use of illegal explosives"};
		case "211": {_x = "Armed Robbery"};
		case "211G": {_x = "Gas Station Robbery"};
		case "211B": {_x = "Bank Robbery",};
		case "207": {_x = "Kidnapping"};
		case "207A": {_x = "Attempted Kidnapping"};
		case "487": {_x = "Grand Theft"};
		case "488": {_x = "Petty Theft"};
		case "480": {_x = "Hit and run"};
		case "481": {_x = "Drug Possession"};
		case "482": {_x = "Intent to distribute"};
		case "483": {_x = "Drug Trafficking"};
		case "919": {_x = "Organ Theft"};
		case "919A": {_x = "Att Organ Theft"};
		case "1014": {_x = "Breaking/Entering"};
		case "390": {_x = "Public Intoxication"};
		case "666": {_x = "Tax Evasion"};
		case "120P": {_x = "Reckless Driving 120+"};
		case "120S": {_x = "Speeding Over Limit"};
		case "120H": {_x = "Habitual Speeding"};
		case "120WL": {_x = "Driving W/O License"};
		case "120FS": {_x = "Felony Speeding AOS"};
		
		case "1": {_x = "Driving without Lights"};
		case "2": {_x = "Driving without License"};
		case "3": {_x = "Driving over the Speed"};
		case "4": {_x = "Reckless Driving"};
		case "5": {_x = "Driving Stolen Vehicle"};
		case "6": {_x = "Hit and Run"};
		case "7": {_x = "Attempted Murder"};
		case "8": {_x = "Terrorism"};
		case "9": {_x = "Obstruction of Justice"};
		case "10": {_x = "Loitering"};
		case "11": {_x = "Misd. Accessory->Crime"};
		case "12": {_x = "Felony Accessory->Crime"};
		case "13": {_x = "Police Evasion"};
		case "14": {_x = "Falsifying Info"};
	};
		_crimesArr pushBack _x;
}forEach _type;
_queryResult set[0,_crimesArr];

diag_log "------------- Client Query Request -------------";
diag_log format["QUERY: %1",_result];
diag_log format["Time to complete: %1 (in seconds)",(diag_tickTime - _tickTime)];
diag_log format["Result: %1",_queryResult];
diag_log "------------------------------------------------";

[[_queryResult],"life_fnc_wantedInfo",_ret,false] spawn life_fnc_MP;