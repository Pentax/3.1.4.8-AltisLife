/*
	File: fn_wantedProfUpdate.sqf
	Author: [midgetgrimm]
	Persistence by: ColinM
	Description:
	Updates name of player if they change profiles
*/
private["_uid","_query","_tickTime","_wantedCheck","_wantedQuery"];

_uid = [_this,0,"",[""]] call BIS_fnc_param;
//Bad data check
if(_uid == "") exitWith {};
_tickTime = diag_tickTime;

_wantedCheck = format["SELECT wantedName FROM wanted WHERE active='1' AND wantedID='%1'",_uid];
waitUntil{!DB_Async_Active};
_wantedQuery = [_wantedCheck,2] call DB_fnc_asyncCall;
//diag_log format["Wanted Query: %1 - Killer Name: %2",_wantedQuery,(_wantedQuery select 0)];
if(count _wantedQuery > 0) then {

	diag_log "Wanted person killed cop, family payment recieved";
	life_OnBankCash = life_OnBankCash + 15000;
	
} else {

};
