private["_civ","_bountyhunter","_id","_third","_result","_queryResult"];
_uid = [_this,0,"",[""]] call BIS_fnc_param;
_civ = [_this,1,Objnull,[Objnull]] call BIS_fnc_param;
_bountyhunter = [_this,2,Objnull,[Objnull]] call BIS_fnc_param;
_third = [_this,3,false,[false]] call BIS_fnc_param;
if(isNull _civ OR isNull _bountyhunter) exitWith {};
_result = format["SELECT wantedID, wantedName, wantedCrimes, wantedBounty FROM wanted WHERE active='1' AND wantedID='%1'",_uid];
waitUntil{!DB_Async_Active};
_queryResult = [_result,2] call DB_fnc_asyncCall;

if(count _queryResult != 0) then
{
	_amount = _queryResult select 3;
	if(_third) then
	{
		[[((_amount) / 3),_amount],"life_fnc_bountyHunterReceive",(owner _bountyhunter),false] spawn life_fnc_MP;
	}
		else
	{
		[[_amount,_amount],"life_fnc_bountyHunterReceive",(owner _bountyhunter),false] spawn life_fnc_MP;
	};
};