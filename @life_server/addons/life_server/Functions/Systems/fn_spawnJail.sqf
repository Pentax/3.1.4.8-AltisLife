diag_log "spawnjail file startrun";
private["_prison_wallholder","_prisonHolder"];
_prison_wallholder = nearestObject[[2799.9119,11569.719,0],"Land_BagFence_Corner_F"];
_prisonHolder = nearestObject[[16758.494,13633.904,0],"Land_Noticeboard_F"];

prison_safe allowDamage false;
prison_safe1 allowDamage false;
prison_safe2 allowDamage false;
_prison_wallholder allowDamage false;
_prisonHolder allowDamage false;
prison_safe attachTo[_prison_wallholder,[3.9,0.85,1.75]]; 
prison_safe1 attachTo [_prison_wallholder,[3.7,1.8,0.57]];
prison_safe2 attachTo [_prisonHolder,[3,4,-0.4]];
diag_log "spawnjail file endrun";