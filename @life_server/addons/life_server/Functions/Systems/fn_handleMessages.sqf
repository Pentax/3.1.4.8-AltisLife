/*

	file: fn_handleMessages.sqf
	Author: Silex

*/	
	
private["_msg","_to","_target","_player","_type","_this"];
_target = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
_msg = [_this,1,"",[""]] call BIS_fnc_param;
_player = [_this,2,ObjNull,[ObjNull]] call BIS_fnc_param;
_type = [_this,3,-1] call BIS_fnc_param;

switch(_type) do
{
	//normal message
	case 0:
	{
		if(isNULL _target)  exitWith {};
		_to = call compile format["%1", _target];
		[[_msg,name _player,0],"TON_fnc_clientMessage",_to,false] spawn life_fnc_MP;
		
		private["_query","_pid","_toID","_fromName","_toName","_msgType"];
		_pid = getPlayerUID _player;
		_toID = getPlayerUID _target;
		_msg = [_msg] call DB_fnc_mresString;
		_fromName = name _player;
		_toName = name _target;
		_msgType = "Player To Player";
		_query = format["INSERT INTO messages (fromID, toID, message, fromName, toName, msgType) VALUES('%1', '%2', '""%3""', '%4', '%5', '%6')",_pid,_toID,_msg,_fromName,_toName,_msgType];
		waitUntil{!DB_Async_Active};
		[_query,1] call DB_fnc_asyncCall;
	};
	//message to cops
	case 1:
	{	
		[[_msg,name _player,1],"TON_fnc_clientMessage",west,false] spawn life_fnc_MP;
		private["_query","_pid","_toID","_fromName","_toName","_msgType"];
		_pid = getPlayerUID _player;
		_msg = [_msg] call DB_fnc_mresString;
		_fromName = name _player;
		_toName = "All Police";
		_msgType = "Player To Police";
		_query = format["INSERT INTO messages (fromID, toID, message, fromName, toName, msgType) VALUES('%1', '%2', '""%3""', '%4', '%5', '%6')",_pid,_toName,_msg,_fromName,_toName,_msgType];
		waitUntil{!DB_Async_Active};
		[_query,1] call DB_fnc_asyncCall;
	};
	//to admins
	case 2:
	{	
		[[_msg,name _player,2],"TON_fnc_clientMessage",true,false] spawn life_fnc_MP;
		private["_query","_pid","_toID","_fromName","_toName","_msgType"];
		_pid = getPlayerUID _player;
		_toID = getPlayerUID _target;
		_msg = [_msg] call DB_fnc_mresString;
		_fromName = name _player;
		_toName = "Admins";
		_msgType = "Player To Admin";
		_query = format["INSERT INTO messages (fromID, toID, message, fromName, toName, msgType) VALUES('%1', '%2', '""%3""', '%4', '%5', '%6')",_pid,_toName,_msg,_fromName,_toName,_msgType];
		waitUntil{!DB_Async_Active};
		[_query,1] call DB_fnc_asyncCall;
	};
	//ems request
	case 3:
	{	
		[[_msg,name _player,5],"TON_fnc_clientMessage",independent,false] spawn life_fnc_MP;
		private["_query","_pid","_toID","_fromName","_toName","_msgType"];
		_pid = getPlayerUID _player;
		_toID = getPlayerUID _target;
		_msg = [_msg] call DB_fnc_mresString;
		_fromName = name _player;
		_toName = "To EMS";
		_msgType = "Player To EMS";
		_query = format["INSERT INTO messages (fromID, toID, message, fromName, toName, msgType) VALUES('%1', '%2', '""%3""', '%4', '%5', '%6')",_pid,_toName,_msg,_fromName,_toName,_msgType];
		waitUntil{!DB_Async_Active};
		[_query,1] call DB_fnc_asyncCall;
	};
	//adminToPerson
	case 4:
	{
		_to = call compile format["%1", _target];
		if(isNull _to) exitWith {};
	
		[[_msg,name _player,3],"TON_fnc_clientMessage",_to,false] spawn life_fnc_MP;
		private["_query","_pid","_toID","_fromName","_toName","_msgType"];
		_pid = getPlayerUID _player;
		_toID = getPlayerUID _target;
		_msg = [_msg] call DB_fnc_mresString;
		_fromName = name _player;
		_toName = name _target;
		_msgType = "Admin To Player";
		_query = format["INSERT INTO messages (fromID, toID, message, fromName, toName, msgType) VALUES('%1', '%2', '""%3""', '%4', '%5', '%6')",_pid,_toID,_msg,_fromName,_toName,_msgType];
		waitUntil{!DB_Async_Active};
		[_query,1] call DB_fnc_asyncCall;
	};
	//adminMsgAll
	case 5:
	{
		[[_msg,name _player,4],"TON_fnc_clientMessage",true,false] spawn life_fnc_MP;
		private["_query","_pid","_toID","_fromName","_toName","_msgType"];
		_pid = getPlayerUID _player;
		_toID = getPlayerUID _target;
		_msg = [_msg] call DB_fnc_mresString;
		_fromName = name _player;
		_toName = "All Players";
		_msgType = "Admin Message All";
		_query = format["INSERT INTO messages (fromID, toID, message, fromName, toName, msgType,) VALUES('%1', '%2', '""%3""', '%4', '%5', '%6')",_pid,_toName,_msg,_fromName,_toName,_msgType];
		waitUntil{!DB_Async_Active};
		[_query,1] call DB_fnc_asyncCall;
	};
};