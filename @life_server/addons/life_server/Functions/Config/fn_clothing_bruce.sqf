/*
	File: fn_clothing_bruce.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master configuration file for Bruce's Outback Outfits.
*/
private["_filter"];
_filter = [_this,0,0,[0]] call BIS_fnc_param;
//Classname, Custom Display name (use nil for Cfg->DisplayName, price
_free = (0 * life_discount);
_25 = (25 * life_discount);
_50 = (50 * life_discount);
_100 = (100 * life_discount);
_125 = (125 * life_discount);
_150 = (150 * life_discount);
_175 = (175 * life_discount);
_200 = (200 * life_discount);
_225 = (225 * life_discount);
_250 = (250 * life_discount);
_300 = (300 * life_discount);
_325 = (325 * life_discount);
_350 = (350 * life_discount);
_375 = (375 * life_discount);
_400 = (400 * life_discount);
_425 = (425 * life_discount);
_450 = (450 * life_discount);
_475 = (475 * life_discount);
_500 = (500 * life_discount);
_525 = (525 * life_discount);
_550 = (550 * life_discount);
_600 = (600 * life_discount);
_650 = (650 * life_discount);
_700 = (700 * life_discount);
_750 = (750 * life_discount);
_800 = (800 * life_discount);
_850 = (850 * life_discount);
_900 = (900 * life_discount);
_950 = (950 * life_discount);
_1k = (1000 * life_discount);
_1500 = (1500 * life_discount);
_2k = (2000 * life_discount);
_2500 = (2500 * life_discount);
_3k = (3000 * life_discount);
_3500 = (3500 * life_discount);
_5k = (5000 * life_discount);
_8k = (8000 * life_discount);
_10k = (10000 * life_discount);
_25k = (25000 * life_discount);

//Shop Title Name
ctrlSetText[3103,"Clothing Store"];

switch (_filter) do
{
	//Uniforms
	case 0:
	{
		[
		["U_C_Commoner1_1","Commoner Clothes 1",_250],
		["U_C_Poloshirt_blue","Poloshirt Blue",_250],
		["U_C_Poloshirt_burgundy","Poloshirt Burgundy",_250],
		["U_C_Poloshirt_redwhite","Poloshirt Red/White",_150],
		["U_C_Poloshirt_salmon","Poloshirt Salmon",_175],
		["U_C_Poloshirt_stripped","Poloshirt stripped",_125],
		["U_C_Poloshirt_tricolour","Poloshirt Tricolor",_350],
		["U_IG_Guerilla2_2","Green stripped shirt & Pants",_650],
		["U_IG_Guerilla3_1","Brown Jacket & Pants",_700],
		["U_IG_Guerilla2_3","The Outback Rangler",_1k],
		["U_C_HunterBody_grn","The Hunters Look",_1500],
		["U_C_WorkerCoveralls","Mechanic Coveralls",_2500],
		["U_C_Journalist","Press Uniform",_2500],
		["U_OrestesBody","Surfing On Land",_1k],
		["U_B_Wetsuit","Diving Wet Suit",_1k]
		];
	};
	
	//Hats
	case 1:
	{
		[
			["H_Cap_press","Press Cap",_100],
			["H_Bandanna_camo","Camo Bandanna",_100],
			["H_Bandanna_surfer","Surfer Bandanna",_100],
			["H_Bandanna_gry","Grey Bandanna",_150],
			["H_Bandanna_cbr","Bandanna",_100],
			["H_Bandanna_surfer","Bandanna Surfer",_100],
			["H_Bandanna_khk","Khaki Bandanna",_100],
			["H_Bandanna_sgg","Sage Bandanna",_150],
			["H_BandMask_blk","Bandanna Mask",_150],
			["H_BandMask_khk","Bandanna Mask Khaki",_150],
			["H_Cap_blu","Blue Cap",_150],
			["H_Cap_grn","Green Cap",_150],
			["H_Cap_grn_BI","Green Cap",_150],
			["H_Cap_oli","Olive Cap",_150],
			["H_Cap_red","Red Cap",_150],
			["H_Cap_tan","Tan Cap",_150],
			["H_Watchcap_blk","Beanie ",_150],
			["H_Watchcap_camo","Beanie Camo",_150],
			["H_Watchcap_khk","Beanie Khaki",_150],
			["H_Watchcap_sgg","Beanie Sage",_150],
			["H_StrawHat","Straw Fedora",_225],
			["H_StrawHat_dark","Straw Fedora Dark",_225],
			["H_BandMask_blk","Hat & Bandanna",_300],
			["H_Shemag_olive","Shemag Olive(Bandit Mask)",_300],
			["H_Shemag_khk","Shemag Khaki(Bandit Mask)",_300],
			["H_Booniehat_tan","Boonie Hat Tan(Bandit Mask)",_425],
			["H_Booniehat_khk","Boonie Hat Khaki",_425],
			["H_Booniehat_mcamo","Boonie Hat Camo",_425],
			["H_Booniehat_dirty","Boonie Hat Dirty",_425],
			["H_Booniehat_grn","Boonie Hat Green",_425],
			["H_Booniehat_indp","Boonie Hat Khaki",_425],
			["H_Hat_blue","Blue Hat",_300],
			["H_Hat_brown","Brown Hat",_300],
			["H_Hat_checker","Checkered Hat",_350],
			["H_Hat_grey","Grey Hat",_300],
			["H_Hat_tan","Tan Hat",_300],
			["H_MilCap_mcamo","Camo Military Hat",_300]
		];
	};
	
	//Glasses
	case 2:
	{
		[
			["G_Diving",nil,_25],
			["G_Shades_Black",nil,_25],
			["G_Shades_Blue",nil,_25],
			["G_Sport_Blackred",nil,_25],
			["G_Sport_Checkered",nil,_25],
			["G_Sport_Blackyellow",nil,_25],
			["G_Sport_BlackWhite",nil,_25],
			["G_Squares",nil,_25],
			["G_Aviator",nil,_100],
			["G_Lady_Mirror",nil,_150],
			["G_Lady_Dark",nil,_150],
			["G_Lady_Blue",nil,_150],
			["G_Lowprofile",nil,_25],
			["G_Combat",nil,_50]
		];
	};
	
	//Vest
	case 3:
	{
		[
			["V_RebreatherB","Rebreather",_1k],
			["V_Rangemaster_belt","Rangemaster Belt",_5k],
			["V_Press_F","Press Vest",_5k],
			["V_HarnessO_brn","LBV Harness Brown",_5k],
			["V_BandollierB_cbr","Bandolier",_1k]
		];
	};
	
	//Backpacks
	case 4:
	{
		[
			["B_AssaultPack_cbr",nil,_2500],
			["B_Kitbag_mcamo",nil,_5k],
			["B_TacticalPack_oli",nil,_3500],
			["B_FieldPack_ocamo",nil,_3500],
			["B_Bergen_sgg",nil,_5k],
			["B_Kitbag_cbr",nil,_5k],
			["B_Carryall_oli",nil,_5k],
			["B_Carryall_khk",nil,_5k]
		];
	};
};