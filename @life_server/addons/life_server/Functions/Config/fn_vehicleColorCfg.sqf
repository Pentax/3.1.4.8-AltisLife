#include <macro.h>
/*
	File: fn_vehicleColorCfg.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master configuration for vehicle colors.
*/
private["_vehicle","_ret","_path"];
_vehicle = [_this,0,"",[""]] call BIS_fnc_param;
if(_vehicle == "") exitWith {[]};
_ret = [];

switch (_vehicle) do
{
	case "I_Heli_Transport_02_F":
	{
		_path = "\a3\air_f_beta\Heli_Transport_02\Data\Skins\";
		_ret =
		[
			[_path + "heli_transport_02_1_ion_co.paa","civ",_path + "heli_transport_02_2_ion_co.paa",_path + "heli_transport_02_3_ion_co.paa"],
			[_path + "heli_transport_02_1_dahoman_co.paa","civ",_path + "heli_transport_02_2_dahoman_co.paa",_path + "heli_transport_02_3_dahoman_co.paa"]
		];
	};
	
	case "C_Hatchback_01_sport_F":
	{
		_path = "\a3\soft_f_gamma\Hatchback_01\data\";
		_ret =
		[
			[_path + "hatchback_01_ext_sport01_co.paa","civ"],
			[_path + "hatchback_01_ext_sport02_co.paa","civ"],
			[_path + "hatchback_01_ext_sport03_co.paa","civ"],
			[_path + "hatchback_01_ext_sport04_co.paa","civ"],
			[_path + "hatchback_01_ext_sport05_co.paa","civ"],
			[_path + "hatchback_01_ext_sport06_co.paa","civ"],
			["#(ai,64,64,1)Fresnel(1.3,7)","cop"],
			["textures\all\ghostbuster.jpg","civ"],
			["textures\all\ghostbuster.jpg","reb"]
		];
	};
		
	case "C_Offroad_01_F":
	{
		_ret = 
		[
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_co.paa", "civ"], 
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE01_CO.paa", "civ"],
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE02_CO.paa", "civ"],
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE03_CO.paa","civ"],
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE04_CO.paa","civ"],
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE05_CO.paa","civ"],
			["#(ai,64,64,1)Fresnel(0.3,1)","cop"],
			["#(ai,64,64,1)Fresnel(1.3,9)","cop"],
			["textures\cop\medic_offroad.jpg","med"],			
			["textures\cop\police_offroad.jpg","cop"]
		];
	};
	case "C_Offroad_01_repair_F":
	{
		_ret = 
		[
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_co.paa", "civ"], 
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE01_CO.paa", "civ"],
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE02_CO.paa", "civ"],
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE03_CO.paa","civ"],
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE04_CO.paa","civ"],
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE05_CO.paa","civ"],
			["#(ai,64,64,1)Fresnel(0.3,1)","cop"],
			["#(ai,64,64,1)Fresnel(1.3,9)","cop"],
			["textures\cop\medic_offroad.jpg","med"]
		];
	};
	case "I_G_Offroad_01_F":
	{
		_ret = 
		[
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_co.paa", "civ"], 
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE01_CO.paa", "civ"],
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE02_CO.paa", "civ"],
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE03_CO.paa","civ"],
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE04_CO.paa","civ"],
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE05_CO.paa","civ"],
			["#(ai,64,64,1)Fresnel(0.3,1)","cop"],
			["#(ai,64,64,1)Fresnel(1.3,9)","cop"],
			["textures\cop\medic_offroad.jpg","med"],			
			["textures\cop\police_offroad.jpg","cop"]
		];
	};
	case "O_G_Offroad_01_F":
	{
		_ret = 
		[
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_co.paa", "civ"], 
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE01_CO.paa", "civ"],
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE02_CO.paa", "civ"],
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE03_CO.paa","civ"],
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE04_CO.paa","civ"],
			["\A3\soft_F\Offroad_01\Data\offroad_01_ext_BASE05_CO.paa","civ"],
			["#(ai,64,64,1)Fresnel(0.3,1)","cop"],
			["#(ai,64,64,1)Fresnel(1.3,9)","cop"],
			["textures\cop\medic_offroad.jpg","med"],			
			["textures\cop\police_offroad.jpg","cop"]
		];
	};
	
	case "C_Hatchback_01_F":
	{
		_ret =
		[
			["\a3\soft_f_gamma\Hatchback_01\data\hatchback_01_ext_base01_co.paa","civ"],
			["\a3\soft_f_gamma\Hatchback_01\data\hatchback_01_ext_base02_co.paa","civ"],
			["\a3\soft_f_gamma\Hatchback_01\data\hatchback_01_ext_base03_co.paa","civ"],
			["\a3\soft_f_gamma\Hatchback_01\data\hatchback_01_ext_base04_co.paa","civ"],
			["\a3\soft_f_gamma\Hatchback_01\data\hatchback_01_ext_base06_co.paa","civ"],
			["\a3\soft_f_gamma\Hatchback_01\data\hatchback_01_ext_base07_co.paa","civ"],
			["\a3\soft_f_gamma\Hatchback_01\data\hatchback_01_ext_base08_co.paa","civ"],
			["\a3\soft_f_gamma\Hatchback_01\data\hatchback_01_ext_base09_co.paa","civ"],
			["textures\all\ghostbuster.jpg","civ"],
			["textures\all\ghostbuster.jpg","reb"]
		];
	};
	
	case "C_SUV_01_F":
	{
		_ret =
		[
			["\a3\soft_f_gamma\SUV_01\Data\suv_01_ext_co.paa","med"],
			["\a3\soft_f_gamma\SUV_01\Data\suv_01_ext_02_co.paa","cop"],
			["\a3\soft_f_gamma\SUV_01\Data\suv_01_ext_03_co.paa","civ"],
			["\a3\soft_f_gamma\SUV_01\Data\suv_01_ext_04_co.paa","civ"],
			["#(ai,64,64,1)Fresnel(1.3,7)","cop"],
			["textures\cop\police_suv1.jpg","cop"],
			["textures\cop\captain_suv.jpg","cop"]
		];
	};
	
	case "C_Van_01_box_F":
	{
		_ret = 
		[
			["\a3\soft_f_gamma\Van_01\Data\van_01_ext_co.paa","civ"],
			["\a3\soft_f_gamma\Van_01\Data\van_01_ext_red_co.paa","civ"],
			["textures\cop\medic_box.jpg","med","textures\cop\medic_box2.jpg"]
		];
	};
	
	case "C_Van_01_transport_F":
	{
		_ret = 
		[
			["\a3\soft_f_gamma\Van_01\Data\van_01_ext_co.paa","civ"],
			["\a3\soft_f_gamma\Van_01\Data\van_01_ext_red_co.paa","civ"]
		];
	};
	
	case "B_Quadbike_01_F":
	{
		_ret = 
		[
			["\A3\Soft_F\Quadbike_01\Data\Quadbike_01_co.paa","cop"],
			["\A3\Soft_F\Quadbike_01\Data\quadbike_01_opfor_co.paa","reb"],
			["\A3\Soft_F_beta\Quadbike_01\Data\quadbike_01_civ_black_co.paa","civ"],
			["\A3\Soft_F_beta\Quadbike_01\Data\quadbike_01_civ_blue_co.paa","civ"],
			["\A3\Soft_F_beta\Quadbike_01\Data\quadbike_01_civ_red_co.paa","civ"],
			["\A3\Soft_F_beta\Quadbike_01\Data\quadbike_01_civ_white_co.paa","civ"],
			["\A3\Soft_F_beta\Quadbike_01\Data\quadbike_01_indp_co.paa","civ"],
			["\a3\soft_f_gamma\Quadbike_01\data\quadbike_01_indp_hunter_co.paa","civ"],
			["\a3\soft_f_gamma\Quadbike_01\data\quadbike_01_indp_hunter_co.paa","reb"]
		];
	};
	case "O_Quadbike_01_F":
	{
		_ret = 
		[
			["\A3\Soft_F\Quadbike_01\Data\Quadbike_01_co.paa","cop"],
			["\A3\Soft_F\Quadbike_01\Data\quadbike_01_opfor_co.paa","reb"],
			["\A3\Soft_F_beta\Quadbike_01\Data\quadbike_01_civ_black_co.paa","civ"],
			["\A3\Soft_F_beta\Quadbike_01\Data\quadbike_01_civ_blue_co.paa","civ"],
			["\A3\Soft_F_beta\Quadbike_01\Data\quadbike_01_civ_red_co.paa","civ"],
			["\A3\Soft_F_beta\Quadbike_01\Data\quadbike_01_civ_white_co.paa","civ"],
			["\A3\Soft_F_beta\Quadbike_01\Data\quadbike_01_indp_co.paa","civ"],
			["\a3\soft_f_gamma\Quadbike_01\data\quadbike_01_indp_hunter_co.paa","civ"],
			["\a3\soft_f_gamma\Quadbike_01\data\quadbike_01_indp_hunter_co.paa","reb"]
		];
	};
	
	case "C_Heli_Light_01_civil_F":
	{
		_ret = 
		[
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_sheriff_co.paa","cop"],
			["\a3\air_f\Heli_Light_01\Data\heli_light_01_ext_ion_co.paa","cop"],
			["\a3\air_f\Heli_Light_01\Data\heli_light_01_ext_blue_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\heli_light_01_ext_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\heli_light_01_ext_indp_co.paa","donate"],
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_blueline_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_elliptical_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_furious_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_jeans_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_speedy_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_sunset_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_vrana_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_wave_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_digital_co.paa","reb"],
			["textures\all\reblbirdcamo1.jpg","reb","textures\rebel\reblbirdcamo1.jpg"],
			["textures\cop\police_heli.jpg","cop"],
			["textures\cop\medic_heli.jpg","med"],
			["textures\all\camo_deser.jpg","civ","textures\all\camo_deser.jpg"],
			["textures\all\camo_fack.jpg","civ","textures\all\camo_fack.jpg"],
			["textures\all\camo_fuel.jpg","civ","textures\all\camo_fuel.jpg"],
			["textures\all\camo_pank.jpg","civ","textures\all\camo_pank.jpg"],
			["textures\all\camo_deser.jpg","reb","textures\all\camo_deser.jpg"],
			["textures\all\camo_fack.jpg","reb","textures\all\camo_fack.jpg"],
			["textures\all\camo_fuel.jpg","reb","textures\all\camo_fuel.jpg"],
			["textures\all\camo_pank.jpg","reb","textures\all\camo_pank.jpg"]
		];
	};
	case "B_Heli_Light_01_F":
	{
		_ret = 
		[
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_sheriff_co.paa","cop"],
			["\a3\air_f\Heli_Light_01\Data\heli_light_01_ext_ion_co.paa","cop"],
			["\a3\air_f\Heli_Light_01\Data\heli_light_01_ext_blue_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\heli_light_01_ext_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\heli_light_01_ext_indp_co.paa","donate"],
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_blueline_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_elliptical_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_furious_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_jeans_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_speedy_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_sunset_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_vrana_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_wave_co.paa","civ"],
			["\a3\air_f\Heli_Light_01\Data\Skins\heli_light_01_ext_digital_co.paa","reb"],
			["textures\all\reblbirdcamo1.jpg","reb","textures\all\reblbirdcamo1.jpg"],
			["textures\cop\police_heli.jpg","cop"],
			["textures\cop\medic_heli.jpg","med"],
			["textures\all\camo_deser.jpg","civ","textures\all\camo_deser.jpg"],
			["textures\all\camo_fack.jpg","civ","textures\all\camo_fack.jpg"],
			["textures\all\camo_fuel.jpg","civ","textures\all\camo_fuel.jpg"],
			["textures\all\camo_pank.jpg","civ","textures\all\camo_pank.jpg"],
			["textures\all\camo_deser.jpg","reb","textures\all\camo_deser.jpg"],
			["textures\all\camo_fack.jpg","reb","textures\all\camo_fack.jpg"],
			["textures\all\camo_fuel.jpg","reb","textures\all\camo_fuel.jpg"],
			["textures\all\camo_pank.jpg","reb","textures\all\camo_pank.jpg"]
		];
	};
	
	case "O_Heli_Light_02_unarmed_F":
	{
		_ret = 
		[
			["\a3\air_f\Heli_Light_02\Data\heli_light_02_ext_co.paa","cop"],
			["\a3\air_f\Heli_Light_02\Data\heli_light_02_ext_civilian_co.paa","civ"],
			["\a3\air_f\Heli_Light_02\Data\heli_light_02_ext_indp_co.paa","donate"],
			["\a3\air_f\Heli_Light_02\Data\heli_light_02_ext_opfor_co.paa","reb"],
			["#(argb,8,8,3)color(1,1,1,0.8)","med"]
		];
	};
	case "I_Heli_light_03_unarmed_F":
	{
		_ret = 
		[
			["#(argb,8,8,3)color(0.05,0.05,0.05,1)","civ"],
			["textures\all\blackcats_madeby_RedPhoenix_from_BIforums.jpg","reb"],
			["textures\all\camo_deser.jpg","civ"],
			["textures\all\camo_fack.jpg","civ"],
			["textures\all\camo_fuel.jpg","civ"],
			["textures\all\camo_pank.jpg","civ"],
			["textures\all\reblbirdcamo1.jpg","reb"],
			["#(argb,8,8,3)color(0.05,0.05,0.05,1)","cop"],
			["textures\all\blackcats_madeby_RedPhoenix_from_BIforums.jpg","civ"],
			["textures\all\camo_deser.jpg","civ"],
			["textures\all\camo_fack.jpg","civ"],
			["textures\all\camo_fuel.jpg","civ"],
			["textures\all\camo_pank.jpg","civ"]
		];
	};
	
	case "B_MRAP_01_F":
	{
		_ret = 
		[
			["\A3\Soft_F\MRAP_01\Data\mrap_01_base_co.paa","cop"],
			["#(argb,8,8,3)color(0.05,0.05,0.05,1)","cop"],
			["textures\cop\cop_hunter.jpg","cop","textures\cop\swat_hunter_2.jpg"]
		];
	};
	case "O_MRAP_02_F":
	{
		_ret = 
		[
			["\A3\Soft_F\MRAP_01\Data\mrap_01_base_co.paa","cop"],
			["#(argb,8,8,3)color(0.05,0.05,0.05,1)","cop"],
			["textures\all\ifrit_skintemplate_from_GLTpolice1.jpg","cop","textures\all\ifrit_skintemplate_from_GLT1.jpg"],
			["textures\all\ifrit_skintemplate_from_GLTblackbeardA.jpg","reb","textures\all\ifrit_skintemplate_from_GLTblackbeardB.jpg"],
			["textures\all\camo_deser.jpg","reb","textures\all\ifrit_skintemplate_from_GLT1.jpg"],
			["textures\all\camo_fack.jpg","reb","textures\all\ifrit_skintemplate_from_GLT1.jpg"],
			["textures\all\camo_fuel.jpg","reb","textures\all\ifrit_skintemplate_from_GLT1.jpg"],
			["textures\all\camo_pank.jpg","reb","textures\all\ifrit_skintemplate_from_GLT1.jpg"],
			["textures\all\ifrit_skintemplate_from_GLTsilver.jpg","reb","textures\all\ifrit_skintemplate_from_GLT1.jpg"],
			["textures\all\starwars1.jpg","reb","textures\all\ifrit_skintemplate_from_GLT1.jpg"],
			["textures\all\starwars2.jpg","reb","textures\all\ifrit_skintemplate_from_GLT1.jpg"],
			["textures\all\space.jpg","reb","textures\all\ifrit_skintemplate_from_GLT1.jpg"],
			["textures\all\ifrit_skintemplate_from_GLTblackbeardA.jpg","civ","textures\all\ifrit_skintemplate_from_GLTblackbeardB.jpg"],
			["textures\all\camo_deser.jpg","civ","textures\all\ifrit_skintemplate_from_GLT1.jpg"],
			["textures\all\camo_fack.jpg","civ","textures\all\ifrit_skintemplate_from_GLT1.jpg"],
			["textures\all\camo_fuel.jpg","civ","textures\all\ifrit_skintemplate_from_GLT1.jpg"],
			["textures\all\camo_pank.jpg","civ","textures\all\ifrit_skintemplate_from_GLT1.jpg"],
			["textures\all\ifrit_skintemplate_from_GLTsilver.jpg","civ","textures\all\ifrit_skintemplate_from_GLT1.jpg"],
			["textures\all\starwars1.jpg","civ","textures\all\ifrit_skintemplate_from_GLT1.jpg"],
			["textures\all\starwars2.jpg","civ","textures\all\ifrit_skintemplate_from_GLT1.jpg"],
			["textures\all\space.jpg","civ","textures\all\ifrit_skintemplate_from_GLT1.jpg"]
		];
	};
	case "I_MRAP_03_F":
	{
		_ret = 
		[
			["\A3\Soft_F\MRAP_01\Data\mrap_01_base_co.paa","reb"],
			["#(argb,8,8,3)color(0.05,0.05,0.05,1)","cop"]
		];
	};
	
	case "I_Truck_02_covered_F":
	{
		_ret = 
		[
			["\A3\Soft_F_Beta\Truck_02\data\truck_02_kab_co.paa","civ","\a3\soft_f_beta\Truck_02\data\truck_02_kuz_co.paa"],
			["#(argb,8,8,3)color(0.05,0.05,0.05,1)","cop"]
		];
	};
	
	case "I_Truck_02_transport_F":
	{
		_ret = 
		[
			["\A3\Soft_F_Beta\Truck_02\data\truck_02_kab_co.paa","civ","\a3\soft_f_beta\Truck_02\data\truck_02_kuz_co.paa"],
			["#(argb,8,8,3)color(0.05,0.05,0.05,1)","cop"]
		];
	};
	
	
	case "O_Heli_Attack_02_black_F":
	{
		_ret = 
		[
			["#(argb,8,8,3)color(0.05,0.05,0.05,1)","cop"]
		];
	};
};

_ret;