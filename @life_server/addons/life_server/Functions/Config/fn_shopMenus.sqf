/*
	File: fn_shopMenus.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master config for a thing that will be gone eventually..
*/
private["_shop"];
_shop = [_this,0,"",[""]] call BIS_fnc_param;
if(_shop == "") exitWith {closeDialog 0;};

if(!dialog) then
{
	if(!(createDialog "shop_menu")) exitWith {};
};
disableSerialization;
ctrlSetText[601,format["Cash: $%1",[ life_OnHandCash] call life_fnc_numberText]];

switch (_shop) do
{
	case "rebel":
	{
		ctrlSetText[2505,"Rebel Outpost"];
		ctrlShow[2503,false];
		ctrlShow[2513,false];
		ctrlShow[2514,false];
	};
	case "rebel_equipment":
	{
		ctrlSetText[2505,"Rebel Outpost"];
		ctrlShow[2503,false];
		ctrlShow[2513,false];
		ctrlShow[2514,false];
	};
	
	case "cop_basic":
	{
		ctrlSetText[2505,"GrimmLife PD"];
		ctrlShow[2503,false];
	};
	case "med_basic":
	{
		ctrlSetText[2505,"Emergency Services"];
		ctrlShow[2503,false];
	};
	
	case "gun":
	{
		ctrlSetText[2505,"Grimm's Guns"];
		ctrlShow[2503,false];
		ctrlShow[2513,false];
		ctrlShow[2514,false];
	};
	
	case "gang":
	{
		ctrlSetText[2505,"Gang Shop"];
		ctrlShow[2503,false];
		ctrlShow[2513,false];
		ctrlShow[2514,false];
	};
	
	case "dive":
	{
		ctrlSetText[2505,"Brody's Diving Shop"];
		ctrlShow[2503,false];
		ctrlShow[2510,false];
		ctrlShow[2511,false];
		ctrlShow[2513,false];
	};
	
	case "donator_equipment":
	{
		ctrlSetText[2505,"Donator Shop"];
	};
	case "donator":
	{
		ctrlSetText[2505,"Donator Shop"];
	};
	case "meat":
	{
		ctrlSetText[2505,"Butcher's Shop"];
	};
	case "grill":
	{
		ctrlSetText[2505,"Grill Market"];
	};
	
	case "swat_equipment":
	{
		ctrlSetText[2505,"SWAT Shop"];
		ctrlShow[2503,false];
	};
	
	case "bountyHunter":
	{
		ctrlSetText[2505,"Bounty Hunter"];
		ctrlShow[2503,false];
	};
	case "genstore":
	{
		ctrlSetText[2505,"General Store"];
		ctrlShow[2503,false];
	};
	
	case "cop_equipment":
	{
		ctrlSetText[2505,"Police Equipment Shop"];
		ctrlShow[2503,false];
	};
	
	case "admin":
	{
		ctrlSetText[2505,"Admin Shop"];
	};
};

["guns"] call life_fnc_shops_changeMenu;
