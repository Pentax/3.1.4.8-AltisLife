#include <macro.h>
/*
	File:
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master configuration list / array for buyable vehicles & prices and their shop.
*/
private["_shop","_return","_civCars","_donCars1","_donCars2","_donCars3","_donCars4","_donCars5","_civTruck","_rebV1","_copCars","_civAir","_copAir","_civShip","_copShip","_emsCars","_emsAir","_admin","_planes","_gliders","_muscle","_exotic"];
_shop = [_this,0,"",[""]] call BIS_fnc_param;
if(_shop == "") exitWith {[]};
_return = [];

100 = (0 * life_discount);
_50 = (50 * life_discount);
_100 = (100 * life_discount);
_125 = (125 * life_discount);
_150 = (150 * life_discount);
_175 = (175 * life_discount);
_200 = (200 * life_discount);
_225 = (225 * life_discount);
_250 = (250 * life_discount);
_300 = (300 * life_discount);
_325 = (325 * life_discount);
_350 = (350 * life_discount);
_375 = (375 * life_discount);
_400 = (400 * life_discount);
_425 = (425 * life_discount);
_450 = (450 * life_discount);
_475 = (475 * life_discount);
_500 = (500 * life_discount);
_525 = (525 * life_discount);
_550 = (550 * life_discount);
_600 = (600 * life_discount);
_650 = (650 * life_discount);
_700 = (700 * life_discount);
_750 = (750 * life_discount);
_800 = (800 * life_discount);
_850 = (850 * life_discount);
_900 = (900 * life_discount);
_950 = (950 * life_discount);
_1k = (1000 * life_discount);
_2k = (2000 * life_discount);
_5k = (5000 * life_discount);
_7k = (7000 * life_discount);
_10k = (10000 * life_discount);
_20k = (20000 * life_discount);
_25k = (25000 * life_discount);
_35k = (35000 * life_discount);
_45k = (45000 * life_discount);
_50k = (50000 * life_discount);
_75k = (75000 * life_discount);
_100k = (100000 * life_discount);
_125k = (125000 * life_discount);
_150k = (150000 * life_discount);
_175k = (175000 * life_discount);
_200k = (200000 * life_discount);
_225k = (225000 * life_discount);
_250k = (250000 * life_discount);
_300k = (300000 * life_discount);
_325k = (325000 * life_discount);
_350k = (350000 * life_discount);
_375k = (375000 * life_discount);
_400k = (400000 * life_discount);
_425k = (425000 * life_discount);
_450k = (450000 * life_discount);
_475k = (475000 * life_discount);
_500k = (500000 * life_discount);
_750k = (750000 * life_discount);
_1000k = (1000000 * life_discount);

_civCars = [
		["C_Quadbike_01_F",_5k],
		["C_Hatchback_01_F",_7k],
		["C_Hatchback_01_sport_F",_10k],
		["C_SUV_01_F",_25k]

];
//I_MRAP_03_F,B_MRAP_01_F, 0_MRAP_02_F
_civTruck = [

		["O_Quadbike_01_F",_5k],
		["C_Offroad_01_F",_45k],
		["C_Offroad_01_repair_F",_45k],
		["C_Van_01_transport_F",_75k],
		["C_Van_01_box_F",_75k],
		["C_Van_01_fuel_F",_75k],
		["O_Truck_02_box_F",_200k],
		["O_Truck_02_transport_F",_200k],
		["O_Truck_03_device_F",_1000k],
		["B_Truck_01_transport_F",_200k],
		["B_Truck_01_mover_F",_200k]
];

_civAir = [

["C_Heli_Light_01_civil_F",_500k],
["B_Heli_Light_01_F",_500k],
["I_Heli_Transport_02_F",_500k],
["I_Heli_light_03_unarmed_F",_500k],
["O_Heli_Light_02_unarmed_F",_750k]

];
_civShip = [

["B_Lifeboat",_5k],
["C_Boat_Civil_01_F",_5k],
["C_Rubberboat",_5k],
["C_Boat_Civil_01_rescue_F",_5k]

];

//cop cars
_copCars = [
		
		["O_Quadbike_01_F",_5k],
		["C_Quadbike_01_F",_5k],
		["C_Hatchback_01_F",_7k],
		["C_Hatchback_01_sport_F",_10k],
		["C_SUV_01_F",_25k],
		["C_Offroad_01_F",_45k],
		["B_MRAP_01_F",_175k],
		["O_MRAP_02_F",_175k],
		["I_MRAP_03_F",_175k],
		["B_Truck_01_transport_F",_200k],
		["B_Truck_01_mover_F",_200k]

];
_copAir = [           
		["C_Heli_Light_01_civil_F",_500k],
		["B_Heli_Light_01_F",_500k],
		["I_Heli_Transport_02_F",_500k],
		["I_Heli_light_03_unarmed_F",_750k],
		["O_Heli_Light_02_unarmed_F",_750k]  

];
_copShip = [
["B_Lifeboat",_5k],
["C_Boat_Civil_01_F",_5k],
["C_Rubberboat",_5k],
["C_Boat_Civil_01_rescue_F",_5k],
["B_Boat_Transport_01_F",_5k],
["C_Boat_Civil_01_police_F",_10k]

];

//rebel/cars
_rebCars = [
		
		["C_Quadbike_01_F",_5k],
		["O_Quadbike_01_F",_5k],
		["C_Hatchback_01_F",_7k],
		["C_Hatchback_01_sport_F",_10k],
		["C_SUV_01_F",_25k],
		["O_G_Offroad_01_F",_45k],
		["C_Offroad_01_repair_F",_45k],
		["C_Van_01_transport_F",_75k],
		["C_Van_01_box_F",_75k],
		["C_Van_01_fuel_F",_75k],
		["O_Truck_02_box_F",_200k],
		["O_Truck_02_transport_F",_200k],
		["O_MRAP_02_F",_1000k],
		["I_MRAP_03_F",_1000k],
		["O_Truck_03_device_F",_1000k],
		["B_Truck_01_transport_F",_200k],
		["B_Truck_01_mover_F",_200k],
		["C_Heli_Light_01_civil_F",_500k],
		["B_Heli_Light_01_F",_500k],
		["I_Heli_Transport_02_F",_750k],
		["I_Heli_light_03_unarmed_F",_1000k],
		["O_Heli_Light_02_unarmed_F",_1000k]

];

_rebAir = [
		
		
		["C_Heli_Light_01_civil_F",_500k],
		["B_Heli_Light_01_F",_500k],
		["I_Heli_Transport_02_F",_750k],
		["I_Heli_light_03_unarmed_F",_1000k],
		["O_Heli_Light_02_unarmed_F",_1000k]

];

//med shops
_emsCars = [
		
		["C_Van_01_box_F",_75k],
		["C_Offroad_01_F",_45k],		
		["C_Quadbike_01_F",_5k],
		["C_Hatchback_01_F",_7k],
		["C_Hatchback_01_sport_F",_10k],
		["C_SUV_01_F",_25k]
		
];
_emsAir = [

		["B_Heli_Light_01_F",_500k],
		["C_Heli_Light_01_civil_F",_500k],
		["O_Heli_Transport_04_medevac_F",_1000k]
		
];


switch (_shop) do
{
	case "med_shop":{_return = _emsCars;};
	case "med_air_hs":{_return = _emsAir;};	
	case "civ_car":{ _return = _civCars;}; 	
	case "donator_1":
	{
		/*
		if(__GETC__(life_donator) == 1) then {_return = _donCars1;};	
		if(__GETC__(life_donator) == 2) then {_return = _donCars2;};	
		if(__GETC__(life_donator) == 3) then {_return = _donCars3;};	
		if(__GETC__(life_donator) == 4) then {_return = _donCars4;};	
		if(__GETC__(life_donator) == 5) then {_return = _donCars5;};
		*/		
	};
	case "civ_truck":{_return = _civTruck;	};			
	case "reb_car":	{if(license_civ_rebel) then {_return = _rebCars;};};
	case "reb_air":{if(license_civ_rebel) then	{_return = _rebAir;	};};	
	case "cop_car":
	{
		_return = _copCars;
		if(__GETC__(life_coplevel) > 2) then {
		
		//_return pushBack ["B_MRAP_01_F",_200k];
		
		};
		
		//if(__GETC__(life_swat) > 1) then{_return pushBack ["",_50k];};
	};
	case "civ_air":{_return = _civAir;};
	
	case "cop_airhq":
	{
		if(__GETC__(life_coplevel) > 1) then{_return = _copAir;};
	};
	
	case "civ_ship":{_return = _civShip;};
	
	case "cop_ship":{_return = _copShip;};
	case "admin_car":
	{
		
	};
};

_return;
