/*
	File: fn_eatFood.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Main handling system for eating food.
	*Needs to be revised and made more modular and more indept effects*
*/
private["_food","_val","_sum"];
_food = [_this,0,"",[""]] call BIS_fnc_param;
if(_food == "") exitWith {};

if([false,_food,1] call life_fnc_handleInv) then {
	switch (_food) do
	{
		case "apple": {_val = 10};
		case "rabbit":{ _val = 20};
		case "salema": {_val = 30};
		case "ornate": {_val = 25};
		case "mackerel": {_val = 30};
		case "tuna": {_val = 100};
		case "mullet": {_val = 80};
		case "catshark": {_val = 100};
		case "turtle": {_val = 100};
		case "turtlesoup": {_val = 100};
		case "donuts": {_val = 30};
		case "tbacon": {_val = 40};
		case "peach": {_val = 10};
		case "gyros": {_val = 50};
		case "burgers": {_val = 25};
		case "pizza": {_val = 20};
		case "chips": {_val = 10};
		case "hen": {_val = 10};
		case "rooster": {_val = 10};
		case "goat": {_val = 15};
		case "sheep": {_val = 30};
		case "rabbit": {_val = 15};
		case "henC": {_val = 15};
		case "roosterC": {_val = 15};
		case "goatC": {_val = 20};
		case "sheepC": {_val = 40};
		case "rabbitC": {_val = 20};
		case "berries": {_val = 10};
		
	};

	_sum = life_hunger + _val;
	if(_sum > 100) then {_sum = 100;  hint "You have over eaten fatty";};
	life_hunger = _sum;
};