/*
	File: fn_itemWeight.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Gets the items weight and returns it.
*/
private["_item"];
_item = [_this,0,"",[""]] call BIS_fnc_param;
if(_item == "") exitWith {};

switch (_item) do
{
	case "oilu": {3};
	case "oilp": {4};
	case "heroinu": {6};
	case "heroinp": {4};
	case "methu": {6};
	case "methp": {5};
	case "kroku": {6};
	case "krokp": {4};
	case "cannabis": {5};
	case "marijuana": {4};
	case "apple": {2};
	case "water": {1};
	case "rabbit": {1};
	case "salema": {2};
	case "ornate": {2};
	case "mackerel": {2};
	case "tuna": {2};
	case "mullet": {2};
	case "catshark": {2};
	case "turtle": {2};
	case "fishing": {2};
	case "turtlesoup": {2};
	case "donuts": {1};
	case "coffee": {1};
	case "fuelE": {5};
	case "fuelF": {5};
	case "money": {0};
	case "pickaxe": {10};
	case "copperore": {4};
	case "ironore": {4};
	case "copper_r": {4};
	case "iron_r": {4};
	case "sand": {4};
	case "salt": {4};
	case "salt_r": {2};
	case "glass": {2};
	case "diamond": {15};
	case "diamondc": {13};
	case "cocaine": {6};
	case "cocainep": {4};
	case "spikeStrip": {15};
	case "rock": {4};
	case "cement": {3};
	case "goldbar": {20};
	case "gyros": {2};
	case "burgers": {1};
	case "pizza": {1};
	case "chips": {1};
	case "mtdew": {1};
	case "kidney": {15};
	case "scalpel": {2};
	case "bloodbag": {2};
	case "blastingcharge": {15};
	case "boltcutter": {5};
	case "defusekit": {5};
	case "ivkit":{2};
	case "organttk":{10};
	case "rye": {5};
	case "hops": {5};
	case "yeast": {5};
	case "cornmeal": {5};
	case "mash": {5};
	case "whiskey": {5};
	case "beerp": {5};
	case "moonshine": {5};
	case "bottledwhiskey": {7};
	case "bottledbeer": {7};
	case "bottledshine": {7};
	case "bottles": {2};
	case "underwatercharge": {15};
	case "goldbarp": {15};
	case "bombdetect": {10};
	case "civdefusekit": {14};
	case "demolitioncharge": {15};
	case "hen": {5};
	case "rooster": {5};
	case "sheep": {6};
	case "goat": {6};
	case "dog": {7};
	case "adog": {7};
	case "rabbit": {5};
	case "henC": {5};
	case "roosterC": {5};
	case "sheepC": {6};
	case "goatC": {6};
	case "dogC": {7};
	case "adogC": {7};
	case "rabbitC": {5};
	case "campfire": {25};
	case "knife": {2};
	case "berries": {2};	
	case "tenta": {25};
	case "tentb": {25};
	case "transporter": {1};
	default {1};
};
