#include <macro.h>

private["_shop"];
_shop = [_this,0,"",[""]] call BIS_fnc_param;
if(_shop == "") exitWith {closeDialog 0}; //Bad shop type passed.
_free = (0 * life_discount);
_25 = (25 * life_discount);
_50 = (50 * life_discount);
_75 = (75 * life_discount);
_100 = (100 * life_discount);
_125 = (125 * life_discount);
_150 = (150 * life_discount);
_175 = (175 * life_discount);
_200 = (200 * life_discount);
_225 = (225 * life_discount);
_250 = (250 * life_discount);
_275 = (275 * life_discount);
_300 = (300 * life_discount);
_325 = (325 * life_discount);
_350 = (350 * life_discount);
_375 = (375 * life_discount);
_400 = (400 * life_discount);
_425 = (425 * life_discount);
_450 = (450 * life_discount);
_475 = (475 * life_discount);
_500 = (500 * life_discount);
_525 = (525 * life_discount);
_550 = (550 * life_discount);
_600 = (600 * life_discount);
_650 = (650 * life_discount);
_700 = (700 * life_discount);
_750 = (750 * life_discount);
_800 = (800 * life_discount);
_850 = (850 * life_discount);
_900 = (900 * life_discount);
_950 = (950 * life_discount);
_1k = (1000 * life_discount);
_1500 = (1500 * life_discount);
_2k = (2000 * life_discount);
_2500 = (2500 * life_discount);
_3500 = (3500 * life_discount);
_5k = (5000 * life_discount);
_7k = (7000 * life_discount);
_8k = (8000 * life_discount);
_10k = (10000 * life_discount);
_20k = (15000 * life_discount);
_25k = (15000 * life_discount);
_35k = (20000 * life_discount);
_45k = (30000 * life_discount);
_50k = (35000 * life_discount);
_65k = (45000 * life_discount);
_75k = (50000 * life_discount);
_80k = (60000 * life_discount);
_100k = (800000 * life_discount);
_125k = (100000 * life_discount);
_150k = (110000 * life_discount);
_175k = (125000 * life_discount);
_200k = (150000 * life_discount);
_225k = (175000 * life_discount);
_250k = (200000 * life_discount);
_300k = (250000 * life_discount);
_325k = (325000 * life_discount);
_350k = (350000 * life_discount);
_375k = (375000 * life_discount);
_400k = (400000 * life_discount);
_425k = (425000 * life_discount);
_450k = (450000 * life_discount);
_475k = (475000 * life_discount);
_500k = (500000 * life_discount);
_750k = (750000 * life_discount);
_1000k = (1000000 * life_discount);
//["",nil,],
switch(_shop) do
{
	
	case "swat_equipment":
	{
		switch(true) do
		{
			case (playerSide != west): {"You are not a cop!"};
			case (__GETC__(life_swat) == 0): {"You are not a member of SWAT"};
			case (__GETC__(life_swat) >= 1):
			{
				["SWAT Locker",
					[
						["Binocular","Binoculars",_50],
						["Rangefinder","RangeFinder",_75],
						["ItemGPS","GPS",_75],
						["ItemCompass","Compass",_25],
						["ToolKit","Free ToolKit",_free],
						["NVGoggles","Night Vision",_125],
						["ItemRadio","CellPhone",_175],
						["optic_Holosight",nil,_175],
						["optic_Arco",nil,_275],
						["optic_DMS",nil,_325],
						["optic_NVS",nil,_400],
						["optic_SOS",nil,_450],
						["optic_MRCO",nil,_375],
						["acc_pointer_IR",nil,_225],
						["acc_flashlight",nil,_75],
						["muzzle_snds_acp",nil,_200],
						["muzzle_snds_B",nil,_200],
						["SmokeShell",nil,_250],
						["SmokeShellYellow",nil,_250],
						["SmokeShellRed",nil,_250],
						["SmokeShellOrange",nil,_250],
						["HandGrenade_Stone","Flashbang",_250]
								
					]
				];
			};
		};
	};
	case "cop_equipment":
	{
		switch(true) do
		{
			case (playerside !=west):{"You are not a cop"};
			case (__GETC__(life_coplevel) > 0):
			{
				["GLPD Equipment",
					[
						["Binocular","Binoculars",_50],
						["Rangefinder","RangeFinder",_75],
						["ItemGPS","GPS",_75],
						["ItemCompass","Compass",_25],
						["ToolKit","Free ToolKit",_free],
						["NVGoggles","Night Vision",_125],
						["ItemRadio","CellPhone",_175],
						["optic_Holosight",nil,_175],
						["optic_Arco",nil,_275],
						["optic_DMS",nil,_325],
						["optic_NVS",nil,_400],
						["optic_SOS",nil,_450],
						["optic_MRCO",nil,_375],
						["optic_Hamr",nil,_375],
						["acc_pointer_IR",nil,_225],
						["acc_flashlight",nil,_75],
						["muzzle_snds_acp",nil,_200],
						["muzzle_snds_B",nil,_200],
						["muzzle_snds_H",nil,_200],
						["muzzle_snds_L",nil,_200],
						["muzzle_snds_M",nil,_200],
						["muzzle_snds_H_MG",nil,_200],
						["SmokeShell",nil,_250],
						["SmokeShellYellow",nil,_250],
						["SmokeShellRed",nil,_250],
						["SmokeShellOrange",nil,_250]
					]
				];
			};
		};
	
	};
	case "cop_basic":
	{
		switch(true) do
		{
			case (playerSide != west): {"You are not a cop!"};
			case (__GETC__(life_coplevel) == 0): {"You are not a whitelisted officer of the law!"};
			case (__GETC__(life_coplevel) == 1):
			{
				["GLPD Cadets Weapons",
					[
						
						["hgun_P07_snds_F",nil,_8k],//hgun_P07_snds_F
						["30Rnd_9x21_Mag","Tazer Rounds",_500],
						["hgun_Rook40_snds_F",nil,_8k],//hgun_Rook40_snds_F
						["30Rnd_9x21_Mag","Tazer Rounds",_500],
						["hgun_ACPC2_snds_F",nil,_8k],//hgun_ACPC2_snds_F
						["9Rnd_45ACP_Mag",nil,_500],
						["hgun_Pistol_heavy_01_F",nil,_10k],
						["11Rnd_45ACP_Mag",nil,_500],
						["hgun_Pistol_heavy_02_F",nil,_10k],
						["6Rnd_45ACP_Cylinder",nil,_500],
						["hgun_Pistol_Signal_F",nil,_10k],
						["6Rnd_GreenSignal_F",nil,_500],
						["6Rnd_RedSignal_F",nil,_500],
						
						["SMG_01_F",nil,_25k],
						["30Rnd_45ACP_Mag_SMG_01",nil,_300],

						
						["arifle_SDAR_F",nil,_35k],
						["20Rnd_556x45_UW_mag",nil,_650],
						["30Rnd_556x45_Stanag",nil,_650],
						
						["arifle_TRG21_F",nil,_35k],
						["arifle_TRG20_F",nil,_35k],
						["30Rnd_556x45_Stanag",nil,_650],
						["srifle_LRR_LRPS_F",nil,_750k],
						["7Rnd_408_Mag",nil,_1500]


					]
				];
			};
			case (__GETC__(life_coplevel) == 2):
			{
				["GLPD Patrolman Weapons",
					[
						
						
						["hgun_P07_snds_F",nil,_8k],//hgun_P07_snds_F
						["30Rnd_9x21_Mag","Tazer Rounds",_500],
						["hgun_Rook40_snds_F",nil,_8k],//hgun_Rook40_snds_F
						["30Rnd_9x21_Mag","Tazer Rounds",_500],
						["hgun_ACPC2_snds_F",nil,_8k],//hgun_ACPC2_snds_F
						["9Rnd_45ACP_Mag",nil,_500],
						["hgun_Pistol_heavy_01_F",nil,_10k],
						["11Rnd_45ACP_Mag",nil,_500],
						["hgun_Pistol_heavy_02_F",nil,_10k],
						["6Rnd_45ACP_Cylinder",nil,_500],
						["hgun_Pistol_Signal_F",nil,_10k],
						["6Rnd_GreenSignal_F",nil,_500],
						["6Rnd_RedSignal_F",nil,_500],
						
						["SMG_01_F",nil,_25k],
						["30Rnd_45ACP_Mag_SMG_01",nil,_300],

						
						["arifle_SDAR_F",nil,_35k],
						["20Rnd_556x45_UW_mag",nil,_650],
						["30Rnd_556x45_Stanag",nil,_650],

						["arifle_TRG21_F",nil,_35k],
						["arifle_TRG20_F",nil,_35k],
						["30Rnd_556x45_Stanag",nil,_650],
						
						["arifle_MXC_F",nil,_45k],
						["arifle_MX_F",nil,_45k],
						["30Rnd_65x39_caseless_mag",nil,_650],
						
						["srifle_LRR_LRPS_F",nil,_750k],
						["7Rnd_408_Mag",nil,_1500]

					]
				];
			};
			case (__GETC__(life_coplevel) == 3):
			{
				["GLPD Sergeant Weapons",
					[					
						["hgun_P07_snds_F",nil,_8k],//hgun_P07_snds_F
						["30Rnd_9x21_Mag","Tazer Rounds",_500],
						["hgun_Rook40_snds_F",nil,_8k],//hgun_Rook40_snds_F
						["30Rnd_9x21_Mag","Tazer Rounds",_500],
						["hgun_ACPC2_snds_F",nil,_8k],//hgun_ACPC2_snds_F
						["9Rnd_45ACP_Mag",nil,_500],
						["hgun_Pistol_heavy_01_F",nil,_10k],
						["11Rnd_45ACP_Mag",nil,_500],
						["hgun_Pistol_heavy_02_F",nil,_10k],
						["6Rnd_45ACP_Cylinder",nil,_500],
						["hgun_Pistol_Signal_F",nil,_10k],
						["6Rnd_GreenSignal_F",nil,_500],
						["6Rnd_RedSignal_F",nil,_500],
						["HandGrenade_Stone","Flashbang",_250],
						
						["SMG_01_F",nil,_25k],
						["30Rnd_45ACP_Mag_SMG_01",nil,_300],

						
						["arifle_SDAR_F",nil,_35k],
						["20Rnd_556x45_UW_mag",nil,_650],
						["30Rnd_556x45_Stanag",nil,_650],

						["arifle_TRG21_F",nil,_35k],
						["arifle_TRG20_F",nil,_35k],
						["30Rnd_556x45_Stanag",nil,_650],
						

						["arifle_MXC_F",nil,_45k],
						["arifle_MX_F",nil,_45k],
						["arifle_MX_SW_F",nil,_45k],
						["arifle_MXM_F",nil,_45k],
						["arifle_MXM_Black_F",nil,_45k],
						["30Rnd_65x39_caseless_mag",nil,_650],
						["srifle_LRR_LRPS_F",nil,_750k],
						["7Rnd_408_Mag",nil,_1500]


					]
				];
			};
			case (__GETC__(life_coplevel) == 4):
			{
				["GLPD Lieutenant Weapons",
					[
						["hgun_P07_snds_F",nil,_8k],//hgun_P07_snds_F
						["30Rnd_9x21_Mag","Tazer Rounds",_500],
						["hgun_Rook40_snds_F",nil,_8k],//hgun_Rook40_snds_F
						["30Rnd_9x21_Mag","Tazer Rounds",_500],
						["hgun_ACPC2_snds_F",nil,_8k],//hgun_ACPC2_snds_F
						["9Rnd_45ACP_Mag",nil,_500],
						["hgun_Pistol_heavy_01_F",nil,_10k],
						["11Rnd_45ACP_Mag",nil,_500],
						["hgun_Pistol_heavy_02_F",nil,_10k],
						["6Rnd_45ACP_Cylinder",nil,_500],
						["hgun_Pistol_Signal_F",nil,_10k],
						["6Rnd_GreenSignal_F",nil,_500],
						["6Rnd_RedSignal_F",nil,_500],
						["HandGrenade_Stone","Flashbang",_250],
						
						["SMG_01_F",nil,_25k],
						["30Rnd_45ACP_Mag_SMG_01",nil,_300],
						
						["arifle_SDAR_F",nil,_35k],
						["20Rnd_556x45_UW_mag",nil,_650],
						["30Rnd_556x45_Stanag",nil,_650],

						["arifle_TRG21_F",nil,_35k],
						["arifle_TRG20_F",nil,_35k],
						["30Rnd_556x45_Stanag",nil,_650],

						["arifle_MXC_F",nil,_45k],
						["arifle_MX_F",nil,_45k],
						["arifle_MX_SW_F",nil,_45k],
						["arifle_MXM_F",nil,_45k],
						["arifle_MXM_Black_F",nil,_45k],
						["30Rnd_65x39_caseless_mag",nil,_650],
						
						["LMG_Mk200_F",nil,_300k],
						["200Rnd_65x39_cased_Box",nil,_1k],
						["srifle_LRR_LRPS_F",nil,_750k],
						["7Rnd_408_Mag",nil,_1500],
						
						["srifle_EBR_DMS_F",nil,_75k],
						["20Rnd_762x51_Mag",nil,_950]

					]
				];
			};
			case (__GETC__(life_coplevel) >= 5):
			{
				["GLPD High Ranks",
					[
						
						["hgun_P07_snds_F",nil,_8k],//hgun_P07_snds_F
						["30Rnd_9x21_Mag","Tazer Rounds",_500],
						["hgun_Rook40_snds_F",nil,_8k],//hgun_Rook40_snds_F
						["30Rnd_9x21_Mag","Tazer Rounds",_500],
						["hgun_ACPC2_snds_F",nil,_8k],//hgun_ACPC2_snds_F
						["9Rnd_45ACP_Mag",nil,_500],
						["hgun_Pistol_heavy_01_F",nil,_10k],
						["11Rnd_45ACP_Mag",nil,_500],
						["hgun_Pistol_heavy_02_F",nil,_10k],
						["6Rnd_45ACP_Cylinder",nil,_500],
						["hgun_Pistol_Signal_F",nil,_10k],
						["6Rnd_GreenSignal_F",nil,_500],
						["6Rnd_RedSignal_F",nil,_500],
						["HandGrenade_Stone","Flashbang",_250],
						
						["SMG_01_F",nil,_25k],
						["30Rnd_45ACP_Mag_SMG_01",nil,_300],
						
						["arifle_SDAR_F",nil,_35k],
						["20Rnd_556x45_UW_mag",nil,_650],
						["30Rnd_556x45_Stanag",nil,_650],

						["arifle_TRG21_F",nil,_35k],
						["arifle_TRG20_F",nil,_35k],
						["30Rnd_556x45_Stanag",nil,_650],
						
						["arifle_MXC_F",nil,_45k],
						["arifle_MX_F",nil,_45k],
						["arifle_MX_SW_F",nil,_45k],
						["arifle_MXM_F",nil,_45k],
						["arifle_MXM_Black_F",nil,_45k],
						["30Rnd_65x39_caseless_mag",nil,_650],
						
						["LMG_Mk200_F",nil,_300k],
						["200Rnd_65x39_cased_Box",nil,_1k],
						
						["srifle_EBR_DMS_F",nil,_75k],
						["20Rnd_762x51_Mag",nil,_950],
						["srifle_LRR_LRPS_F",nil,_750k],
						["7Rnd_408_Mag",nil,_1500],
	
						["srifle_GM6_LRPS_F",nil,_450k],
						["5Rnd_127x108_Mag",nil,_1500]

					]
				];
			};
			
		};
	};
	
	case "med_basic":
	{
		switch (true) do 
		{
			case (playerSide != independent): {"You are not an EMS Medic"};
			default {
				["Hospital EMS Shop",
					[
						["ItemGPS",nil,_75],["Binocular",nil,_50],["ItemRadio","CellPhone",_free],["ToolKit",nil,_free],["Medikit",nil,_250],["NVGoggles",nil,_125],["B_Kitbag_cbr",nil,_225],["U_Competitor",nil,_225]
					]
				];
			};
		};
	};

	
	case "rebel_equipment":
	{
		switch(true) do
		{
			case (playerSide !=east): {"You are not a civilian"};
			case (!license_civ_rebel): {"You have not joined the rebels yet!"};
			default
			{
				["Rebel Equipment",
					[
						["Binocular","Binoculars",_50],
						["Rangefinder","RangeFinder",_75],
						["ItemGPS","GPS",_75],
						["ItemCompass","Compass",_25],
						["ToolKit","Free ToolKit",_free],
						["NVGoggles","Night Vision",_125],
						["ItemRadio","CellPhone",_175],
						["optic_Holosight",nil,_175],
						["optic_Arco",nil,_275],
						["optic_DMS",nil,_325],
						["optic_NVS",nil,_400],
						["optic_SOS",nil,_450],
						["optic_MRCO",nil,_375],
						["optic_Hamr",nil,_375],
						["acc_pointer_IR",nil,_225],
						["acc_flashlight",nil,_75],
						["muzzle_snds_acp",nil,_200],
						["muzzle_snds_B",nil,_200],
						["muzzle_snds_H",nil,_200],
						["muzzle_snds_L",nil,_200],
						["muzzle_snds_M",nil,_200],
						["SmokeShell",nil,_250],
						["SmokeShellYellow",nil,_250],
						["SmokeShellRed",nil,_250],
						["SmokeShellOrange",nil,_250]
					]
				];	
			};
		};
	};
	case "rebel":
	{
		switch(true) do
		{
			case (playerSide != east): {"You are not a civilian!"};
			case (!license_civ_rebel): {"You have not joined the rebels yet!!"};
			default
			{
				["Rebel Weapons Cache",
					[
						["hgun_P07_F",nil,_8k],//hgun_P07_snds_F
						["30Rnd_9x21_Mag",nil,_500],
						["hgun_Rook40_F",nil,_8k],//hgun_Rook40_snds_F
						["30Rnd_9x21_Mag",nil,_500],
						["hgun_ACPC2_F",nil,_8k],//hgun_ACPC2_snds_F
						["9Rnd_45ACP_Mag",nil,_500],
						["hgun_Pistol_heavy_01_F",nil,_10k],
						["11Rnd_45ACP_Mag",nil,_500],
						["hgun_Pistol_heavy_02_F",nil,_10k],
						["6Rnd_45ACP_Cylinder",nil,_500],
						["hgun_Pistol_Signal_F",nil,_10k],
						["6Rnd_GreenSignal_F",nil,_500],
						["6Rnd_RedSignal_F",nil,_500],
						["hgun_PDW2000_F",nil,_20k],
						["30Rnd_9x21_Mag",nil,_500],

						["SMG_02_F",nil,_25k],
						["30Rnd_9x21_Mag",nil,_300],

						["arifle_SDAR_F",nil,_35k],
						["20Rnd_556x45_UW_mag",nil,_650],
						["30Rnd_556x45_Stanag",nil,_650],

						["arifle_Mk20_F",nil,_50k],
						["arifle_Mk20C_F",nil,_50k],
						["30Rnd_556x45_Stanag",nil,_650],

						["arifle_MXM_Black_F",nil,_45k],
						["30Rnd_65x39_caseless_mag",nil,_650],
						["srifle_DMR_01_F",nil,_175k],
						["10Rnd_762x51_Mag",nil,_950],

						["LMG_Zafir_F",nil,_500k],
						["150Rnd_762x51_Box",nil,_1k],

						["srifle_LRR_LRPS_F",nil,_750k],
						["7Rnd_408_Mag",nil,_1500]
					]
				];
			};
		};
	};
	case "supporter":
	{
		switch(true) do
		{
			case (playerSide != east): {"You are not a civilian!"};
			case (__GETC__(life_donator) < 1): {hint"You are not a supporter of this server!";};
			case (__GETC__(life_donator) > 0): {
			
				["Supporter Weapons",
					[
						["hgun_P07_F",nil,_8k],//hgun_P07_snds_F
						["30Rnd_9x21_Mag",nil,_500],
						["hgun_Rook40_F",nil,_8k],//hgun_Rook40_snds_F
						["30Rnd_9x21_Mag",nil,_500],
						["hgun_ACPC2_F",nil,_8k],//hgun_ACPC2_snds_F
						["9Rnd_45ACP_Mag",nil,_500],
						["hgun_Pistol_heavy_01_F",nil,_10k],
						["11Rnd_45ACP_Mag",nil,_500],
						["hgun_Pistol_heavy_02_F",nil,_10k],
						["6Rnd_45ACP_Cylinder",nil,_500],
						["hgun_Pistol_Signal_F",nil,_10k],
						["6Rnd_GreenSignal_F",nil,_500],
						["6Rnd_RedSignal_F",nil,_500],
						["hgun_PDW2000_F",nil,_20k],
						["30Rnd_9x21_Mag",nil,_500],

						["SMG_02_F",nil,_25k],
						["30Rnd_9x21_Mag",nil,_300],

						["arifle_SDAR_F",nil,_35k],
						["20Rnd_556x45_UW_mag",nil,_650],
						["30Rnd_556x45_Stanag",nil,_650],

						["arifle_Mk20_F",nil,_50k],
						["arifle_Mk20C_F",nil,_50k],
						["30Rnd_556x45_Stanag",nil,_650],

						["arifle_MXM_Black_F",nil,_45k],
						["30Rnd_65x39_caseless_mag",nil,_650],
						["srifle_DMR_01_F",nil,_175k],
						["10Rnd_762x51_Mag",nil,_950],

						["LMG_Zafir_F",nil,_500k],
						["150Rnd_762x51_Box",nil,_1k],

						["srifle_LRR_LRPS_F",nil,_750k],
						["7Rnd_408_Mag",nil,_1500]
					]
				];
			};
		};
	};
	case "gun":
	{
		switch(true) do
		{
			case (playerSide != east): {"You are not a civilian!"};
			case (!license_civ_gun): {"I don't sell guns illegally, you need a license!"};
			default
			{
				["Grimm's GunShop",
					[
						["Binocular","Binoculars",_50],
						["Rangefinder","RangeFinder",_75],
						["ItemGPS","GPS",_75],
						["ItemCompass","Compass",_25],
						["ToolKit","Free ToolKit",_free],
						["NVGoggles","Night Vision",_125],
						["ItemRadio","CellPhone",_175],
						["optic_Holosight",nil,_175],
						["optic_Arco",nil,_275],
						["optic_DMS",nil,_325],
						["optic_NVS",nil,_400],
						["optic_SOS",nil,_450],
						["optic_MRCO",nil,_375],
						["acc_pointer_IR",nil,_225],
						["acc_flashlight",nil,_75],
						["muzzle_snds_acp",nil,_200],
						["muzzle_snds_B",nil,_200],
						["SmokeShell",nil,_250],
						["SmokeShellYellow",nil,_250],
						["SmokeShellRed",nil,_250],
						["SmokeShellOrange",nil,_250],						
						["hgun_Pistol_heavy_01_F",nil,_10k],
						["11Rnd_45ACP_Mag",nil,_500],
						["hgun_Pistol_heavy_02_F",nil,_10k],
						["6Rnd_45ACP_Cylinder",nil,_500],
						["hgun_Pistol_Signal_F",nil,_10k],
						["6Rnd_GreenSignal_F",nil,_500],
						["6Rnd_RedSignal_F",nil,_500],
						["hgun_PDW2000_F",nil,_20k],
						["30Rnd_9x21_Mag",nil,_500],
						["SMG_01_F",nil,_25k],
						["30Rnd_45ACP_Mag_SMG_01",nil,_300],
						["arifle_SDAR_F",nil,_35k],
						["20Rnd_556x45_UW_mag",nil,_650],
						["30Rnd_556x45_Stanag",nil,_650],
						["arifle_Mk20_F",nil,_50k],
						["arifle_Mk20C_F",nil,_50k],
						["30Rnd_556x45_Stanag",nil,_650]


						
						
					]
				];
			};
		};
	};
	
	
	case "genstore":
	{
		["General Store",
			[
						["Binocular","Binoculars",_50],
						["Rangefinder","RangeFinder",_75],
						["ItemGPS","GPS",_75],
						["ItemCompass","Compass",_25],
						["ToolKit","Free ToolKit",_free],
						["NVGoggles","Night Vision",_125],
						["ItemRadio","CellPhone",_175],
						["optic_Holosight",nil,_175],
						["optic_Arco",nil,_275],
						["optic_DMS",nil,_325],
						["optic_NVS",nil,_400],
						["optic_SOS",nil,_450],
						["optic_MRCO",nil,_375],
						["acc_pointer_IR",nil,_225],
						["acc_flashlight",nil,_75],
						["muzzle_snds_acp",nil,_200],
						["muzzle_snds_B",nil,_200],
						["SmokeShell",nil,_250],
						["SmokeShellYellow",nil,_250],
						["SmokeShellRed",nil,_250],
						["SmokeShellOrange",nil,_250]
			]
		];
	};
	
	case "gang":
	{
		switch(true) do
		{
			case (playerSide != east): {"You are not a east!"};
			default
			{
				["Hideout Armament",
					[
						["Binocular","Binoculars",_50],
						["Rangefinder","RangeFinder",_75],
						["ItemGPS","GPS",_75],
						["ItemCompass","Compass",_25],
						["ToolKit","Free ToolKit",_free],
						["NVGoggles","Night Vision",_125],
						["ItemRadio","CellPhone",_175],
						["optic_Holosight",nil,_175],
						["optic_Arco",nil,_275],
						["optic_DMS",nil,_325],
						["optic_NVS",nil,_400],
						["optic_SOS",nil,_450],
						["optic_MRCO",nil,_375],
						["acc_pointer_IR",nil,_225],
						["acc_flashlight",nil,_75],
						["muzzle_snds_acp",nil,_200],
						["muzzle_snds_B",nil,_200],
						["SmokeShell",nil,_250],
						["SmokeShellYellow",nil,_250],
						["SmokeShellRed",nil,_250],
						["SmokeShellOrange",nil,_250],
						["hgun_P07_F",nil,_8k],//hgun_P07_snds_F
						["30Rnd_9x21_Mag",nil,_500],
						["hgun_Rook40_F",nil,_8k],//hgun_Rook40_snds_F
						["30Rnd_9x21_Mag",nil,_500],
						["hgun_ACPC2_F",nil,_8k],//hgun_ACPC2_snds_F
						["9Rnd_45ACP_Mag",nil,_500],
						["hgun_Pistol_heavy_01_F",nil,_10k],
						["11Rnd_45ACP_Mag",nil,_500],
						["hgun_Pistol_heavy_02_F",nil,_10k],
						["6Rnd_45ACP_Cylinder",nil,_500],
						["hgun_Pistol_Signal_F",nil,_10k],
						["6Rnd_GreenSignal_F",nil,_500],
						["6Rnd_RedSignal_F",nil,_500],
						["hgun_PDW2000_F",nil,_20k],
						["30Rnd_9x21_Mag",nil,_500],

						["SMG_02_F",nil,_25k],
						["30Rnd_9x21_Mag",nil,_300],

						["arifle_SDAR_F",nil,_35k],
						["20Rnd_556x45_UW_mag",nil,_650],
						["30Rnd_556x45_Stanag",nil,_650],

						["arifle_Mk20_F",nil,_50k],
						["arifle_Mk20C_F",nil,_50k],
						["30Rnd_556x45_Stanag",nil,_650],

						["arifle_MXM_Black_F",nil,_45k],
						["30Rnd_65x39_caseless_mag",nil,_650],
						["srifle_DMR_01_F",nil,_175k],
						["10Rnd_762x51_Mag",nil,_950],

						["LMG_Zafir_F",nil,_500k],
						["150Rnd_762x51_Box",nil,_1k],

						["srifle_LRR_LRPS_F",nil,_750k],
						["7Rnd_408_Mag",nil,_1500]
						
					]
				];
			};
		};
	};
};
