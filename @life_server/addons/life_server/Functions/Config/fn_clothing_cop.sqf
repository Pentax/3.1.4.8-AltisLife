#include <macro.h>
/*
	File: fn_clothing_cop.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master config file for Cop clothing store.
*/
private["_filter","_ret"];
_filter = [_this,0,0,[0]] call BIS_fnc_param;
//Classname, Custom Display name (use nil for Cfg->DisplayName, price

//Shop Title Name
ctrlSetText[3103,"Grimm Life Police Dept"];

_ret = [];
_free = (0 * life_discount);
_25 = (25 * life_discount);
_50 = (50 * life_discount);
_100 = (100 * life_discount);
_125 = (125 * life_discount);
_150 = (150 * life_discount);
_175 = (175 * life_discount);
_200 = (200 * life_discount);
_225 = (225 * life_discount);
_250 = (250 * life_discount);
_300 = (300 * life_discount);
_325 = (325 * life_discount);
_350 = (350 * life_discount);
_375 = (375 * life_discount);
_400 = (400 * life_discount);
_425 = (425 * life_discount);
_450 = (450 * life_discount);
_475 = (475 * life_discount);
_500 = (500 * life_discount);
_525 = (525 * life_discount);
_550 = (550 * life_discount);
_600 = (600 * life_discount);
_650 = (650 * life_discount);
_700 = (700 * life_discount);
_750 = (750 * life_discount);
_800 = (800 * life_discount);
_850 = (850 * life_discount);
_900 = (900 * life_discount);
_950 = (950 * life_discount);
_1k = (1000 * life_discount);
_1500 = (1500 * life_discount);
_2k = (2000 * life_discount);
_2500 = (2500 * life_discount);
_3k = (3000 * life_discount);
_3500 = (3500 * life_discount);
_5k = (5000 * life_discount);
_8k = (8000 * life_discount);
_10k = (10000 * life_discount);
_25k = (25000 * life_discount);
switch (_filter) do
{
	//Uniforms
	case 0:
	{
		
		_ret pushBack ["U_Rangemaster","Police Uniform",_100];
		_ret pushBack ["U_B_Wetsuit",nil,_1k];
		if(__GETC__(life_coplevel) > 2) then
		{
			_ret pushBack ["U_B_GhillieSuit",nil,_550];
			_ret pushBack ["U_B_survival_uniform",nil,_550];
			_ret pushBack ["U_B_Wetsuit",nil,_1k];
			_ret pushBack ["U_B_CTRG_1",nil,_550];
			_ret pushBack ["U_B_CTRG_2",nil,_550];
			_ret pushBack ["U_B_CTRG_3",nil,_550];
		};
		if(__GETC__(life_coplevel) > 4) then
		{
			
			_ret pushBack ["U_B_survival_uniform",nil,_550];
		};
		if(__GETC__(life_coplevel) > 5) then
		{
			_ret pushBack ["U_BG_leader",nil,_550];
		};
	};
	
	//Hats
	case 1:
	{
		_ret pushBack ["H_Cap_police",nil,_100];
		_ret pushBack ["H_MilCap_mcamo",nil,_100];
		_ret pushBack ["H_Cap_tan_specops_US",nil,_100];
		if(__GETC__(life_coplevel) > 2) then
		{
			_ret pushBack ["H_CrewHelmetHeli_B","GasMask",_1k];
			_ret pushBack ["H_MilCap_oucamo",nil,_100];
			_ret pushBack ["H_Booniehat_mcamo",nil,_100];
			_ret pushBack ["H_Watchcap_blk",nil,_100];
			_ret pushBack ["H_Watchcap_khk",nil,_100];
			_ret pushBack ["H_Beret_grn",nil,_25];
			_ret pushBack ["H_HelmetB",nil,_25];
			_ret pushBack ["H_HelmetB_plain_mcamo",nil,_25];
			_ret pushBack ["H_HelmetB_light",nil,_25];
			_ret pushBack ["H_HelmetB_light_black",nil,_25];
			_ret pushBack ["H_CrewHelmetHeli_B","GasMask",_1k];
			_ret pushBack ["H_MilCap_oucamo",nil,_100];
			_ret pushBack ["H_Booniehat_mcamo",nil,_100];
			_ret pushBack ["H_Watchcap_blk",nil,_100];
			_ret pushBack ["H_Watchcap_khk",nil,_100];
		};
		if(__GETC__(life_coplevel) > 4) then
		{
			_ret pushBack ["H_Beret_grn",nil,_25];
			_ret pushBack ["H_HelmetB",nil,_25];
			_ret pushBack ["H_HelmetB_plain_mcamo",nil,_25];
			_ret pushBack ["H_HelmetB_light",nil,_25];
			_ret pushBack ["H_HelmetB_light_black",nil,_25];
		};
		if(__GETC__(life_coplevel) > 5) then
		{
			_ret pushBack ["H_Beret_Colonel",nil,_25];
			
		};
	};
	
	//Glasses
	case 2:
	{
		
		_ret pushBack ["G_Aviator",nil,_100];
		_ret pushBack ["G_Squares",nil,_100];
		_ret pushBack ["G_Lowprofile",nil,_100];
		_ret pushBack ["G_Combat",nil,_100];
		_ret pushBack ["G_Diving",nil,_100];
		_ret pushBack ["G_Bandanna_beast",nil,_25];
			_ret pushBack ["G_Bandanna_tan",nil,_25];
			_ret pushBack ["G_Bandanna_sport",nil,_25];
			_ret pushBack ["G_Bandanna_blk",nil,_25];
			_ret pushBack ["G_Bandanna_oli",nil,_25];
			_ret pushBack ["G_Bandanna_shades","GasMask",_1k];
			_ret pushBack ["G_Bandanna_aviator",nil,_100];
			_ret pushBack ["G_Balaclava_blk",nil,_100];
			_ret pushBack ["G_Balaclava_lowprofile",nil,_100];
			_ret pushBack ["G_Balaclava_combat",nil,_100];
			_ret pushBack ["G_Balaclava_oli",nil,_100];
			_ret pushBack ["G_Shades_Black",nil,_100];
			_ret pushBack ["G_Shades_Blue",nil,_100];
			_ret pushBack ["G_Sport_Blackred",nil,_100];
			_ret pushBack ["G_Sport_Checkered",nil,_100];
			_ret pushBack ["G_Sport_Blackyellow",nil,_100];
			_ret pushBack ["G_Sport_BlackWhite",nil,_100];
			_ret pushBack ["G_Aviator",nil,_100];
			_ret pushBack ["G_Squares",nil,_100];
			_ret pushBack ["G_Lowprofile",nil,_100];
			_ret pushBack ["G_Combat",nil,_100];
			_ret pushBack ["G_Diving",nil,_100];
	};
	
	//Vest
	case 3:
	{
		_ret pushBack ["V_Rangemaster_belt",nil,_800];
		_ret pushBack ["V_RebreatherB",nil,_2k];
		_ret pushBack ["V_TacVest_blk_POLICE",nil,_800];
		_ret pushBack ["V_BandollierB_blk",nil,_1500];
		_ret pushBack ["V_Chestrig_blk",nil,_1500];
		if(__GETC__(life_coplevel) > 1) then
		{
				
				_ret pushBack ["V_TacVestIR_blk",nil,_1500];
				_ret pushBack ["V_PlateCarrier1_blk",nil,_1500];
				_ret pushBack ["V_PlateCarrierIAGL_dgtl",nil,_1500];
				_ret pushBack ["V_PlateCarrier1_blk",nil,_1500];
		};
		
	};
	
	//Backpacks
	case 4:
	{
		_ret pushBack ["B_AssaultPack_blk",nil,_800];
		if(__GETC__(life_coplevel) > 1) then
		{
				_ret pushBack ["B_Kitbag_mcamo",nil,_2k];
				_ret pushBack ["B_TacticalPack_oli",nil,_800];
				_ret pushBack ["B_FieldPack_blk",nil,_1500];
				_ret pushBack ["B_FieldPack_ocamo",nil,_1500];
				_ret pushBack ["B_Bergen_sgg",nil,_1500];
				_ret pushBack ["B_Kitbag_cbr",nil,_1500];
				_ret pushBack ["B_Carryall_oli",nil,_1500];
				_ret pushBack ["B_Carryall_khk",nil,_1500];
				_ret pushBack ["B_Parachute",nil,_1500];
								
		};
	};
};

_ret;