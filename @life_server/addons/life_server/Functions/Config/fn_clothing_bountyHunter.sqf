#include <macro.h>
/*
	File: fn_clothing_reb.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master configuration file for Reb shop.
*/
private["_filter","_ret"];
_filter = [_this,0,0,[0]] call BIS_fnc_param;
//Classname, Custom Display name (use nil for Cfg->DisplayName, price

//Shop Title Name
ctrlSetText[3103,"Bounty Hunter Clothing"];
_ret = [];
_free = (0 * life_discount);
_50 = (50 * life_discount);
_100 = (100 * life_discount);
_125 = (125 * life_discount);
_150 = (150 * life_discount);
_175 = (175 * life_discount);
_200 = (200 * life_discount);
_225 = (225 * life_discount);
_250 = (250 * life_discount);
_300 = (300 * life_discount);
_325 = (325 * life_discount);
_350 = (350 * life_discount);
_375 = (375 * life_discount);
_400 = (400 * life_discount);
_425 = (425 * life_discount);
_450 = (450 * life_discount);
_475 = (475 * life_discount);
_500 = (500 * life_discount);
_525 = (525 * life_discount);
_550 = (550 * life_discount);
_600 = (600 * life_discount);
_650 = (650 * life_discount);
_700 = (700 * life_discount);
_750 = (750 * life_discount);
_800 = (800 * life_discount);
_850 = (850 * life_discount);
_900 = (900 * life_discount);
_950 = (950 * life_discount);
_1k = (1000 * life_discount);
_1500 = (1500 * life_discount);
_2k = (2000 * life_discount);
_2500 = (2500 * life_discount);
_3k = (3000 * life_discount);
_3500 = (3500 * life_discount);
_5k = (5000 * life_discount);
_8k = (8000 * life_discount);
_10k = (10000 * life_discount);
_25k = (25000 * life_discount);
switch (_filter) do
{
	//Uniforms"[`U_OG_Guerilla3_1`,`V_I_G_resistanceLeader_F`,``,`G_Bandanna_beast`,`H_Cap_tan`,[`ItemMap`,`ItemCompass`,`ItemWatch`],`arifle_Katiba_C_ACO_F`,``,[`U_OG_Guerilla2_2`,`V_Chestrig_blk`],[],[],[],[],[`30Rnd_65x39_caseless_green_mag_Tracer`,`30Rnd_65x39_caseless_green_mag_Tracer`,`30Rnd_65x39_caseless_green_mag_Tracer`,`30Rnd_65x39_caseless_green_mag_Tracer`,`30Rnd_65x39_caseless_green_mag_Tracer`,`30Rnd_65x39_caseless_green_mag_Tracer`],[``,``,`optic_ACO_grn`],[],[]]"
	case 0:
	{
		
		_ret pushBack ["U_OG_Guerilla3_1","Bandit Smocks Dark",_2500];
		_ret pushBack ["U_OG_Guerilla3_2","Bandit Smocks Light",_2500];
		
		
	};
	
	//Hats
	case 1:
	{
		
		_ret pushBack ["H_Beret_blk",nil,_1500];
		_ret pushBack ["H_Cap_tan",nil,_1500];
		_ret pushBack ["H_Booniehat_dgtl","Digi Boonie",_1500];
		_ret pushBack ["H_Booniehat_grn","Booniehat",_1500];
		_ret pushBack ["H_Booniehat_khk","Booniehat Hex",_1500];
		_ret pushBack ["H_ShemagOpen_tan","Tan Shemag",_1500];
		_ret pushBack ["H_Shemag_olive","Olive Shemag",_1500];
		_ret pushBack ["H_ShemagOpen_khk","Khaki Shemag",_1500];
		_ret pushBack ["H_Bandanna_camo","Camo Bandanna",_1500];
		
	};
	
	//Glasses
	case 2:
	{
		_ret =
		[
			["G_Diving",nil,_750],
			["G_Bandanna_beast",nil,_750],
			["G_Bandanna_tan",nil,_750],
			["G_Bandanna_sport",nil,_750],
			["G_Bandanna_blk",nil,_750],
			["G_Bandanna_oli",nil,_750],
			["G_Bandanna_shades",nil,_750],
			["G_Bandanna_aviator",nil,_750],
			["G_Balaclava_blk",nil,_750],
			["G_Balaclava_lowprofile",nil,_750],
			["G_Balaclava_combat",nil,_750],
			["G_Balaclava_oli",nil,_750],
			["G_Shades_Black",nil,_750],
			["G_Shades_Blue",nil,_750],
			["G_Sport_Blackred",nil,_750],
			["G_Sport_Checkered",nil,_750],
			["G_Sport_Blackyellow",nil,_750],
			["G_Sport_BlackWhite",nil,_750],
			["G_Squares",nil,_750],
			["G_Aviator",nil,_750],
			["G_Lady_Mirror",nil,_750],
			["G_Lady_Dark",nil,_750],
			["G_Lady_Blue",nil,_750],
			["G_Lowprofile",nil,_750],
			["G_Combat",nil,_750]
		];
	};
	
	//Vest
	case 3:
	{
		_ret pushBack ["V_BandollierB_khk","Slash Bandolier",_5k];
		_ret pushBack ["V_Chestrig_khk","Chest rig",_5k];
		_ret pushBack ["V_TacVest_brn","Tactical Vest",_5k];
		_ret pushBack ["V_HarnessO_brn","LBV Harness",_5k];
		_ret pushBack ["V_RebreatherIR","Rebreather",_5k];
		_ret pushBack ["V_PlateCarrierIA1_dgtl","GA Carrier Lite",_5k];
		_ret pushBack ["V_I_G_resistanceLeader_F","Tactical Vest Leader",_5k];
		
	};
	
	//Backpacks
	case 4:
	{
		_ret =
		[
			["B_AssaultPack_Base", nil,_1k],
			["B_AssaultPack_blk", nil,_1k],
			["B_AssaultPack_blk_DiverExp", nil,_1k],
			["B_AssaultPack_cbr", nil,_1k],
			["B_AssaultPack_dgtl", nil,_1k],
			["B_AssaultPack_khk", nil,_1k],
			["B_AssaultPack_mcamo", nil,_1k],
			["B_AssaultPack_ocamo", nil,_1k],
			["B_AssaultPack_rgr", nil,_1k],
			["B_Kitbag_mcamo",nil,_5k],
			["B_TacticalPack_oli",nil,_3500],
			["B_FieldPack_ocamo",nil,_3k],
			["B_FieldPack_blk",nil,_3k],
			["B_Bergen_Base", nil,_1k],
			["B_Bergen_sgg",nil,_5k],
			["B_Kitbag_cbr",nil,_5k],
			["B_Carryall_oli",nil,_5k],
			["B_Carryall_khk",nil,_5k] ,
			["B_Parachute",nil,_25k]
		];
	};
};
_ret;