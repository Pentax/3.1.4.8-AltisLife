/*
	File: fn_virt_shops.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Config for virtual shops.
*/
private["_shop"];
_shop = _this select 0;

switch (_shop) do
{
	case "meat":{["Butcher's Shop",["hen","rooster","sheep","goat","dog","adog","rabbit","henC","roosterC","sheepC","goatC","dogC","adogC","rabbitC"]]};
	case "grub":{["Grill Market",["water","mtdew","burgers","gyros","pizza","chips","henC","roosterC","sheepC","goatC","dogC","adogC","rabbitC"]]};
	case "bus":{["Bus Tickets",["transporter"]]};
	case "gang": {["Gang Market", ["transporter","water","apple","peach","redgull","lockpick","pickaxe","fuelF","blastingcharge","boltcutter","kidney","scalpel","bloodbag","ivkit","organttk","demolitioncharge"]]};
	case "market": {["Convenience Store",["transporter","water","redgull","mtdew","pizza","gyros","chips","bloodbag","ivkit","pickaxe","lockpick","fuelF","knife","campfire"]]};
	case "rebel": {["Rebel Items",["transporter","water","mtdew","redgull","burgers","gyros","pizza","chips","lockpick","pickaxe","fuelF","boltcutter","blastingcharge","scalpel","bloodbag","ivkit","organttk","underwatercharge","demolitioncharge","bombdetect","knife","campfire"]]};
	case "organ": {["Medical Supplies",["kidney","scalpel","bloodbag","ivkit","organttk"]]};
	case "wongs": {["Wong's Food Cart",["turtlesoup","turtle"]]};
	case "coffee": {["Altis Coffee Club",["coffee","donuts"]]};
	case "heroin": {["Drug Dealer",["cocainep","heroinp","marijuana","methp","krokp"]]};
	case "oil": {["Oil Trader",["oilp","pickaxe","fuelF"]]};
	case "fishmarket": {["Fishmarket",["chips","salema","ornate","mackerel","mullet","tuna","catshark","turtle","knife"]]};
	case "farmer": {["Farmer's Market",["berries","apple","peach"]]};
	case "glass": {["Altis Glass Dealer",["glass","bottles"]]};
	case "iron": {["Altis Industrial Trader",["iron_r","copper_r"]]};
	case "diamond": {["Diamond Dealer",["diamond","diamondc"]]};
	case "salt": {["Salt Dealer",["salt_r"]]};
	case "gold": {["Curly's Gold",["goldbar"]]};
	case "cop": {["Cop Item Shop",["transporter","kidney","bloodbag","ivkit","donuts","coffee","redgull","spikeStrip","water","rabbit","apple","peach","redgull","fuelF","defusekit","lockpick","civdefusekit","bombdetect","knife"]]};
	case "ems": {["EMS Supplies",["kidney","organttk","ivkit","bloodbag","scalpel","water","apple","peach","redgull","lockpick","mtdew","pizza","chips"]]};
	case "cement": {["Cement Dealer",["cement"]]};
	case "speakeasy": {["Liquor Lounge",["bottledwhiskey","bottledshine","bottledbeer","moonshine"]]};
	case "Prospector": {["Gold Ingot Dealer",["goldbarp"]]};
	case "admin": {["Admin Market",["transporter","water","apple","peach","redgull","lockpick","pickaxe","fuelF","mtdew","burgers","gyros","pizza","chips","boltcutter","blastingcharge","scalpel","bloodbag","ivkit","organttk","underwatercharge","civdefusekit","bombdetect","knife","campfire"]]};
	
};

/*


		case "admin" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' OR shoptype='health' OR shoptype='food' OR shoptype='rebel' OR shoptype='market'",_data];};
		case "market" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' OR shoptype='utility' OR shoptype='health' OR shoptype='food' ",_data];};
		case "grub" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' OR shoptype='food'",_data];};
		case "organ" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' OR shoptype='health'",_data];};
		case "heroin" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1'",_data];};
		case "speakeasy" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1'",_data];};
		case "rebel" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' OR shoptype='market' OR shoptype='health' OR shoptype='utility'",_data];};
		case "fishmarket" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' OR shoptype='food' ",_data];};
		case "wongs" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' ",_data];};
		case "coffee" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' ",_data];};
		case "oil" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' ",_data];};
		case "cop" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' OR shoptype='market' OR shoptype='health' OR shoptype='utility'",_data];};
		case "ems" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' OR shoptype='health' OR shoptype='food' OR shoptype='utility'",_data];};
		case "diamond" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' ",_data];};
		case "iron" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' ",_data];};
		case "glass" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' ",_data];};
		case "salt" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' ",_data];};
		case "cement" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' ",_data];};
		case "Prospector" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1'",_data];};
		case "gold" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' ",_data];};
		case "gang" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' OR shoptype='rebel' OR shoptype='market' OR shoptype='health' OR shoptype='utility'",_data];};
		case "economy" :{ format["SELECT ressource, buyprice, sellprice FROM economy",_data];};
		default {"Error"};



*/