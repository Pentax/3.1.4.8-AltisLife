#include <macro.h>
if(__GETC__(life_donator) < 1) exitWith {hint "Sorry this area is not available";};

/*
	File: fn_clothing_bruce.sqf
	Author: Bryan "Tonic" Boardwine
	
	
	Description:
	Master configuration file for Donator Clothing Store.
*/
private["_filter"];
_filter = [_this,0,0,[0]] call BIS_fnc_param;
_free = (0 * life_discount);
_25 = (25 * life_discount);
_50 = (50 * life_discount);
_100 = (100 * life_discount);
_125 = (125 * life_discount);
_150 = (150 * life_discount);
_175 = (175 * life_discount);
_200 = (200 * life_discount);
_225 = (225 * life_discount);
_250 = (250 * life_discount);
_300 = (300 * life_discount);
_325 = (325 * life_discount);
_350 = (350 * life_discount);
_375 = (375 * life_discount);
_400 = (400 * life_discount);
_425 = (425 * life_discount);
_450 = (450 * life_discount);
_475 = (475 * life_discount);
_500 = (500 * life_discount);
_525 = (525 * life_discount);
_550 = (550 * life_discount);
_600 = (600 * life_discount);
_650 = (650 * life_discount);
_700 = (700 * life_discount);
_750 = (750 * life_discount);
_800 = (800 * life_discount);
_850 = (850 * life_discount);
_900 = (900 * life_discount);
_950 = (950 * life_discount);
_1k = (1000 * life_discount);
_1500 = (1500 * life_discount);
_2k = (2000 * life_discount);
_2500 = (2500 * life_discount);
_3k = (3000 * life_discount);
_3500 = (3500 * life_discount);
_5k = (5000 * life_discount);
_8k = (8000 * life_discount);
_10k = (10000 * life_discount);
_25k = (25000 * life_discount);
//Shop Title Name
ctrlSetText[3103,"Clothing Store"];
switch (_filter) do
{
	//Uniforms
	case 0:
	{
		[
		["U_C_Poloshirt_blue","Poloshirt Blue",_250],
		["U_C_Poloshirt_burgundy","Poloshirt Burgundy",_275],
		["U_C_Poloshirt_redwhite","Poloshirt Red/White",_150],
		["U_C_Poloshirt_salmon","Poloshirt Salmon",_175],
		["U_C_Poloshirt_stripped","Poloshirt stripped",_125],
		["U_C_Poloshirt_tricolour","Poloshirt Tricolor",_250],
		["U_C_Poor_2","Rag tagged clothes",_250],
		["U_IG_Guerilla2_2","Green stripped shirt & Pants",_500],
		["U_IG_Guerilla3_1","Brown Jacket & Pants",_500],
		["U_IG_Guerilla2_3","The Outback Rangler",_500],
		["U_C_HunterBody_grn","The Hunters Look",_1500],
		["U_C_WorkerCoveralls","Mechanic Coveralls",_2500],
		["U_OrestesBody","Surfing On Land",_1k],
		["U_NikosAgedBody","Business(WIP)",_1k],
		["U_C_Scientist","Scientist",_1k],
		["U_B_Protagonist_VR",nil,_1500],
		["U_O_Protagonist_VR",nil,_1500],
		["U_I_Protagonist_VR",nil,_1500],
		["U_I_GhillieSuit",nil,_1500],
		["U_OG_Guerilla1_1",nil,_1500],
		["U_OG_Guerilla2_1",nil,_1500],
		["U_O_Wetsuit",nil,_1500],
		["U_OG_Guerilla2_2",nil,_1500],
		["U_OG_Guerilla2_3",nil,_1500],
		["U_O_GhillieSuit",nil,_1500],
		["U_O_OfficerUniform_ocamo",nil,_1500],
		["U_OG_Guerilla3_1",nil,_1500],
		["U_OG_Guerilla3_2",nil,_1500],
		["U_I_G_Story_Protagonist_F",nil,_1500],
		["U_I_OfficerUniform",nil,_1500]
		];
	};
	
	//Hats
	case 1:
	{
		[	
			["H_StrawHat","Straw Fedora",_150],
			["H_BandMask_blk","Hat & Bandanna",_150],
			["H_Booniehat_tan",nil,_150],
			["H_Hat_blue",nil,_150],
			["H_Hat_brown",nil,_150],
			["H_Hat_checker",nil,_150],
			["H_Hat_grey",nil,_150],
			["H_Hat_tan",nil,_150],
			["H_Cap_blu",nil,_150],
			["H_Cap_grn",nil,_150],
			["H_Cap_grn_BI",nil,_150],
			["H_Cap_oli",nil,_150],
			["H_Cap_red",nil,_150],
			["H_Cap_tan",nil,_150],
			["H_Booniehat_dgtl",nil,_150],
			["H_Booniehat_khk",nil,_150],
			["H_ShemagOpen_tan",nil,_150],
			["H_Shemag_olive",nil,_150],
			["H_HelmetO_ocamo",nil,_150],
			["H_HelmetIA_net",nil,_150],
			["H_HelmetSpecB",nil,_150],
			["H_HelmetCrew_O",nil,_150],
			["H_HelmetCrew_I",nil,_150],
			["H_HelmetB_light_snakeskin",nil,_150],
			["H_PilotHelmetFighter_B",nil,_1500],
			["H_PilotHelmetFighter_I",nil,_1500]
		];
	};
	
	//Glasses
	case 2:
	{
		[
			["H_Bandanna_camo","Camo Bandanna",_150],
			["H_Bandanna_surfer","Surfer Bandanna",_150],
			["H_Bandanna_gry","Grey Bandanna",_150],
			["H_Bandanna_cbr",nil,_150],
			["H_Bandanna_khk","Khaki Bandanna",_150],
			["H_Bandanna_sgg","Sage Bandanna",_150],
			["G_Diving",nil,_750],
			["G_Bandanna_beast",nil,_750],
			["G_Bandanna_tan",nil,_750],
			["G_Bandanna_sport",nil,_750],
			["G_Bandanna_blk",nil,_750],
			["G_Bandanna_oli",nil,_750],
			["G_Bandanna_shades",nil,_750],
			["G_Bandanna_aviator",nil,_750],
			["G_Balaclava_blk",nil,_750],
			["G_Balaclava_lowprofile",nil,_750],
			["G_Balaclava_combat",nil,_750],
			["G_Balaclava_oli",nil,_750],
			["G_Shades_Black",nil,_750],
			["G_Shades_Blue",nil,_750],
			["G_Sport_Blackred",nil,_750],
			["G_Sport_Checkered",nil,_750],
			["G_Sport_Blackyellow",nil,_750],
			["G_Sport_BlackWhite",nil,_750],
			["G_Squares",nil,_750],
			["G_Aviator",nil,_750],
			["G_Lady_Mirror",nil,_750],
			["G_Lady_Dark",nil,_750],
			["G_Lady_Blue",nil,_750],
			["G_Lowprofile",nil,_750],
			["G_Combat",nil,_750]
		];
	};
	
	//Vest
	case 3:
	{
		[
			["V_RebreatherB",nil,_2k],
			["V_RebreatherIR",nil,_2k],
			["V_TacVest_khk",nil,_2500],
			["V_Chestrig_khk",nil,_2500],
			["V_PlateCarrierIA1_dgtl",nil,_2500],
			["V_BandollierB_cbr",nil,_2500],
			["V_HarnessO_brn",nil,_2500],
			["V_PlateCarrierL_CTRG",nil,_2500],
			["V_I_G_resistanceLeader_F",nil,_2500],
			["V_BandollierB_khk",nil,_2500],
			["V_TacVest_brn",nil,_2500],
			["V_Chestrig_blk",nil,_2500]
		];
	};
	
	//Backpacks
	case 4:
	{
		[
			["B_AssaultPack_cbr",nil,_2500],
			["B_AssaultPack_blk",nil,_2500],
			["B_Kitbag_mcamo",nil,_3500],
			["B_TacticalPack_oli",nil,_3500],
			["B_FieldPack_ocamo",nil,_3500],
			["B_FieldPack_blk",nil,_3500],
			["B_Bergen_sgg",nil,_3500],
			["B_Kitbag_cbr",nil,_3500],
			["B_Carryall_oli",nil,_5k],
			["B_Carryall_khk",nil,_5k],
			["B_Parachute",nil,_25k]
		];
	};
};