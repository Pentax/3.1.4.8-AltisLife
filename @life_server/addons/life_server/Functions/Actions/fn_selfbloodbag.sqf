/*
	File: fn_selfbloodbag.sqf
	author: [midgetgrimm]
	Description:
	Self bloodbag to heal.....
*/
if(player != vehicle player) exitWith {hint "You must exit your vehicle to bloodbag"};
life_interrupted = false;
if(life_action_inUse) exitWith {};
if((player getVariable ["Escorting",FALSE])) exitWith {};
if((player getVariable ["Transporting",FALSE])) exitWith {};
if((player getVariable ["restrained",FALSE])) exitWith {};
if((damage player) < 0.01) exitWith {};
if(life_inv_ivkit < 1) exitWith {hint "You need an IV to start blood transfusion"};
if(life_inv_bloodbag < 1) exitWith {hint "You need a bloodbag to heal..."};

if(isNil "life_ivkit_uses") then {life_ivkit_uses = 0;};
player setVariable["bloodBagged",false,true];
//hint "You start your own blood transfusion";
player setVariable["bloodBagged",true,true];
life_action_inUse = true;


if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
		//[[player,"AinvPknlMstpSnonWnonDnon_medic_1"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
		player switchMove "AinvPknlMstpSnonWnonDnon_medic_1";
		player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
	};
sleep 6;
if(!alive player OR life_istazed) exitWith {life_action_inUse = false;player setVariable["bloodBagged",false,true];};
if(life_interrupted) exitWith {life_interrupted = false; titleText["Action cancelled","PLAIN"]; life_action_inUse = false;player setVariable["bloodBagged",false,true];};
player setdamage 0; 
player setFatigue 0;
hint "Transfusion was successful";
life_thirst = 100;
life_hunger = 100;
//life_blood = 100;
life_action_inUse = false;
player setVariable["bloodBagged",false,true];
life_inv_bloodbag = life_inv_bloodbag - 1;
life_ivkit_uses = life_ivkit_uses + 1;
if(life_ivkit_uses >= 3) then {
	life_inv_ivkit = life_inv_ivkit - 1;
	hint "You have used up an IV Starter Kit";
	life_ivkit_uses = 0;
};