/*
	File: fn_replaceOrgan.sqf
	author: [midgetgrimm]
	Description:
	Replaces stolen organ
*/
if(player != vehicle player) exitWith {hint "You must exit your vehicle to transplant";};
life_interrupted = false;
if(life_action_inUse) exitWith {};
if((player getVariable ["Escorting",FALSE])) exitWith {};
if((player getVariable ["Transporting",FALSE])) exitWith {};
if((player getVariable ["restrained",FALSE])) exitWith {};
if(!(player getVariable["missingOrgan",false])) exitWith {hint "Silly, you aren't missing any organs";};
if(life_inv_kidney < 1 ) exitWith {hint "You need a kidney to transplant"};
if(life_inv_organttk < 1) exitWith {hint "You need a transplant kit"};
//hint "You start your own organ transplant";
life_action_inUse = true;

if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
		//[[player,"AinvPknlMstpSnonWnonDnon_medic_1"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
		player switchMove "AinvPknlMstpSnonWnonDnon_medic_1";
		player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
	};
sleep 6;

if(!alive player OR life_istazed) exitWith {life_action_inUse = false;};
if(life_interrupted) exitWith {life_interrupted = false; titleText["Action cancelled","PLAIN"]; life_action_inUse = false;};
player setVariable["missingOrgan",false,true];
player setFatigue 0.5;
hint "Transplant was successful";
life_thirst = 90;
life_hunger = 90;
//life_blood = 100;
life_action_inUse = false;
life_inv_kidney = life_inv_kidney - 1;
life_inv_organttk  = life_inv_organttk  - 1;
