/*
	File: fn_organTransplantKit.sqf
	Author: Bryan "Tonic" Boardwine
	edited by:[midgetgrimm]
	Description:
	gives organ to other players
*/
private["_curTarget","_distance","_isVehicle","_title","_progressBar","_cP","_titleText","_badDistance"];
_curTarget = cursorTarget;
life_interrupted = false;
if(life_action_inUse) exitWith {};
if(isNull _curTarget) exitWith {}; //Bad type
_distance = ((boundingBox _curTarget select 1) select 0) + 2;
if(player distance _curTarget > _distance) exitWith {}; //Too far
if(player != vehicle player) exitWith {hint "You must exit your vehicle to give transplant";};
if(_curTarget != vehicle _curTarget) exitWith {hint "You cannot perform this operation while patient is in a vehicle"};
//More error checks
if(isNull _curTarget) exitWith {}; //if unit is null, than NO
if(!(_curTarget getVariable ["missingOrgan",FALSE])) exitWith {hint"They aren't missing any organs";};
if((_curTarget getVariable ["inSurgery",FALSE])) exitWith {hint"Only one transplant at a time please";};
if((player getVariable ["hasOrgan",FALSE])) exitWith {hint"You can only perform surgery so many times...";};
if(player == _curTarget) exitWith {};//if the thief is the cursor target(dafuq) than NO
if(!isPlayer _curTarget) exitWith {};//if the cursor target is not a player than NO
if(life_inv_organttk < 1) exitWith {hint "You need an Organ Transplant Kit"};
if(life_inv_kidney < 1) exitWith {hint "You need a kidney to transplant"};

_title = format["Performing organ transplant on %1",name _curTarget];
life_action_inUse = true; //Lock out other actions

//Setup the progress bar
disableSerialization;
5 cutRsc ["life_progress","PLAIN"];
_ui = uiNamespace getVariable "life_progress";
_progressBar = _ui displayCtrl 38201;
_titleText = _ui displayCtrl 38202;
_titleText ctrlSetText format["%2 (1%1)...","%",_title];
_progressBar progressSetPosition 0.01;
_cP = 0.008;

while {true} do
{
	if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
		//[[player,"AinvPknlMstpSnonWnonDnon_medic_1"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
		player switchMove "AinvPknlMstpSnonWnonDnon_medic_1";
		player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
	};
	sleep 0.26;
	if(isNull _ui) then {
		5 cutRsc ["life_progress","PLAIN"];
		_ui = uiNamespace getVariable "life_progress";
		_progressBar = _ui displayCtrl 38201;
 	_titleText = _ui displayCtrl 38202;
	};
	_cP = _cP + 0.009;
	_progressBar progressSetPosition _cP;
	_titleText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_title];
	_curTarget setVariable["inSurgery",true,true];
	player setVariable["hasOrgan",true,true];
	if(_cP >= 1 OR !alive player) exitWith {};
	if(life_istazed) exitWith {_curTarget setVariable["inSurgery",false,true];player setVariable["hasOrgan",false,true];}; //Tazed
	if(life_interrupted) exitWith {_curTarget setVariable["inSurgery",false,true];player setVariable["hasOrgan",false,true];};
	if(player distance _curTarget > _distance) exitWith {_badDistance = true;player setVariable["hasOrgan",false,true];player setVariable["inSurgery",false,true];};
};

//Kill the UI display and check for various states
5 cutText ["","PLAIN"];
player playActionNow "stop";
if(!alive player OR life_istazed) exitWith {life_action_inUse = false;_curTarget setVariable["inSurgery",false,true];player setVariable["hasOrgan",false,true];};
if(!isNil "_badDistance") exitWith {titleText["You got to far away from the target.","PLAIN"]; life_action_inUse = false;_curTarget setVariable["inSurgery",false,true];player setVariable["hasOrgan",false,true];};
if(life_interrupted) exitWith {life_interrupted = false; titleText["Action cancelled","PLAIN"]; life_action_inUse = false;_curTarget setVariable["inSurgery",false,true];player setVariable["hasOrgan",false,true];};
hint "Transplant surgery was successful";
_curTarget setVariable["inSurgery",false,true];
_curTarget setVariable["missingOrgan",false,true];

life_inv_organttk = life_inv_organttk - 1;
life_inv_kidney = life_inv_kidney - 1;
life_action_inUse = false;
if(playerSide in [west,independent]) then { life_OnHandCash =  life_OnHandCash + 1000;hint"You have received $1000 cash for your services";};
player setVariable["hasOrgan",false,true];