/*
	File: fn_lockpick.sqf doubling as a scalpel trick mixed with fn_takeOrgan/fn_hasOrgans
	Author: Bryan "Tonic" Boardwine
	edited by:[midgetgrimm]
	Description:
	organ theft
*/
private["_curTarget","_distance","_isVehicle","_title","_progressBar","_cP","_titleText","_dice","_badDistance","_oopsie"];
_curTarget = cursorTarget;
_oopsie = round(random 10);
if(player != vehicle player) exitWith {hint "You must exit your vehicle to take an organ"};
if(_curTarget != vehicle _curTarget) exitWith {hint "You cannot perform this operation while in a vehicle"};
life_interrupted = false;
if(life_action_inUse) exitWith {};
if(isNull _curTarget) exitWith {}; //Bad type
_distance = ((boundingBox _curTarget select 1) select 0) + 2;
if(player distance _curTarget > _distance) exitWith {}; //Too far

//More error checks
//if(!_isVehicle && !isPlayer _curTarget) exitWith {};
if(!(_curTarget getVariable["restrained",false])) exitWith {};
//mychecks
if(playerSide in [west,independent]) exitWith {hint"That's not the behavior of an official";};
if(isNull _curTarget) exitWith {}; //if unit is null, than NO
if((_curTarget getVariable ["missingOrgan",FALSE])) exitWith {hint"They have no organs to take";};
if((_curTarget getVariable ["inSurgery",FALSE])) exitWith {hint"They only have so many kidneys...relax";};
if((player getVariable ["hasOrgan",FALSE])) exitWith {hint"You can only perform surgery so many times...cooldown";};
if(player == _curTarget) exitWith {};//if the thief is the cursor target(dafuq) than NO
if(!isPlayer _curTarget) exitWith {};//if the cursor target is not a player than NO
if(life_inv_scalpel < 1 && life_inv_knife < 1) exitWith {hint "You need a knife to do some cuttin..."};
if(life_inv_ivkit < 1) exitWith {hint "You need a Starter IV Kit for the blood transfusion"};
if(life_inv_bloodbag < 1) exitWith {hint "You need a bloodbag to do some cuttin, can't have them die on us..."};
if (_oopsie == 2) exitWith {
								
								5 cutText ["","PLAIN"];
								_curTarget setVariable["restrained",false,true];
								_curTarget setVariable["Escorting",false,true];
								_curTarget setVariable["transporting",false,true];
								_curTarget setVariable["inSurgery",false,true];
								_curTarget setVariable["missingOrgan",false,true];
								detach _curTarget;
								_curTarget say3D "cuff";
								player say3D "cuff";
								[[0,format["News Report: Wannabe surgeon %1 tried to cut %2 and take his kidney, but they broke free and kicked his ass",name player, name _curTarget]],"life_fnc_broadcast",nil,false] spawn life_fnc_MP;
								[[getPlayerUID player,player getVariable["realname",name player],"919A"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
								titleText[format["HAHA - %1 broke free and knocked you out...",name _curTarget],"PLAIN"];
								player playMoveNow "Incapacitated";
								_obj = "Land_ClutterCutter_small_F" createVehicle (getPosATL player);
								_obj setPosATL (getPosATL player);
								player attachTo [_obj,[0,0,0]];
								sleep 45;
								player playMoveNow "amovppnemstpsraswrfldnon";
								detach player;
								deleteVehicle _obj;
								player setVariable["hasOrgan",false,true];
							};
if(isNil "life_ivkit_uses") then {life_ivkit_uses = 0;};
if(isNil "life_scalpel_uses") then {life_scalpel_uses = 0;};
_title = format["Bloodbagging and stealing the organs of %1",name _curTarget];
life_action_inUse = true; //Lock out other actions

//Setup the progress bar
disableSerialization;
5 cutRsc ["life_progress","PLAIN"];
_ui = uiNamespace getVariable "life_progress";
_progressBar = _ui displayCtrl 38201;
_titleText = _ui displayCtrl 38202;
_titleText ctrlSetText format["%2 (1%1)...","%",_title];
_progressBar progressSetPosition 0.01;
_cP = 0.007;

while {true} do
{
	if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
		//[[player,"AinvPknlMstpSnonWnonDnon_medic_1"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
		player switchMove "AinvPknlMstpSnonWnonDnon_medic_1";
		player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
	};
	sleep 0.26;
	if(isNull _ui) then {
		5 cutRsc ["life_progress","PLAIN"];
		_ui = uiNamespace getVariable "life_progress";
		_progressBar = _ui displayCtrl 38201;
 	_titleText = _ui displayCtrl 38202;
	};
	_cP = _cP + 0.008;
	_progressBar progressSetPosition _cP;
	_titleText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_title];
	_curTarget setVariable["inSurgery",true,true];
	player setVariable["hasOrgan",true,true];
	if(_cP >= 1 OR !alive player) exitWith {};
	if(life_istazed) exitWith {_curTarget setVariable["inSurgery",false,true];player setVariable["hasOrgan",false,true];}; //Tazed
	if(life_interrupted) exitWith {_curTarget setVariable["inSurgery",false,true];player setVariable["hasOrgan",false,true];};
	if((player getVariable["restrained",false])) exitWith {};
	if(player distance _curTarget > _distance) exitWith {_badDistance = true;};
};

//Kill the UI display and check for various states
5 cutText ["","PLAIN"];
player playActionNow "stop";
if(!alive player OR life_istazed) exitWith {life_action_inUse = false;_curTarget setVariable["inSurgery",false,true];};
if((player getVariable["restrained",false])) exitWith {life_action_inUse = false;_curTarget setVariable["inSurgery",false,true];};
if(!isNil "_badDistance") exitWith {titleText["You got to far away from the target.","PLAIN"]; life_action_inUse = false;_curTarget setVariable["inSurgery",false,true];};
if(life_interrupted) exitWith {life_interrupted = false; titleText["Action cancelled","PLAIN"]; life_action_inUse = false;_curTarget setVariable["inSurgery",false,true];};
_curTarget setVariable["missingOrgan",true,true];
//[true,"kidney",1] call life_fnc_handleInv;//put stolen kidney into inventory of thief
//[false,"scalpel",1] call life_fnc_handleInv;
hint "Bloodbag and surgery was successful";
life_inv_bloodbag = life_inv_bloodbag - 1;
life_inv_kidney = life_inv_kidney + 1;
life_action_inUse = false;
[[0,format["News Report: Suspect %1 has cut out a kidney from victim %2.",name player,name _curTarget]],"life_fnc_broadcast",nil,false] spawn life_fnc_MP;
[[getPlayerUID player,player getVariable["realname",name player],"919"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
_curTarget setVariable["inSurgery",false,true];
life_scalpel_uses = life_scalpel_uses + 1;
if(life_scalpel_uses >= 2) then {
	life_inv_scalpel = life_inv_scalpel - 1;
	hint "Your scalpel is no longer sharp..";
	life_scalpel_uses = 0;
};
life_ivkit_uses = life_ivkit_uses + 1;
if(life_ivkit_uses >= 3) then {
	life_inv_ivkit = life_inv_ivkit - 1;
	hint "You have used up an IV Starter Kit";
	life_ivkit_uses = 0;
};
sleep 300;
player setVariable["hasOrgan",false,true];