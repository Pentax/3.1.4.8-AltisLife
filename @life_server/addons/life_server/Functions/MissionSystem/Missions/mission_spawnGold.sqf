private["_marker","_markerText","_units","_grp","_unitsDeadTimeout","_missionTimeout","_startTime","_currTime","_result","_weapon"];
// Settings
_missionTimeout = (60 * 20); // set to 45 or 60 minutes minutes
//_unitsDeadTimeout = (60 * 3); // 3 minutes after the units died it will timeout
_unitsDeadTimeout = (60 * 10);// set to 20minutes
//Create gold vehicle wreck 
heli = "Land_UWreck_MV22_F" createVehicle (["mrkGreen",2,["mrkRed_1","mrkRed_2","mrkRed_3"]] call SHK_pos);
heli setPosASLW [(position heli) select 0, (position heli) select 1, -5.2];
heli enableSimulation false;
 
wreck = "Land_UWreck_FishingBoat_F" createVehicle (getPos heli);
wreck SetPosATL [(getPos heli select 0)-200*sin(round(random 359)),(getPos heli select 1)-200*cos(round(random 359)), 0];
wreck enableSimulation false;
gold_safe = "Land_Cargo20_blue_F" createVehicle (getPos wreck);
clearItemCargoGlobal gold_safe;
clearMagazineCargoGlobal  gold_safe;
clearWeaponCargoGlobal gold_safe;
gold_safe attachTo [wreck,[3,4,-0.4]];
gold_safe setVectorDirAndUp [[90,0,80],[-90,0,0]];
gold_safe setVariable["gold",5 + round(random 4),true];
gold_safe setVariable["gold_open",false,true];
gold_safe enableSimulation false;
gold_safe allowDamage false;
publicVariable "heli";
publicVariable "wreck";
publicVariable "gold_safe";
/*ADDING OF AI*/
_maxUnits = 2 + round(random 2);
_units = [];
_grp = createGroup independent;
_grp allowFleeing 0; // Fucking pussies
_grp setBehaviour "STEALTH";
_grp setCombatMode "RED";
for [{ _i = 0 },{ _i != _maxUnits},{_i = _i + 1}] do {
	// Create unit
	_temp = _grp createUnit ["O_Soldier_M_F",getPos heli,[],15,"NONE"];
	// Make the soldier join resistance. Necessary, otherwise they start shooting eachother.
	[_temp] join _grp;
	removeAllWeapons _temp;
	removeUniform _temp;
	removeHeadgear _temp;
	_temp setUnitAbility 1;
	_temp setSkill ["aimingAccuracy",0.7];
	_temp setSkill ["aimingShake",0.60];
	_temp setSkill ["aimingSpeed",0.65];
	_temp setSkill ["endurance",0.75];
	_temp setSkill ["spotDistance",0.75];
	_temp setSkill ["spotTime",0.75];
	_temp setSkill ["courage",0.75];
	_temp setSkill ["reloadSpeed",0.75];
	_temp setSkill ["commanding",0.75];
	_temp setSkill ["general",0.75];
	_units pushBack _temp;
	
	_temp addWeapon "arifle_SDAR_F";
	_temp addMagazine "20Rnd_556x45_UW_mag";
	_temp addMagazine "20Rnd_556x45_UW_mag";
	_temp addMagazine "20Rnd_556x45_UW_mag";
	_temp addMagazine "20Rnd_556x45_UW_mag";
	_temp addMagazine "20Rnd_556x45_UW_mag";
	_temp addGoggles "G_Diving";
	_temp addVest "V_RebreatherIR";
	_temp addItem "ItemGPS";
	_temp assignItem "ItemGPS";
	_temp forceAddUniform "U_O_Wetsuit";
};

_marker = createMarker ["Marker200", heli];
_marker setMarkerColor "ColorOrange";
_marker setMarkerType "Empty";
_marker setMarkerShape "ELLIPSE";
_marker setMarkerSize [500,500];
_markerText = createMarker ["MarkerText200", heli];
_markerText setMarkerColor "ColorBlack";
_markerText setMarkerText "Shipwreck";
_markerText setMarkerType "mil_warning";
[[5,"<t size='3'><t color='#00FF00'>SHIPWRECK</t></t> <br/><t size='1.5'>A ship wreck site has been spotted, check the area for <t color='#FFFF00'>underwater crates of gold</t>. Check your map</t>"],"life_fnc_broadcast",true,false] spawn life_fnc_MP; 


_startTime = floor(time);
waitUntil {
	sleep 1;
	_currTime = floor(time);
	
	_result = 0;
	if(_currTime - _startTime >= _missionTimeout) then {_result = 1;};
	if (_result == 0) then {
		_unitsAlive = ({alive _x} count units _grp);
		
		if (_unitsAlive == 0) then {
			_hint = "<t size='1'><t color='#00FF00'>Shipwreck has been secured</t></t>";
			[[5,_hint],"life_fnc_broadcast",true,false] spawn life_fnc_MP;
			diag_log "==============Success AI  Mission===================";
			
			_nearUnits = nearestObjects[getPos wreck, ["Man"], 500];
			_nearUnitsAlive = [];
			{
				if (alive _x) then {
					_nearUnitsAlive pushBack _x;
				};
			} forEach _nearUnits;
			//[[30],"life_fnc_timer",_nearUnitsAlive,false] spawn life_fnc_MP;
			sleep _unitsDeadTimeout;
			_result = 2;
		};
	};
	
	// These are just the timeouts for safety.
	// TODO: Actual box check.
	
	(_result > 0);
};

// The mission ended for whatever reason. The _result now contains how it ended. TODO, obviously.
_hint = "";
switch (_result) do
{
	case 1:
	{
		_hint = "<t align='center'>The Shipwreck has been cleaned up and the units have been extracted. Better luck next time</t>";
		diag_log "==============AI never battled==============";
	};
	case 2:
	{
		_hint = "<t align='center'>The Shipwreck has been cleaned up</t>";
		diag_log "==============Loot was grabbed==============";
	};
};

// Cleanup
{
	deleteVehicle _x;
} forEach _units;
/*
detach gold_safe;
gold_safe setPos [8179.064,25089.068,0.1];
gold_safe setVariable["gold_open",false,true];*/
deleteVehicle gold_safe;
deleteVehicle wreck;
deleteGroup _grp;
deleteVehicle heli;
deleteMarker _marker;
deleteMarker _markerText;
diag_log "Deleted shipwreck markers";
[[5,_hint],"life_fnc_broadcast",true,false] spawn life_fnc_MP;