/*	File: mission_helicopterCrash.sqf	Author: Lars 'Levia' Wesselius */
private["_marker","_markerText","_units","_grp","_unitsDeadTimeout","_missionTimeout","_startTime","_currTime","_result","_weapon"];
// Settings
_missionTimeout = (60 * 45); // 30 minutes
//_unitsDeadTimeout = (60 * 3); // 3 minutes after the units died it will timeout
_unitsDeadTimeout = (60 * 20);
arrayCrash = ["Land_Wreck_Heli_Attack_02_F","Land_Wreck_Plane_Transport_01_F","Land_UWreck_MV22_F","Land_UWreck_Heli_Attack_02_F","Plane_Fighter_03_wreck_F","Land_Wreck_Heli_Attack_01_F"];
heliCrashType = arrayCrash select floor random count arrayCrash;
heliCrash = heliCrashType createVehicle (["mrkGreen",0,["heliGold_1","heliGold_2","heliGold_3"]] call SHK_pos);
heliCrash enableSimulation false;
/*
//heli_gold attachTo [heliCrash,[10,-1,-4]];
//heli_gold setVectorDirAndUp [[90,0,0],[0,0,1]];
heli_gold setPos [ (getPos heliCrash select 0) + 10, (getPos heliCrash select 1) - 4,  (getPos heliCrash select 2) - 1.1];
heli_gold enableSimulation false;
heli_gold allowDamage false;
*/
heli_gold = "Box_East_AmmoVeh_F" createVehicle (getPos heliCrash);
clearItemCargoGlobal heli_gold;
clearMagazineCargoGlobal  heli_gold;
clearWeaponCargoGlobal heli_gold;
heli_gold setVariable["heligold",175 + round(random 100),true];
heli_gold setVariable["heligold_open",false,true];
heli_gold enableSimulation false;
heli_gold allowDamage false;
arraySmug = ["life_inv_diamondr","life_inv_krokp","life_inv_methp","life_inv_marijuana","life_inv_heroinp"];
randomSmug = arraySmug select floor random count arraySmug;
diag_log format["Random Smuggler Drug: %1",randomSmug];
publicVariable "heliCrash";
publicVariable "heli_gold";
publicVariable "randomSmug";
// Spawn 3 units + a random number between 0 and 3.
_maxUnits = 6 + round(random 3);
_units = [];
_grp = createGroup independent;
_grp allowFleeing 0; // Fucking pussies
_grp setBehaviour "STEALTH";
_grp setCombatMode "RED";
for [{ _i = 0 },{ _i != _maxUnits},{_i = _i + 1}] do {
	// Create unit
	_temp = _grp createUnit ["O_Soldier_M_F",getPos heliCrash,[],15,"NONE"];
	// Make the soldier join resistance. Necessary, otherwise they start shooting eachother.
	[_temp] join _grp;
	removeAllWeapons _temp;
	removeUniform _temp;
	removeHeadgear _temp;
	_temp setSkill ["aimingAccuracy",0.7];
	_temp setSkill ["aimingShake",0.60];
	_temp setSkill ["aimingSpeed",0.65];
	_temp setSkill ["endurance",0.75];
	_temp setSkill ["spotDistance",0.75];
	_temp setSkill ["spotTime",0.75];
	_temp setSkill ["courage",0.75];
	_temp setSkill ["reloadSpeed",0.75];
	_temp setSkill ["commanding",0.75];
	_temp setSkill ["general",0.75];
	_units pushBack _temp;
	
	// Random weapon set.
	_r = round(random(100));
	
	if (_r <= 40) then {
		// 40% chance to get this set.
		_weapon = [_temp, "arifle_mas_m16", 6] call BIS_fnc_addWeapon;
		_temp addVest "V_PlateCarrierIA1_dgtl";
		_temp addItem "NVGoggles_OPFOR";
		_temp assignItem "NVGoggles_OPFOR";
		_temp addItem "ItemGPS";
		_temp assignItem "ItemGPS";
		_temp forceAddUniform "U_IG_Guerilla2_1";
		
	} else {
		
		if (_r > 40 && _r < 75) then {
		
			_weapon = [_temp, "arifle_mas_g3", 6] call BIS_fnc_addWeapon;
			_temp addVest "V_PlateCarrierIA1_dgtl";
			_temp addItem "NVGoggles_OPFOR";
			_temp assignItem "NVGoggles_OPFOR";
			_temp addItem "ItemGPS";
			_temp assignItem "ItemGPS";
			_temp forceAddUniform "U_IG_Guerilla1_1";
			
		} else {
			if (_r >= 75) then {
				// 10% chance to get this set.
			_weapon = [_temp, "arifle_mas_fal", 6] call BIS_fnc_addWeapon;
			_temp addVest "V_PlateCarrierIA1_dgtl";
			_temp addItem "NVGoggles_OPFOR";
			_temp assignItem "NVGoggles_OPFOR";
			_temp addItem "ItemGPS";
			_temp assignItem "ItemGPS";
				_temp forceAddUniform "U_BG_leader";
			};
		};
	};
};

_marker = createMarker ["missionHelicopterCrash", heliCrash];
_marker setMarkerShape "ELLIPSE";
_marker setMarkerColor "ColorRed";
_marker setMarkerSize [500,500];
_marker setMarkerType "Empty";
_markerText = createMarker ["MarkerTextHeliCrash", heliCrash];
_markerText setMarkerColor "ColorBlack";
_markerText setMarkerText "Weapons Convoy";
_markerText setMarkerType "MinefieldAP";

[[5,"<t size='2'><t color='#00FF00'>SMUGGLERS</t></t> <br/><t size='1'>A Weapons Convoy have crash landed and is protected by soldiers. Check your map</t>"],"life_fnc_broadcast",true,false] spawn life_fnc_MP;

_startTime = floor(time);
waitUntil {
	sleep 1;
	_currTime = floor(time);
	
	_result = 0;
	if(_currTime - _startTime >= _missionTimeout) then {_result = 1;};
	if (_result == 0) then {
		_unitsAlive = ({alive _x} count units _grp);
		
		if (_unitsAlive == 0) then {
			_hint = "<t size='1'><t color='#00FF00'>Weapons Convoy wreck has been secured</t></t>";
			[[5,_hint],"life_fnc_broadcast",true,false] spawn life_fnc_MP;
			diag_log "==============Success AI  Mission===================";
			
			_nearUnits = nearestObjects[getPos heliCrash, ["Man"], 500];
			_nearUnitsAlive = [];
			{
				if (alive _x) then {
					_nearUnitsAlive pushBack _x;
				};
			} forEach _nearUnits;
			sleep _unitsDeadTimeout;
			_result = 2;
		};
	};
	
	// These are just the timeouts for safety.
	// TODO: Actual box check.
	
	(_result > 0);
};

// The mission ended for whatever reason. The _result now contains how it ended. TODO, obviously.
_hint = "";
switch (_result) do
{
	case 1:
	{
		_hint = "<t align='center'>The Weapons Convoy wreck has been cleaned up and the units have been extracted. Better luck next time</t>";
		diag_log "==============AI never battled==============";
	};
	case 2:
	{
		diag_log "==============Loot was grabbed==============";
	};
};

// Cleanup
{
	deleteVehicle _x;
} forEach _units;
/*//setPos [8182.2383,25061.299,44.337894];
detach heli_gold;
heli_gold setPos [8182.2383,25061.299,0.1];
heli_gold setVariable["heligold_open",false,true];*/
deleteGroup _grp;
deleteVehicle heliCrash;
deleteVehicle heli_gold;
deleteMarker _marker;
deleteMarker _markerText;
diag_log "Deletedsmuggler markers";
[[5,_hint],"life_fnc_broadcast",true,false] spawn life_fnc_MP;