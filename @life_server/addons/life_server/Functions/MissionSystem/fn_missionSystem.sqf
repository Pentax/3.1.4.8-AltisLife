/*
	File: fn_missionSystem.sqf
	Author: Lars 'Levia' Wesselius
	
	Description:
	Initializes the mission system and spawns missions periodically.
	Some of this stuff is borrowed from A3Wasteland GitHub, but mostly the concept. Don't reinvent the wheel, folks.
*/
// Settings
#define nextMissionDelay (30 * 60) // 60 minutes delay in between missions.
#define firstMissionDelay (10 * 60) // 30 minutes after the server's started the first mission will start.
//#define nextMissionDelay 20 // After the mission ends, wait for 5 seconds (debug mode).
//#define firstMissionDelay 30 // 10 seconds after the server's started the first mission will start (debug mode).
//
private["_availableMissions","_availableMissionWeights","_firstMission","_nextMission","_nextMissionName","_missionRunning"];
// Define missions here!
_availableMissions =
[
	["mission_helicopterCrash",1],
	//["mission_convoyCrash",1],
	["mission_spawnGold",1]
];
_availableMissionWeights = [];
{
	_availableMissionWeights set [_forEachIndex, if (count _x > 1) then { _x select 1 } else { 1 }];
	
	// Precompile missions for error checking
	compile preprocessFileLineNumbers format ["\life_server\Functions\MissionSystem\Missions\%1.sqf", _x select 0];
} forEach _availableMissions;

_firstMission = true;
while {true} do {
	if (_firstMission) then {
		sleep firstMissionDelay;
	};
	_firstMission = false;
	
	// Select next random mission
	_nextMission = [_availableMissions, _availableMissionWeights] call BIS_fnc_selectRandomWeighted;
	_nextMissionName = _nextMission select 0;
	
	// Start the mission
	diag_log format["========AI Mission started. File %1", _nextMissionName];
	_missionRunning = execVM format ["\life_server\Functions\MissionSystem\Missions\%1.sqf", _nextMissionName];
	
	// Wait until mission is over. The mission itself decides win/lose whatever conditions
	waitUntil{sleep 0.1; scriptDone _missionRunning};
	
	
	sleep nextMissionDelay;
};