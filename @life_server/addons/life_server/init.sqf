#define __CONST__(var1,var2) var1 = compileFinal (if(typeName var2 == "STRING") then {var2} else {str(var2)})
DB_Async_Active = false;
DB_Async_ExtraLock = false;
life_server_isReady = false;
publicVariable "life_server_isReady";

[] execVM "\life_server\functions.sqf";
[] execVM "\life_server\eventhandlers.sqf";
_extDB = false;
//Only need to setup extDB once.
if(isNil {uiNamespace getVariable "life_sql_id"}) then {
	life_sql_id = round(random(9999));
	__CONST__(life_sql_id,life_sql_id);
	uiNamespace setVariable ["life_sql_id",life_sql_id];
	//extDB Version
	_result = "extDB" callExtension "9:VERSION";
	diag_log format ["extDB: Version: %1", _result];
	if(_result == "") exitWith {};
	if ((parseNumber _result) < 14) exitWith {diag_log "Error: extDB version 14 or Higher Required"};
	//Initialize the database
	_result = "extDB" callExtension "9:DATABASE:Altis";
	if(_result != "[1]") exitWith {diag_log "extDB: Error with Database Connection";};
	_result = "extDB" callExtension format["9:ADD:DB_RAW_V2:%1",(call life_sql_id)];
	if(_result != "[1]") exitWith {diag_log "extDB: Error with Database Connection";};
	diag_log "extDB: Connected to Database";
	"extDB" callExtension "9:ADD:LOG:deathmessages";
	"extDB" callExtension "9:LOCK";
	_extDB = true;
} else {
	life_sql_id = uiNamespace getVariable "life_sql_id";
	__CONST__(life_sql_id,life_sql_id);
	_extDB = true;
	diag_log "extDB: Still Connected to Database";
};

//Broadbase PV to Clients, to warn about extDB Error.
//	exitWith to stop trying to run rest of Server Code
if (!_extDB) exitWith {
	life_server_extDB_notLoaded = true;
	publicVariable "life_server_extDB_notLoaded";
	diag_log "extDB: Error checked extDB/logs for more info";
};

//Run procedures for SQL cleanup on mission start.
["CALL resetLifeVehicles",1] spawn DB_fnc_asyncCall;
["CALL deleteDeadVehicles",1] spawn DB_fnc_asyncCall;
["CALL deleteGangsInactive",1] spawn DB_fnc_asyncCall;
["CALL deleteWInactive",1] spawn DB_fnc_asyncCall;
//Custom Content
fn_whoDunnit = compile preprocessFileLineNumbers "\life_server\Functions\Actions\fn_whoDunnit.sqf";
[] execVM "\life_server\Functions\MissionSystem\fn_missionSystem.sqf";
call compile preProcessFileLineNumbers "\life_server\SHK_pos\shk_pos_init.sqf";
//whitelistings
life_adminlevel = 0;
life_medicLevel = 0;
life_coplevel = 0;
life_swat = 0;
life_rebel = 0;
life_bountyHunter = 0;

west setFriend[east,1];
east setFriend[west,1];
independent setFriend[east,0];
east setFriend[independent,0];
independent setFriend[west,0];
west setFriend[independent,0];
//Null out harmful things for the server.
__CONST__(JxMxE_PublishVehicle,"No");

//[] execVM "\life_server\fn_initHC.sqf";

life_radio_west = radioChannelCreate [[0, 0.6, 1, 1], "Side Channel", "%UNIT_NAME", []];
life_radio_civ = radioChannelCreate [[1, 0, 1, 1], "Side Channel", "%UNIT_NAME", []];
life_radio_indep = radioChannelCreate [[1, 0.9, 0, 1], "Side Channel", "%UNIT_NAME", []];

serv_sv_use = [];

fed_bank setVariable["safe",6 + round(random 5),true];
addMissionEventHandler ["HandleDisconnect",{_this call TON_fnc_clientDisconnect; false;}]; //Do not second guess this, this can be stacked this way.
[] spawn TON_fnc_cleanup;
life_gang_list = [];
publicVariable "life_gang_list";
life_wanted_list = [];
client_session_list = [];

[] execFSM "\life_server\cleanup.fsm";

[] spawn
{
	private["_logic","_queue"];
	while {true} do
	{
		sleep (30 * 60);
		_logic = missionnamespace getvariable ["bis_functions_mainscope",objnull];
		_queue = _logic getvariable "BIS_fnc_MP_queue";
		_logic setVariable["BIS_fnc_MP_queue",[],TRUE];
	};
};

[] spawn TON_fnc_federalUpdate;
/*
diag_log "spawnjail";
[] call TON_fncspawnJail;
diag_log "endspawnjail";*/
["hunting_zone",50] spawn TON_fnc_huntingZone;

[] spawn
{
	while {true} do
	{
		sleep (60 * 60);
		{
			_x setVariable["sellers",[],true];
		} foreach [Dealer_1,Dealer_2,Dealer_3_1];
	};
};

//Strip NPC's of weapons
{
	if(!isPlayer _x) then {
		_npc = _x;
		{
			if(_x != "") then {
				_npc removeWeapon _x;
			};
		} foreach [primaryWeapon _npc,secondaryWeapon _npc,handgunWeapon _npc];
	};
} foreach allUnits;



//Lockup the dome
private["_dome","_rsb","_prison_wallholder","_prisonHolder"];
_dome = nearestObject [[10273.7,2168.1455,5.5],"Land_City_Gate_F"];
_rsb = nearestObject [[12429.118,14102.525,2.687788],"Land_Research_house_V1_F"];
_prison_wallholder = nearestObject[[3064.7214,11833.168,17.240364],"Land_BagFence_Corner_F"];
_prisonHolder = nearestObject[[16758.494,13633.904,9.3295164],"Land_Noticeboard_F"];
_dome setVariable["bis_disabled_Door_1",1,true];
_dome animate ["Door_1_rot",0];
_rsb setVariable["bis_disabled_Door_1",1,true];
_rsb allowDamage false;
_dome allowDamage false;
fed_bank attachTo[_rsb,[-0.1,3,0.8]];
prison_safe attachTo[_prison_wallholder,[3.9,0.85,1.75]]; 
prison_safe1 attachTo [_prison_wallholder,[3.7,1.8,0.57]];
prison_safe2 attachTo [_prisonHolder,[3,4,-0.4]];
life_server_isReady = true;
publicVariable "life_server_isReady";