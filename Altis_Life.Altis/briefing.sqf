waitUntil {!isNull player && player == player};
if(player diarySubjectExists "rules")exitwith{};

player createDiarySubject ["serverrules","Rules"];
player createDiarySubject ["keybinds","KeyBinds"];
player createDiarySubject ["skillsystem","Skills"];
player createDiarySubject ["flyrules","Flying"];
player createDiarySubject ["rdmvdmrules","RDM/VDM"];
player createDiarySubject ["nlrrule","New Life"];
player createDiarySubject ["fedrules","Fed"];
player createDiarySubject ["warrules","War"];
player createDiarySubject ["faqsection","FAQ"];

/*  Example
	player createDiaryRecord ["", //Container
		[
			"", //Subsection
				"
				TEXT HERE<br/><br/>
				"
		]
	];
*/	
	player createDiaryRecord ["faqsection", //Container
		[
			"FAQ's", //Subsection
				"
				============================<br/>
				Will I get arrested if I'm wearing rebel clothes and a cop pulls me over, or see's me in a city?<br/>
				Rebel clothing is particular and cause for suspicion, but not a direct reason to pull someone over<br/>
				============================<br/>

				============================<br/>
				Is owning a rebel license illegal?<br/>
				Yes it is, and if being searched it is cause for a ticket.<br/>
				============================<br/>

				============================<br/>
				Will my helicopter get impounded for having camouflage if a cop sees it?<br/>
				Your helicopter will only be impounded if left, or it has landed in town, or if you have been arrested. But owning a helicopter is perfectly legal as long as you follow all flying rules. If you have landed and run off to do a mission, the police should check the registration and text message you that your vehicle is about to be impounded, if you don't respond, then they could, yes.<br/>
				============================<br/>

				============================<br/>
				Can I fly lower then 250m outside cities and towns without pissing off the police?<br/>
				Yes you can fly lower than 250m outside of town, but you'll still probably piss the cops off. As long as you follow the flyby rules and hovering rules, it should be no problem.<br/>
				============================<br/>

				============================<br/>
				I am currently playing a rebel and was curious if their is some way to detain hostages?<br/>
				There already is a way to detain civilians, you knock them out using SHIFT + G and then use SHIFT + R to restrain them. The only way become unrestrained currently is by use of a lockpick, or to wait the 5 minute excessive restraint time.<br/>
				============================<br/>

				============================<br/>
				The rebel spawn point is a safe zone for rebels, does this include the whole Red circular areas or just around the general vicinity of the rebel spawn points?<br/>
				This includes the immediate area of the spawn, also you can't camp the spawn waiting for others to leave. The red circle rebel zone is really to declare the zone that is owned by rebels. So say a cop wanders into these zones, you can shoot him dead, for any reason. I would hope that people choose to roleplay killing an officer in that area, but if people continually choose to ignore roleplay and abuse the KOS area, it could be removed or altered.<br/>
				============================<br/>

				============================<br/>
				Can I deny police a search of my vehicles and persons if they don't have probable cause or I'm not breaking any laws?<br/>
				You can try. Most police will not allow you to refuse, as police should not be detaining/stopping you without probable cause in the first place.<br/>
				============================<br/>

				============================<br/>
				I text admins but get no response, how should I report things to the admins?<br/>
				The first step should always be to join our Teamspeak and Request to speak to an Administrator. Your second step for recourse is to post on this website if for some reason an Admin isn't available on Teamspeak. If you want to report abuse or bad cops, then please record it. Only so much can be done on word vs word and what somebody feels<br/>
				============================<br/>

				============================<br/>
				If I donate, can I not follow some rules?<br/>
				NO. Donators and non-donators are subject to all the same rules. Anyone who makes a donation is simply doing that, and is garnered no extra treatment. The extra in-game items I give donors are a thank-you for donating to HELP KEEP THE SERVER RUNNING. If you are caught cheating, breaking rules, not roleplaying, you will be disciplined like anyone else. The only way a server runs well is if everyone has a good time and nobody is ruining it.<br/>
				============================<br/><br/>
				"
		]
	];
	player createDiaryRecord ["rdmvdmrules", //Container
		[
			"RDM/VDM Rules", //Subsection
				"
				============================<br/>
				Performing a Random Deathmatch may result in your removal from the server and/or ban, based on the admins discretion<br/>
				============================<br/>
				Random Deathmatch is defined as:<br/>
				Killing anyone in a safe zone<br/>
				Killing anyone without a roleplay cause<br/>
				Declaring a rebellion is not a cause to kill anyone, even cops<br/>
				Cops and civilians/rebels can only commence in a shootout if there are reasons relating to a crime<br/>
				If you are killed in the crossfire of a fight, it is not RDM<br/>
				Killing someone in an attempt to protect yourself or others is not RDMing<br/>
				Shooting a player without giving reasonable time to follow demands is considered RDM<br/>
				============================<br/>
				Vehicle Deathmatch is something we have a zero tolerance for on this server:<br/>
				You may not, for any reason, destroy your vehicle or intentionally use your vehicle to cause a head on collision or to collide with players on foot regardless of the situation.<br/>
				This includes, but is not limited to; running over people because they are shooting at you, ramming other vehicles or otherwise using your vehicle for purposes other than transportation. <br/>
				"
		]
	];
	player createDiaryRecord ["flyrules", //Container
		[
			"Flying Rules", //Subsection
				"
				============================<br/>
				General Flying Rules<br/>
				============================<br/>
				You are not to perform flybys, or maneuvers or pick-ups inside any spawn city or safe zone<br/>
				The Federal Reserve is not in a safe zone and you can land/hover/loiter while robbing the bank. However it is in a city and the 250 meter rule applies when flying by<br/>
				Any abuse will lead to a warning followed by; a kick for the night then; deletion of all vehicles + a week ban, then; Permanent ban<br/>

				============================<br/>
				Civilian Aviation<br/>
				============================<br/>
				Voilation of the items in this section may result in your removal from the server and/or ban, based on the admins discretion<br/>
				ABSOLUTELY NO FLYBYS OVER SAFEZONES / SPAWNPOINTS. Check points are not safe zones.<br/>
				When a properly declared attack is underway, the attackers may hover in a helo, as they are attacking something, but only in the immediate area<br/>
				Repeatedly flying over areas with police to annoy them will be considered trolling, do it somewhere else<br/><br/>
				Excessive flybys in any situation will not be allowed. It causes lag on other players and disrupts gameplay. Excessive is more than once<br/>
				Intentionally causing a collision with a helicopter into anything; i.e. other helicopters, vehicles, buildings, will be treated as VDM<br/>
				Flying below 250m over the city constantly. Once is illegal, more than that you risk crashing into the city, thus against server rules<br/>
				Stealing helicopters without proper warning and significant time for the driver to lock the vehicle. If they land and run away without locking, fine, if they just get out and you get in before they get a chance to lock it<br/><br/>

				============================<br/>
				Police Aviation<br/>
				============================<br/>
				Items on this list may result in your blacklisting from playing as cop and also removal from the server and/or ban, based on the admins discretion<br/>
				No helicopter can land within city limits without authorization from the highest ranking officer online<br/>
				Helicopters may not land on roads<br/>
				Police may temporarily forbid landing at a landing zone due to Police activity, but it cannot remain closed for a long period of time<br/>
				Helicopters cannot fly below 250m over the city or safe zone without authorization<br/>
				Helicopters cannot hover over the city. Cops may only hover over the city if there is an active police operation going on<br/>
				You are to only use helicopters when necessary, i.e. bank robbery, car chase, manhunt, going real long distances. NOT FOR JUST GETTING AROUND WHERE YOU WANT<br/><br/>
				"
		]
	];
	player createDiaryRecord ["nlrrule", //Container
		[
			"New Life Rules", //Subsection
				"
				============================<br/>
				Civilian New Life Rule<br/>
				============================<br/>

				If you are killed you must wait 15 minutes before returning to the scene of your death.<br/>
				If you are killed you may not be involved in the same situation that you were involved in before you died.<br/>
				If you die during roleplay your past crimes are forgotten, but you also cannot seek revenge<br/>
				If you are RDM'd, it is not a new life<br/>
				If you manually respawn, it is not a new life<br/>
				If you purposefully kill yourself to avoid roleplay, it is not a new life<br/>
				If you disconnect while being brought to jail you will be kicked/banned<br/>
				If you die, you may not speak with anyone you have met in your past life, regarding the situation of your death, or pointing out where other players are<br/>
				You MUST NOT return to within 1000m of your location of death until 15min after your death<br/>

				============================<br/>
				Police New Life Rule<br/>
				============================<br/>
				If you are killed you must wait 15 minutes before returning to the scene of your death, Unless there are less than 6 Cop's on.<br/>
				If you die during roleplay you cannot seek revenge<br/>
				If you are RDM'd, it is not a new life<br/>
				If you are VDM'd, it is not a new life<br/>
				If you manually respawn, it is not a new life<br/>
				If you purposefully kill yourself<br/> to avoid roleplay, it is not a new life and will be demoted
				If you die at the Federal Reserve, you are NOT to go to Curly's Gold to attempt to stop the robbers again.<br/><br/>
				"
		]
	];

	player createDiaryRecord ["warrules", //Container
		[
			"War Rules", //Subsection
				"
				============================<br/>
				-YOU MUST BE WITHIN 300 METERS OF THE TARGET YOU ARE DECLARING WAR ON BEFORE DECLARING WAR ON IT AND DURING THE WAR ON IT<br/>
				-YOU CANNOT MAKE A DECLARATION OF WAR ON ANY SPAWN POINT OR CITY OR PRISON OR THE AIRPORT<br/>
				-DECLARING WAR IS NOT A FREE FOR ALL TO DISREGARD ROLEPLAY<br/>
				-IF THERE ARE LESS THAN 6 POLICE ONLINE, THERE MAY ONLY BE ONE WAR DECLARED AT A TIME ON THE WHOLE SERVER<br/>
				-WAR AND ROBBING THE FEDERAL RESERVE ARE BOTH ACTS OF TERRORISM, AS SUCH WAR CANNOT BE DECLARED ON THE FEDERAL RESERVE UNTIL AFTER A ROBBERY HAS COMPLETED AND THE APPROPRIATE COOLDOWN PERIOD HAS PASSED. ROBBERIES CAN OCCUR IN WAR THOUGH<br/>
				-WAR IS NOT ALLOWED TO BE DECLARED ON THE FEDERAL RESERVE AS IT IS BEING ROBBED<br/>
				=============== Hostage Rules =============<br/>
				-CREATING FAKE HOSTAGE SITUATIONS IS CONSIDERED FAIL ROLEPLAY<br/>
				-YOU MAY NOT HARM THE HOSTAGE IF YOUR DEMANDS HAVE BEEN MET<br/>
				-MAKING UNREASONABLE DEMANDS WILL CONSIDERED FAIL ROLEPLAY<br/>
				<br/>
				<br/>
				"
		]
	];

	player createDiaryRecord ["fedrules", //Container
		[
			"Fed Robbery", //Subsection
				"
				============================<br/>
				-IF BANK HAS BEEN ROBBED, THERE IS A 30 MINUTE COOLDOWN PERIOD BEFORE IT CAN BE ROBBED AGAIN (IF SUCCESSFUL, 20MIN IF IT FAILS), YOU MUST DO YOUR BEST TO LEAVE THE AREA ASAP AFTER THE ROBBERY HAS SUCCEEDED/FAILED AND THERE IS NO IMMEDIATE THREAT TO YOU<br/>
				-POLICE SHOULD NOT BEGIN IMPOUNDING VEHICLES AT THE FEDERAL RESERVE UNTIL THE ROBBERY HAS FAILED/SUCCEEDED, UNLESS THE VEHICLE IS OTHERWISE ILLEGALLY PARKED OR A HAZARD<br/>
				-LEAVING THE AREA ASAP IS TO ALLOW POLICE TO COME THROUGH AND CLEAR VEHICLES TO PREVENT DESYNCING ISSUES, POLICE PRESENCE WITHOUT IMMEDIATE THREAT SHOULD NOT COMPEL YOU TO STAY<br/>
				-LEAVING THE AREA IS DEFINED AS LEAVING THE AREA COMPLETELY, YOU SHOULD BE WELL OVER 1000m+ AWAY<br/>
				-YOU MAY NOT CAMP THE FEDERAL RESERVE AREA TO WAIT FOR A ROBBERY. THIS GOES FOR ANY COP, CIV, OR REBEL<br/>
				-IF YOU ARE NOT AFFILIATED WITH THE GROUP THAT IS CURRENTLY ROBBING THE FEDERAL RESERVE, THEN YOU ARE NOT TO BE THERE.(THIS INCLUDES WAITING AT CURLY'S GOLD FOR THE ROBBERS TO SELL)<br/>
				-WAR IS NOT ALLOWED TO BE DECLARED ON THE FEDERAL RESERVE AS IT IS BEING ROBBED<br/>
				=============== Hostage Rules =============<br/>
				-CREATING FAKE HOSTAGE SITUATIONS IS CONSIDERED FAIL ROLEPLAY<br/>
				-YOU MAY NOT HARM THE HOSTAGE IF YOUR DEMANDS HAVE BEEN MET<br/>
				-MAKING UNREASONABLE DEMANDS WILL CONSIDERED FAIL ROLEPLAY<br/>
				<br/>
				"
		]
	];
	player createDiaryRecord["keybinds",
		[
			"Keybinds",
				"
				Y : Open Player Menu<br/>
				Shift + Y : Surrender<br/>
				U : Lock and unlock cars<br/>
				T : Vehicle Trunk<br/>
				SHIFT + T : Use pickaxe if  have a pickaxe and are near mine
				R : Reload<br/>
				Shift + L : Earplugs on or off<br/>
				Shift + R: Restrain<br/>
				Shift + G: Knock out / stun (Civ Only, used for robbing them)<br/>
				LEFT WINDOWS KEY IS MAIN INTERACTION KEY FOR PLAYERS AND VEHICLES<br/>				
				 - Picking up items/money off the ground, you can also have the scroll wheel option to pick up items<br/>
				 - Interacting with cars (repair, push, impound etc)<br/>
				 - Interacting with players (escort, arrest,take organ)<br/>
				 - The key can be rebound to a single key like H by pressing ESC->Configure->Controls->Custom->Use Action 10<br/>
				Shift + H: Holster weapon<br/>
				Ctrl + H: UnHolster weapon<br/>
				Shift + Space: Jump<br/>
				ALT + H: Self-Bloodbag when needed<br/>
				O : Gate opener for police<br/>
				ALT + O: Replace own organ if needed<br/>
				====== POLICE SHOUTS ======<br/>
				Alt + 1 : Put your hands up where I can see them! <br/>
				Alt + 2 : Pull over and stay in your vehicle! <br/>
				Alt + 3 : Drop your weapon and put your hands on your head! <br/>
				Alt + 4 : HELI: Clear the area or you will be fired upon <br/>
				Alt + 5 : If you are not involved, leave the area, or you will be arrested <br/>
				====== MEDIC SHOUTS ======<br/>
				Alt + 1 : Hold Fire - Medic in the area <br/>
				Alt + 2 : Back away from the victim <br/>
				Alt + 3 : Harming a medic is against the law <br/>
				Alt + 4 : Dont shoot I'm a medic for god's sake <br/>
				Alt + 5 : Who needs a medic? <br/>
				====== CIVILIAN SHOUTS ======<br/>
				Alt + 1 : Put your gun down and your hands up <br/>
				Alt + 2 : Get away from the vehicle! <br/>
				Alt + 3 : Leave me alone <br/>
				Alt + 4 : Dont shoot I give up! <br/>
				Alt + 5 : I'm just trying to leave, don't shoot! <br/>
				"
		]
	];


		player createDiaryRecord ["serverrules",
		[
			"Server Rules", 
				"
				============================<br/>
				-ALL RULES ARE SUBJECT TO CHANGE AT ANY TIME<br/>
				-ALL DECISIONS/JUDGEMENTS/PUNISHMENTS BY ADMINISTRATORS AND STAFF ARE FINAL<br/>
				-VIOLATION OF ANY SERVER RULE MAY RESULT IN YOUR REMOVAL FROM THE SERVER/BAN AT THE DISCRETION OF ADMINISTRATORS<br/>
				============================<br/>
				FAILING TO ROLEPLAY IS A SERIOUS VIOLATION OF SERVER RULES; THIS INCLUDES BUT IS NOT LIMITED TO; ABUSING ANY GAME FUNCTIONALITY TO PREVENT ARREST OR INHIBIT PLAY FOR OTHERS. (SUCH AS REPEATEDLY JUMPING OUT OF A VEHICLE WHEN AN OFFICER ATTEMPTS TO ARREST YOU OR SHOOTING YOUR FRIEND AFTER HE HAS BEEN RESTRAINED TO PREVENT HIS ARREST)<br/>
				============================<br/>
				-Threatening other players with DDOS'ing, IP theft etc will result in a Perm ban!<br/>
				-Advertising on Teamspeak or Game Servers is strictly forbidden! Advertising will result in a Perm ban!<br/>
				-If you are playing as a Cop, you must be on Teamspeak<br/>
				-ALWAYS ROLEPLAY<br/>
				-NO TROLLING<br/>
				-NO RDM / VDM PERIOD<br/>
				-IF YOU ARE BEING CHASED BY SOMEONE, YOU MAY NOT USE THE BUS STOPS (TELEPORTERS). YOU MAY NOT USE THEM UNTIL YOU CAN PROVE THAT YOU ARE NO LONGER BEING CHASED<br/>
				-IF YOU ARE BEING CHASED BY SOMEONE, YOU MAY NOT FLEE INTO A SAFE ZONE TO GET AWAY<br/>
				-Absolutely no theft of any Police vehicle unless officers are dead, they've left the vehicle (In excess of 150m), or you are involved in Police chase and they've failed to apprehend you and you are able to steal their car to get away. Any civilian not involved in Police activity may not steal police vehicles involved in Police activity AT ALL<br/>
				-Wearing Police Clothing or Medic Clothing is against server rules and will result in a kick/ban<br/>
				-After the Federal Reserve has been robbed police will start doing cleanup, at this time, if you come and start killing cops or attempting to rob the fed, is against server rules, which may lead to a kick/ban<br/>
				============================<br/>
				-IF YOU REPORT A PLAYER AND DO ANY OF THE FOLLOWING YOU COULD BE BANNED:<br/>
				     -You lie about any details in the report<br/>
					 -You choose to skip any details that may change the outcome of a report<br/>
					 -You report to blackmail other players and/or demand your items or vehicles to be given back in exchange for dropping the report<br/>
					 -You provide misinformation of any kind when reporting another player.<br/>
				============================<br/>
				"
		]
	];
	