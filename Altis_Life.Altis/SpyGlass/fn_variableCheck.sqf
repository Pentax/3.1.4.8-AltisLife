/*
	File: fn_variableCheck.sqf
	
	Description:
	Checks against harmful variables, disable this if client-performance is 
	to bad in the fn_initSpy.sqf, the menuCheck should be good enough!
*/
private ["_badFile","_badFiles","_badVariables","_checkThread","_reason","_caught"];

_badFile = [
	(format['%1\tele.sqf',profileName]),(format['%1\tp.sqf',profileName]),(format['%1\DefaultMenu.sqf',profileName]),(format['%1\initmenu.sqf',profileName]),
	(format['%1\Startup.sqf',profileName]),(format['%1\start.sqf',profileName]),(format['%1\run.sqf',profileName]),(format['%1\init.sqf',profileName])
];
_badVariables = [
	'poalmgoasmzxuhnotx','ceozxignhazju','letmeknow','Listw','mahcaq','mapm','weapFun','firstrun','take1','initarr2','wuat_fpsMonitor','drawic','E_X_T_A_S_Y_Cash_1k_t','E_X_T_A_S_Y_Cash_a','ptags',	
	'Isogay','imhornyaf','prore','getrekt','fnc_usec_damageHandler','r_doLoop','r_player_lowblood','r_player_unconscious','nb','Wookie','Extasy','GOD','GodMode','JxMxE_Exec','Hack',
	'Script','Wookie_Exec','Bypass','W_O_O_K_I_E_FUD_Pro_RE','W_O_O_K_I_E_FUD_Car_RE','JxMxE_Veh_M','JxMxE_LifeCash500k','W_O_O_K_I_E_FUD_FuckUp_GunStore','infiSTAR_fill_Weapons','wl','W_O_O_K_I_E_RE',
	'JxMxE_hide','JME_Keybinds','JME_has_yet_to_fuck_this_shit','JME_deleteC','JME_Tele','JME_ANAL_PLOW','JME_M_E_N_U_initMenu','JME_M_E_N_U_hax_toggled','Admin_Lite_Menu','admingod','footSpeedKeys',
	'W_O_O_K_I_E_FUD_M_E_N_U_initMenu','W_O_O_K_I_E_FuckUp_GunStore_a','JME_KillCursor','JME_OPTIONS','ASSPLUNGE','FOXBYPASS','POLICE_IN_HELICOPTA','JxMxE_Infect','hub','scrollinit','gfYJV',
	'JxMxE_EBRP','W_O_O_K_I_E_M_E_N_U_funcs_inited','Menu_Init_Lol','E_X_T_A_S_Y_Atm','W_O_O_K_I_E_Pro_RE','W_O_O_K_I_E_Debug_Mon','W_O_O_K_I_E_Debug_1337','E_X_T_A_S_Y_LicenseDrive','zombieshield',
	'Veh_S_P_A_W_N_Shitt','sfsefse','tw4etinitMenu','tw4etgetControl','JxMxEsp','JxMxE_GOD','JxMxE_Heal','efr4243234','sdfwesrfwesf233','sdgff4535hfgvcxghn','E_X_T_A_S_Y_Keybinds',
	'adadawer24_1337','lkjhgfuyhgfd','E_X_T_A_S_Y_M_E_N_U_funcs_inited','dayz_serverObjectMonitor','fsfgdggdzgfd','fsdddInfectLOL','lol','l0l','fapcar','fapgod','fapg0d','wookiewuat','monkyintmenu','kk',
	'JxMxE_TP','Wookie_Car_RE','Wookie_Debug_Mon','faze_funcs_inited','advertising_banner_infiSTAR','atext_star_xa','Monky_hax_dbclick','Menulocations','exstr1','pathtoscrdir3','Monky_funcs_inited',
	'qopfkqpofqk','debug_star_colorful','AntiAntiAntiAntiHax','godmode_on','godmode_off','dance','vehicleg0dv3_BushWookie','cargodv3','hostage','lel','shrekt','rekted','W_0_0_K_I_E_Pro_RE','GodLolPenis',	
	'JxMxE_secret','player_zombieCheck','E_X_T_A_S_Y_Recoil','godlol','playericons','go_invisible_infiSTAR','adminlite','adminlitez','antihacklite','bp','inSub','Metallica_vehicleg0dv3_infiSTAR',
	'serverObjectMonitor','enamearr','initarr3','locdb','infAmmoIndex','nukeDONEstar','Wookie_List','Wookie_Pro_RE','FUCKTONIC','E_X_T_A_S_Y_FuckUp_GunStore_a','adminESPicons','fnc_MapIcons_infiSTAR',		
	'scroll_m_init_star','xtags','wierdo','fcukupstar','m0nky','monkey','monky','markerCount','zombies','startmenu_star','LystoDone','MOD_EPOCH','BIS_MPF_remoteExecutionServer4','adminadd','shnext',
	'adminZedshld','adminAntiAggro','admin_vehicleboost','admin_low_terrain','admin_debug','admincrate','exstr','nlist','PV_AdminMainCode','TPCOUNTER','fn_esp','aW5maVNUQVI_re_1','passcheck',
	'fn_filter','vehiList','Remexec_Bitch','zeus_star','igodokxtt','tmmenu','AntihackScrollwheel','survcam','PVAH_AHTEMPBAN','isInSub','qodmotmizngoasdommy','ozpswhyx','E_X_T_A_S_Y_Menu_LOOOOOOOOOL',
	'lalf','toggle','iammox','telep','dayzlogin3','dayzlogin4','changeBITCHinstantly','antiAggro_zeds','BigFuckinBullets','abcdefGEH','adminicons','xdistance','wiglegsuckscock','diz_is_real__i_n_f_i_S_T_A_R',
	'pic','veh','unitList','list_wrecked','addgun','ESP','BIS_fnc_3dCredits_n','dayzforce_save','ViLayer','blackhawk_sex','activeITEMlist','items1','omgwtfbbq','namePlayer','thingtoattachto',
	'adgnafgnasfnadfgnafgn','Metallica_infiSTAR_hax_toggled','activeITEMlistanzahl','xyzaa','iBeFlying','rem','DAYZ_CA1_Lollipops','HMDIR','vehC','HaxSmokeOn','testIndex','g0d','spawnvehicles_star',
	'HDIR','YOLO','carg0d','init_Fncvwr_menu_star','altstate','black1ist','ARGT_JUMP','ARGT_KEYDOWN','ARGT_JUMP_w','ARGT_JUMP_a','bpmenu','color_black','kill_all_star','sCode','dklilawedve','peter_so_fly_CUS',
	'p','fffffffffff','markPos','pos','TentS','VL','MV','monkytp','pbx','nametagThread','spawnmenu','sceptile15','sandshrew','spawnweapons1','abcd','skinmenu','changebackpack','keymenu','godall',
	'mk2','i','j','v','fuckmegrandma','mehatingjews','TTT5OptionNR','zombieDistanceScreen','cargodz','R3m0te_RATSifni','wepmenu','admin_d0','RAINBOWREMEXECVEH','ALL_MAGS_TO_SEARCH','pfEpochTele',	
	'selecteditem','moptions','delaymenu','gluemenu','g0dmode','zeus','zeusmode','cargod','infiSTAR_fillHax','itemmenu','gmadmin','fapEsp','mapclick','theKeyControl','infiSTAR_FILLPLAYER','whitelist',	
	'custom_clothing','img','surrmenu','footSpeedIndex','ctrl_onKeyDown','plrshldblcklst','DEV_ConsoleOpen','executeglobal','cursoresp','Asdf','planeGroup','TAG_onKeyDown','changestats','derp123','heel',
	'teepee','spwnwpn','musekeys','dontAddToTheArray','morphtoanimals','aesp','LOKI_GUI_Key_Color','Monky_initMenu','tMenu','recon','curPos','pilot','rangelol','unitsmenu','xZombieBait','plrshldblckls',
	'playerDistanceScreen','ihatelife','debugConsoleIndex','MY_KEYDOWN_FNC','pathtoscrdir','Bowen_RANDSTR','ProDayz','idonteven','walrein820','RandomEx','ARGT_JUMP_s','ARGT_JUMP_d','globalplaya',
	'shnmenu','pm','lmzsjgnas','vm','bowonkys','glueallnigga','hotkeymenu','Monky_hax_toggled','espfnc','playeresp','zany','dfgjafafsafccccasd','FUK_da_target','damihakeplz','ratingloop_star',
	'atext','boost','nd','vspeed','Ug8YtyGyvguGF','inv','rspwn','pList','loldami','T','bowonky','aimbott','Admin_Layout','markeresp','allMrk','MakeRandomSpace','damikeyz_veryhawt','mapopt','slag',
	'helpmenu','rustlinginit','qofjqpofq','invall','initarr','reinit','admin_toggled','fn_ProcessDiaryLink','ALexc','DAYZ_CREATEVEHICLE','jizz','kkk','ebay_har','sceptile279','TargetPlayer',
	'tell_me_more_infiSTAR','airborne_spawn_vehicle_infiSTAR','sxy_list_stored','advert_SSH','antiantiantiantih4x','SuperAdmin_MENU','eExec_commmand','cockasdashdioh','fsdandposanpsdaon','sfg4w3t5esfsdf',
	'bl4ck1ist','keybinds','actualunit','mark_player','unitList_vec','yo2','actualunit_vec','typeVec','mark','r_menu','hfghfg','vhnlist','work','antiloop','anti','spawn_explosion_target_ebay',
	'yo3','q','yo4','k','cTargetPos','cpbLoops','cpLoopsDelay','Flare','Flare1','Flare2','Flare3','Flare4','Flare5','Flare6','Flare7','Flare8','kanghaskhan','palkia','whatisthis4','Veh_Spawn_Shitt',
	'PVAH_admin_rq','PVAH_writelog_rq','sandslash','muk','pidgeotto','charmeleon','pidgey','lapras','LYST1C_UB3R_L33T_Item','MathItem','fapLayer','jopamenu','ggggg','tlm','toggle_keyEH','infammoON','pu',
	'raichu','infiSTAR_chewSTAR_dayz_1','infi_STAR_output','infi_STAR_code_stored','keybindings','keypress','menu_toggle_on','dayz_godmode','aiUnit','chute','dayzforce_savex','PVDZ_AdminMenuCode',
	'MENUTITLE','runHack','Dwarden','ealxogmniaxhj','ohhpz','fn_genStrFront','shazbot1','cip','Armor1','byebyezombies','PVDZ_SUPER_AdminList','DarkwrathBackpack','patharray','time','epochRemoteNukeAll',
	'kickable','stop','possible','friendlies','hacks','main','mapscanrad','maphalf','DelaySelected','SelectDelay','GlobalSleep','isAdmin','vehD','ALL_WEPS_TO_SEARCH','fT','tpTarget','keyp',
	'PVDZ_hackerLog','BP_OnPlayerLogin','material','mapEnabled','markerThread','addedPlayers','playershield','spawnitems1','sceptile27','Proceed_BB','ZobieDistanceStat','infiSTARBOTxxx','keyspressed',
	'ESPEnabled','wpnbox','fnc_temp','MMYmenu_stored','VMmenu_stored','LVMmenu_stored','BIS_MPF_ServerPersistentCallsArray','PV_CHECK','admin_animate1','HumanityVal','yanma','absol','SimpleMapHackCount',
	'aggron','magazines_spawn','weapons_spawn','backpack_spawn','backpackitem_spawn','keybindings_exec','keypress_exec','MajorHageAssFuckinfBulletsDude','yothefuckingplayerishere','Namey',
	'Wannahaveexplosivesforbullets','TheTargetedFuckingPlayerDude','haHaFuckAntiHakcsManIbypasDatShit','aintNoAntiHackCatchMyVars','objMYPlayer','sendmsg12','jkh','DELETE_THIS','move_forward',
	'Awwwinvisibilty','vehiclebro','wtfyisthisshithere','terrainchangintime','Stats','menu','ssdfsdhsdfh','youwantgodmodebro','leftAndRight','forwardAndBackward','distanceFromGround',
	'hoverPos','hovering','bulletcamon','cheatlist','espOn','removegrass','timeday','infammo','norekoil','nocollide','esp2ez','fastwalk','entupautowalk','murkrow','noctowl','isExecuted','da342szvcxzcvx',
	'BensWalker','dropnear','executer','killme','magnetmenu','loadmain','magnet','A11','loadMenu','refreshPlayers','ALREADYRAN','players','BigBenBackpack','piloswine','AddPlayersToMap','markers',
	'sendMessage','newMessage','W34p0ns','amm0','Att4chm3nt','F0od_Dr1nk','M3d1c4l','T0ol_it3ms','B4ckp4cks','It3m5','Cl0th1ng','walkloc','nwaf','cherno','malfly','keyForward','upAndDown',
	'cherno_resident','cherno_resident_2','dubky','oaks','swaf','swmb','getX','PlayerShowDistance','M_e_n_u_2','colorme','keybindloop','Tractor_Time','miltank','GearAdd','GearRemove','Malvsm','Malcars',
	'PermDialogSelected','TempDialogSelected','AdminDialogList','pfKeygen','pfScanUnits','pfPickPlayer','pfshnext','pfnlist','pfselecteditem','pfshnmenu','LY_Swagger','LY_GetObject','LY_Run','DonorSkins',
	'pfPlayerMonitor','pfPlayersToMonitor','pfShowPlayerMonitor','pfPlayerMonitorMutex','marker','JJJJ_MMMM___EEEEEEE_INIT_MENU','E_X_T_A_S_Y_Init_Menu','monkaiinsalt','monkaiin','part88','adminKeybinds',
	'PV_DevUIDs','fapEspGroup','Repair','RepairIT','rainbowTarget','rainbowTarget1','rainbowTarget2','rainbowTarget3','equalGOD','equalESP','BB_Ultima','BB_RapidFire_Toggle','BB_Menu','BTC_liftHudId',
	'vars','PSwap','toLower_new','BCast','thfile','tlmadminrq','infiSTARBLACK','name','carepkg','scrollAim','BlurExec','quake','menu_run','ZedProtect','actid1','vehicles1','MapClicked',
	'MapClickedPosX','MouseUpEvent','scrollPlayerlist','keypress_xxx','envi','G_A_N_G_S_T_A','ZoombiesCar','timebypass','returnString_z','isori','tangrowth27','PVAH_AdminRequest','AH_OFF_LOL','b',
	'infiSTAR_fillRE','qwak','infoe','font','title_dialog','sexymenu_adds_Star','boolean_1','initre337','skype_option','bleh','magnetomortal','fnc_allunits','PV_IAdminMenuCode','PVAH_WriteLogRequest',
	'skype_img','Lhacks','Lpic','LtToTheRacker','Lexstr','Called','epochExec','W_O_O_K_I_E_Car_RE','life_no_injection','Tonic_has_a_gaping_vagina','teletoplr','ygurv1f2','vehiclegooov3ood_BushWookie',
	'kW_O_O_K_I_E_Go_Fast','epchDeleted','lystobindkeys','lystoKeypress','toggle_1','shiftMenu','dbClicked','b_loop','re_loop','v_bowen','bowen','melee_startAttack','asdasdasd','antihax2','PV_AdminMenuCode',
	'AdminLoadOK','AdminLoadOKAY','PV_TMPBAN','T_o_g_g_l_e_BB','fixMenu','PV_AdminMenuCodee','AdminPlayer','PVAH_AdminRequestVariable','epochBackpack','JME_Red','JME_MENU_Sub','JME_menu_title','JME_Sub',
	'heal','grass','fatguybeingchasedbyalion','night','day','nvg','thermal','fredtargetkill','loopfredtpyoutome','epochTp','VehicleMenue','Menue_Vehicle','my_anus_hurtz','poiuytfczsvtg','Pro_RE',
	'fzhgdhhbzfhzfghz','dgbfzhg5ey456w6s','asdsgxfvzv5345','dadsasdsada','aw235resfzdfcs','d3245resfrsd','feastge4rt636te5','sfsdfsdf4333','Hack_Pos_Orig','REdasfsfwef','dfhfgkjjhkhjkdgfg','adadadad23444',
	'Lysto_Lyst','ewrfdfcsf','sr3453sdfsfe33','rgyehgdrfhg','d121435rdsczcawddawdsf','adawredzfdsf','c0lorthem','srgte54y6rdgtrg','oighijkfcjloypysh','fazelist','faze_fill','dfgrgdrfgretg345t345','fetg5e4ytdrg',
	'inf_ammo_loop_infiSTAR','awrdw4355345sfs','Wookie_Init_Menu','asdr432r5edfsad','fdsgdr42424ZombieColor','adasdawer4w3r','hthxhzhgcbcxvb','dsagfgbdfhgsd','htrukilojhkukvh','d4t365tearfsafgrg','ddsfsdfv',
	'faze_getControl','dsfsgfsfsdfsdf','yer652374rfd','t0ggl3','d45y6w45ytrdfragsrega','morphm3','sgdfgzgdzfrgfdg','q25t3qttsdfaf','dawerdsczcdsf','gdzhzthtdhxthh6757','W00kie_Pro_RE','fdsgdr42424', 
	'battleHIGH_vehpub','sdsf45t3rsgfd','hdtrhyztyh','MenuInitLol','few3t5364etsfg','adadgvdthw54y64','eeeeeeewwwwwwwww','faze_initMenu','dawr5wdfsf23','sgstgr4stwe4t','gffffffffffffffh','asd34r253453453',
	'adawedr3q4r3w2qr4','eyghrdfyh','W00kie_Init_Menu','awdet3465taddd','rainbow_var','iluio9pilkgvuk','awer234rrsdfcsd','rdgfdzgzrfgr','gzgdghragfzdgvdz','sdfxdcfs3','infi_STAR_exec','asdw435r325efdsd',
	'xtags_star_xx','ChangingBullets_xx','ljkluilufgdsgzgzdrf324','hgjghjfrdfeewrferrt43','adr4w5trsdef','wadgrfgzrd','dasd324r245rdsfs','da124q3easds','awdadr3q42','awde3q4erasd','ShadowyFaz3VehZ',
	'fryt5tytfshyfkj','sfewsrw','W00kieMenu_hax_toggled','thuytshujsr65uy','adawdawe21','ad44234efdzsf','ffafsafafsfsgol','shth654436tj','gyjjgcjcj','htjhytu6waqe3q45','y6sretysrt','ANTI_ANTI_HAX',
	'hax_toggled','yiukfligzsgargfrae','asdddddddddddad','adddaaaaaaaaa','fesf4535teaf','rainbowbitch','ads2542345esfds','n0clip','saddaaaaaaaadd23','fefq34tqtrafg','f313131FukDaPolice1324e',"Sload","T3le",
	"aKFerm","aKMMenu","aKTitans","aKLikeaG0d","riasgremory_G0d_Mode","aKCarG0d","riasgremory_Car_Jesus","aKE5p","riasgremory_isseilol","aKPMark","aKCardetroy","aKGetKey","jaimepaslepoisin_HLEAL",
	"riasgremory_Noobs","riasgremory_Bitches","riasgremory_Map_Markers","aKUnMmo","jenesuispasuncheateur_unamo","aKVoit","Loljesaispasquoiecriremdr","isseigremory","gremorysama","aKTaCu",
	"aKKillcursor","aKNoEscort","aKEscort","aKtroll","aKTPall","aKUnrestrain","aKNoEscortMe","aKNoTaze","aKJailplayer","aKLisense","riasgremory_titans_shit_reold","Tonic_merde","Root_Launcher4",
	"TTTT_IIII___TTTTTTT_RAP_FR","TTTT_IIII___TTTTTTT_REPGA","TTTT_IIII___TTTTTTT_REPGAs","Root_Main4","Root_Pistol4","Root_Rifle4","Root_Machinegun4","Root_Sniper4","Root_Attachement4"
];

preProcessFileLineNumbers 'Scan complete, bad content was';
_checkThread = {
	_caught = false;
	_reason = ['none','none'];
	_c = 0;
	{
		_c = _c + 1;
		if (_c >= 10) then {_c = 0;uiSleep 0.5;};
		_f = preprocessFileLineNumbers _x;
		if (_f != '') exitWith {_caught = true;_reason = [_x,format['File: %1',_x]];};
		true
	} count (_this select 0);
	{
		if (!isNil _x) exitWith {
			_caught = true;
			_reason = [_x,format['Variable: %1',_x]];
		};
	} count (_this select 1);
	if (_caught) then {
		[[profileName,getPlayerUID player,_reason select 0],"SPY_fnc_cookieJar",false,false] spawn life_fnc_MP;
		[[profileName,_reason select 1],"SPY_fnc_notifyAdmins",true,false] spawn life_fnc_MP;
		uiSleep 0.5;
		diag_log format ["Bad Variable Check 0: %1",(_reason select 0)];//disable below for debugging, remember to enable before release
		diag_log format ["Bad Variable Check 1: %1",(_reason select 1)];//disable below for debugging, remember to enable before release
		//["SpyGlass",false,false] call compile PreProcessFileLineNumbers "\a3\functions_f\Misc\fn_endMission.sqf";
	};
};
/*
//Create the worker.
while {true} do {
	[_badFiles,_badVariables] spawn _checkThread;
	uiSleep 900; //Goto sleep for 2 minutes.
};
*/
/*
_checkThread = {
	_caught = false;
	_reason = ['none','none'];
	{
		if (!isNil _x) exitWith {
			_caught = true;
			_reason = [_x,format['Variable: %1',_x]];
		};
	} count _this;
	if (_caught) then {
		[[profileName,getPlayerUID player,_reason select 0],"SPY_fnc_cookieJar",false,false] spawn life_fnc_MP;
		[[profileName,_reason select 1],"SPY_fnc_notifyAdmins",true,false] spawn life_fnc_MP;
		uiSleep 0.5;
		["SpyGlass",false,false] call BIS_fnc_endMission;
	};
};

//Create the worker.
while {true} do {
	_badVariables spawn _checkThread;
	uiSleep 300; //Goto sleep for 2 minutes.
};
*/