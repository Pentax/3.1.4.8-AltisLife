/*
Author: code34 nicolas_boiteux@yahoo.fr-Copyright (C) 2013 Nicolas BOITEUX 
edited/gutted by [midgetgrimm] for altis life rpg forums
*/
private ["_lastrain","_rain","_fog","_lightning","_overcast","_random", "_startingdate","_startingweather","_timesync","_wind","_case"];

_timesync = 60;// send sync data across the network each 60 seconds
_startingdate = [2035, 07, 06, 17, 30];//starting date [year,month,day,hour,minute]
_startingweather = "RAIN";//starting weather for everyone
setdate _startingdate;
switch(toUpper(_startingweather)) do {
	case "CLEAR": {rWeather = [0, 0, 0, [random 3, random 3, true], date,0];_case = "CLEAR";diag_log format["Weather is now %1 ",_case];};
	
	case "CLOUDY": {rWeather = [0, 0, 0.3, [random 3, random 3, true], date,0.4];_case = "CLOUDY";diag_log format["Weather is now %1 ",_case];};
		
	case "RAIN": {rWeather = [0, 0, 0.7, [random 3, random 3, true], date,0.99];_case = "RAIN";diag_log format["Weather is now %1 ",_case];};
	default {rWeather = [0, 0, 0, [random 3, random 3, true], date,0];_case = "default";diag_log format["Weather is %1, the array is %2 ",_case, rWeather];
	};
};

// add handler
if (local player) then {
	rWeatherstart = true;
	"rWeather" addPublicVariableEventHandler {
		// first JIP synchronization
		if(rWeatherstart) then {
			rWeatherstart = false;
			//skipTime -24;
			60 setRain (rWeather select 0);
			60 setfog (rWeather select 1);
			60 setOvercast (rWeather select 2);
			60 setLightnings (rWeather select 5);
			//skipTime 24;
			simulweatherSync;
			setwind (rWeather select 3);
			setdate (rWeather select 4);
		}else{
			rWeather = _this select 1;
			60 setRain (rWeather select 0);
			60 setfog (rWeather select 1);
			60 setOvercast (rWeather select 2);
			60 setLightnings (rWeather select 5);
			setwind (rWeather select 3);
			setdate (rWeather select 4);
		};
	};
};
// SERVER SIDE SCRIPT
if (!isServer) exitWith{};

// apply weather
//skipTime -24;
60 setRain (rWeather select 0);
60 setfog (rWeather select 1);
60 setOvercast (rWeather select 2);
60 setLightnings (rWeather select 5);
//skipTime 24;
simulweatherSync;
setwind (rWeather select 3);
setdate (rWeather select 4);

// sync server & client weather & time
[_timesync] spawn {
	private["_timesync"];
	_timesync = _this select 0;
	while { true } do {
		rWeather set [4, date];
		publicvariable "rWeather";
		uiSleep _timesync;
	};
};

_lastrain = 0;
_rain = 0;
_overcast = 0;

while {true} do {
	
	_overcast = random 1;
	if(_overcast > 0.55) then {
		
		_rain = random 0.6;
		_lightning = random 0.9;
		_wind = [random 7, random 7, true];
	
	} else {
		
		_rain = 0;
		_lightning = 0;
		_wind = [0,0,true];
		_overcast = 0
		
	};
	
	if((_lastrain > 0.7) and (_rain < 0.4)) then {
			
			_fog = [0.5,0.5,5];
			
	} else {
			
			_fog = 0;
			
	};
	_lastrain = _rain;
	rWeather = [_rain, _fog, _overcast, _wind, date,_lightning];
	60 setRain (rWeather select 0);
	60 setfog (rWeather select 1);
	60 setOvercast (rWeather select 2);
	60 setLightnings (rWeather select 5);
	setwind (rWeather select 3);
	uiSleep 300;//set to 300 for testing
};
diag_log "--------------Weather Information-----------";
diag_log format["Rain: %1",_rain];
diag_log format["Wind: %1",_fog];
diag_log format["Fog: %1",_overcast];
diag_log format["Overcast: %1",_wind];
diag_log format["Lightning: %1",_lightning];
diag_log "--------------------------------------------";