#include <macro.h>
/*
	File: fn_initCiv.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Initializes the civilian.
*/
private["_spawnPos"];

waitUntil {!(isNull (findDisplay 46))};
if((str(player) in ["civ_9999","civ_99999"])) then {
	if(__GETC__(life_adminlevel) < 4) then {
				["NotWhitelisted",false,true] call BIS_fnc_endMission;
		sleep 35;
	};
};
if(life_is_arrested) then
{
	life_is_arrested = false;
	[player,true] spawn life_fnc_jail;
}
	else
{
	[] call life_fnc_spawnMenu;
	waitUntil{!isNull (findDisplay 38500)}; //Wait for the spawn selection to be open.
	waitUntil{isNull (findDisplay 38500)}; //Wait for the spawn selection to be done.
};
player addRating 9999999;
[] call life_fnc_newZoneCreator;
[] spawn
	{
		while {true} do
		{
			waitUntil {uniform player == "U_Competitor"};
			player setObjectTextureGlobal [0,"textures\prisoner.jpg"];
			waitUntil {uniform player != "U_Competitor"};
	};
};