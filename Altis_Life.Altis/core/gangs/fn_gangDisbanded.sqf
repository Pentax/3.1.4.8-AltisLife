#include <macro.h>
/*	Author: Bryan "Tonic" Boardwine*/
private["_group"];_group = [_this,0,grpNull,[grpNull]] call BIS_fnc_param;if(isNull _group) exitWith {}; hint localize "STR_GNOTF_DisbandWarn_2";[player] joinSilent (createGroup east);if(count units _group == 0) then {	deleteGroup _group;};