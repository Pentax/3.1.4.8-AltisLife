/*
Author: code34 nicolas_boiteux@yahoo.fr-Copyright (C) 2013 Nicolas BOITEUX 
edited/gutted by [midgetgrimm] for altis life rpg forums
*/
private ["_lastrain", "_rain", "_fog", "_mintime", "_maxtime", "_overcast", "_realtime", "_random", "_skiptime", "_startingdate", "_startingweather", "_timeforecast", "_timeratio", "_timesync", "_wind","_lightning"];
	
_realtime = true;//realtime(true) vs fasttime(false) Real better for persistence
_random = false;//true between min/maxtime false happens at mintime
_mintime = 1200;//time before new forecast
_maxtime = 3600;//maxtime for new forecast
_timeratio = 1;//ratio is 1 realtime second is X game seconds
_timesync = 60;//syncs data across the network
_startingdate = [2035, 07, 06, 17, 30];//mission starting date/time
_startingweather = "CLEAR";// Mission starting weather "CLEAR|CLOUDY|RAIN";
// Do not edit below unless you not dumb :P
if(_mintime > _maxtime) exitwith {
		hint format["Real weather: Max time: %1 can no be higher than Min time: %2", _maxtime, _mintime];
};
_timeforecast = _mintime;

// we check the skiptime for 10 seconds
_skiptime = _timeratio * 0.000278 * 10;
setdate _startingdate;

switch(toUpper(_startingweather)) do {
	case "CLEAR": 
	{	
		wcweather = [0, 0, 0, [random 3, random 3, true], date,0];
		diag_log format ["Starting Weather: %1",_startingweather];
	};
	case "CLOUDY": 
	{
		wcweather = [0, 0.1, 0.3, [random 7, random 7, true], date,0.8];
		diag_log format ["Starting Weather: %1",_startingweather];
	};
	case "RAIN": 
	{
		wcweather = [0.7, 0.2, 0.7, [random 15, random 15, true], date,0.99];
		diag_log format ["Starting Weather: %1",_startingweather];
	};
	default {
	// clear
		wcweather = [0, 0, 0, [random 3, random 3, true], date,0];
		diag_log format ["Starting Weather: %1",_startingweather];
	};
		
};
	
// add handler
if (local player) then {
	wcweatherstart = true;
	"wcweather" addPublicVariableEventHandler {
		// first JIP synchronization
		if(wcweatherstart) then {
			wcweatherstart = false;
			skipTime -24;
			86400 setRain (wcweather select 0);
			86400 setfog (wcweather select 1);
			86400 setOvercast (wcweather select 2);
			86400 setLightnings (wcweather select 5);
			skipTime 24;
			simulweatherSync;
			setwind (wcweather select 3);
			setdate (wcweather select 4);
			diag_log format ["Weather Local Player: %1",wcweather];
		}else{
			wcweather = _this select 1;
			60 setRain (wcweather select 0);
			60 setfog (wcweather select 1);
			60 setOvercast (wcweather select 2);
			setwind (wcweather select 3);
			setdate (wcweather select 4);
			60 setLightnings (wcweather select 5);
			diag_log format ["Weather Local Player Else: %1",wcweather];
		};
	};
};

// accelerate time 
if!(_realtime) then {
	[_skiptime] spawn {
		private["_skiptime"];
		_skiptime = _this select 0;
		while {true} do {
			skiptime _skiptime;
			sleep 10;
		};
	};
};

// SERVER SIDE SCRIPT
if (!isServer) exitWith{diag_log "Not a server?";};
// apply weather
skipTime -24;
86400 setRain (wcweather select 0);
86400 setfog (wcweather select 1);
86400 setOvercast (wcweather select 2);
86400 setLightnings (wcweather select 5);
skipTime 24;
simulweatherSync;
setwind (wcweather select 3);
setdate (wcweather select 4);
diag_log format ["Weather Apply: %1",wcweather];
// sync server & client weather & time
[_timesync] spawn {
	private["_timesync"];
	_timesync = _this select 0;
	while { true } do {
		wcweather set [4, date];
		publicvariable "wcweather";
		sleep _timesync;
	};
};

_lastrain = 0;
_rain = 0;
_overcast = 0;
_lightning = 0;
while {true} do {
	_overcast = random 1;
	diag_log format ["overcast random %1", _overcast];
	if(_overcast > 0.55) then {
			_rain = random 0.8;
			_lightning = 0.9;
			_wind = [random 7, random 7, true];
			diag_log format ["Rain %1 -- Lightning %2 -- Wind %3", _lastrain, _rain, _wind];
	} else {
			_rain = 0;
			_lightning = 0;
			_wind = [random 5,random 5,true];
	};
	if((_lastrain > 0.7) and (_rain < 0.4)) then {
			_fog = [0.5,0.5,5];
	} else {
			_fog = 0;
	};
	diag_log format ["LastRain %1 -- Rain %2", _lastrain, _rain];
	_lastrain = _rain;
	wcweather = [_rain, _fog, _overcast, _wind, date,_lightning];
	60 setRain (wcweather select 0);
	60 setfog (wcweather select 1);
	60 setOvercast (wcweather select 2);
	60 setLightnings (wcweather select 5);
	setwind (wcweather select 3);
	diag_log format ["Rain %1 Fog %2 Overcast %3 Wind %4 Lightning %5",_rain, _fog, _overcast, _wind, _lightning];
	if(_random) then {
		_timeforecast = _mintime + (random (_maxtime - _mintime));
	};
	sleep _timeforecast;
};