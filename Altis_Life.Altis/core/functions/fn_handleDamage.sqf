private["_unit","_damage","_source","_projectile"];
_unit = _this select 0;
_damage = _this select 2;
_source = _this select 3;
_projectile = _this select 4;

//Handle the tazer first (Top-Priority).
if(!isNull _source) then {
	if(_source != _unit) then {
		_curWep = currentWeapon _source;
		if(_projectile == "B_9x21_Ball" && _curWep in ["hgun_P07_snds_F","hgun_Rook40_snds_F"]) then {
			if(side _source == west && playerSide != west) then {
				private["_distance","_isVehicle","_isQuad"];
				_distance = 75;
				_isVehicle = if(vehicle player != player) then {true} else {false};
				_isQuad = if(_isVehicle) then {if(typeOf (vehicle player) == "B_Quadbike_01_F") then {true} else {false}} else {false};
				_damage = false;
				if(_unit distance _source < _distance) then {
					if(!life_istazed && !(_unit getVariable["restrained",false])) then {
						if(_isVehicle && _isQuad) then {
							if(vehicle player != player) then {_damage = false;};
							_damage = false;
						} else {
							
							[_unit,_source] spawn life_fnc_tazed;
							
						};
					};
				} else {
				
					_damage = false;
				};
			};//Temp fix for super tasers on cops.if(playerSide == west && side _source == west) then {_damage = false;};
		};
	};
};
// Eliminate damage when VDM from another player.
if (vehicle _unit == _unit) then
{
	if ( _source isKindOf "Air" OR _source isKindOf "Car" OR _source isKindOf "Boat" ) then
		{
			diag_log "_Source is Vehicle, Not a player driving a vehicle"
		}
		else
		{	
			_isVehicle = vehicle _source;
			if (_isVehicle isKindOf "Air" OR _isVehicle isKindOf "Car" OR _isVehicle isKindOf "Boat") then 
			{
				_damage = 0.001;
				[[player,"amovppnemstpsraswrfldnon"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
			};
	};
};

[] call life_fnc_hudUpdate;
_damage;