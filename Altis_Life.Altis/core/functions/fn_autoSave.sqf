/*
@Version: 0.3
@Author: =IFD= Cryptonat
@Edited: 5/16/14

Description:
Saves the player's gear every 30 minutes.
*/

while {true} do {
sleep 1800;
[] call SOCK_fnc_updateRequest;
hint "Database Updated. Player information has been saved to the server."
};