/*	Author: Bryan "Tonic" Boardwine*/
private["_container","_unit"];if(count _this == 1) exitWith {false};_unit = _this select 0;_container = _this select 1;_isPack = getNumber(configFile >> "CfgVehicles" >> (typeOf _container) >> "isBackpack");if(_isPack == 1) then {	hint localize "STR_MISC_Backpack";	[] spawn {waitUntil {!isNull (findDisplay 602)};closeDialog 0;	};};if(_container isKindOf "LandVehicle" OR _container isKindOf "Ship" OR _container isKindOf "Air") exitWith {	if(!(_container in life_vehicles) && {(locked _container) == 2}) exitWith {	hint localize "STR_MISC_VehInventory";	[] spawn {	waitUntil {!isNull (findDisplay 602)};	closeDialog 0;	};};}; 

/*
if(_container isKindOf "Man" && !alive _container) exitWith {
	hint localize "STR_NOTF_NoLootingPerson";
	[] spawn {
		waitUntil {!isNull (findDisplay 602)};
		closeDialog 0;
	};
};
*/