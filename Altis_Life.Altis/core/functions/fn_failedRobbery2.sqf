/*
original Code by: Robert "weiRd" May
edited/updated by midgetgrimm

*/

player setVariable ["restrained",true,true];
titleText["Your bank robbery has backfired and the authorities have been notified!","PLAIN"];
player say3D "cuff"; 	//cuff sound
//Restrain timer
[] spawn
{
	private["_time"];
	while {true} do
	{
		_time = time;
		waitUntil {(time - _time) > (5 * 60)};
		
		if(!(player getVariable["restrained",FALSE])) exitWith {};
		if(player getVariable["restrained",FALSE]) exitWith {
			player setVariable["restrained",FALSE,TRUE];
			player setVariable["Escorting",FALSE,TRUE];
			player setVariable["transporting",false,true];
			detach player;
			titleText["You have been released automatically for excessive restrainment time","PLAIN"];
		};
	};
};
			
while {player getVariable "restrained"} do
{
	player playMove "AmovPercMstpSnonWnonDnon_Ease";
	_state = vehicle player;
	waitUntil {animationState player != "AmovPercMstpSnonWnonDnon_Ease" || !(player getvariable "restrained") || vehicle player != _state};
			
	if(!alive player) exitWith
	{
		player setVariable ["restrained",false,true];
			player setVariable["Escorting",FALSE,TRUE];
			player setVariable["transporting",false,true];
	};
	
	if(vehicle player != player) then
	{
		//disableUserInput true;
		if(driver (vehicle player) == player) then {player action["eject",vehicle player];};
	}
		else
	{
		
	};
};
