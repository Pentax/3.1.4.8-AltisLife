hintSilent parseText format ["
	<t size='1.15' font='puristaLight' align='left'>Cash: </t><t size='1.15' font='puristaLight' align='right'>%1</t><br/>
	<t size='1.15' font='puristaLight' align='left'>Bank: </t><t size='1.15' font='puristaLight' align='right'>%2</t><br/>
	<t size='1.15' font='puristaLight' align='left'>Health: </t><t size='1.15' font='puristaLight' align='right'>%3</t><br/>
	<t size='1.15' font='puristaLight' align='left'>BAC Level: </t><t size='1.15' font='puristaLight' align='right'>%4</t><br/>
	<br/>
	<t size='1.15' font='puristaLight' align='left'>Police Online: </t><t size='1.15' font='puristaLight' align='right'>%5</t><br/>
	<t size='1.15' font='puristaLight' align='left'>Medics Online: </t><t size='1.15' font='puristaLight' align='right'>%6</t><br/>
	<t size='1.15' font='puristaLight' align='left'>Civilians Online: </t><t size='1.15' font='puristaLight' align='right'>%16</t><br/>
	<t size='1.15' font='puristaLight' align='left'>Player Rating: </t><t size='1.15' font='puristaLight' align='right'>%7</t><br/>
	<br/>",
	[life_OnHandCash] call life_fnc_numberText,
	[life_OnBankCash] call life_fnc_numberText,
	(round(damage player)),
	[life_drink] call life_fnc_numberText,
	west countSide playableUnits, 
	independent countSide playableUnits, 
	east countSide playableUnits,
	([(rating player)] call life_fnc_numberText)
];