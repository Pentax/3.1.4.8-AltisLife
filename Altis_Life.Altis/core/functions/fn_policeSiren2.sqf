private["_veh"];
if(playerSide in [west,independent] && vehicle player != player && !life_siren2_active && ((driver vehicle player) == player)) then
		{
            [] spawn
            {
                life_siren2_active = true;
                sleep 3;
                life_siren2_active = false;
            };
            _veh = vehicle player;
            if(isNil {_veh getVariable "siren2"}) then {_veh setVariable["siren2",false,true];};
            if((_veh getVariable "siren2")) then
            {
                titleText ["Horn Off","PLAIN"];
                _veh setVariable["siren2",false,true];
            }
                else
            {
                titleText ["Horn On","PLAIN"];
				_veh setVariable["siren2",true,true];
				if(playerSide == west) then {
					[[_veh],"life_fnc_copSiren2",nil,true] spawn life_fnc_MP;
				} else {
					//I do not have a custom sound for this and I really don't want to go digging for one, when you have a sound uncomment this and change medicSiren.sqf in the medical folder.
					[[_veh],"life_fnc_medicSiren2",nil,true] spawn life_fnc_MP;
				};
            };
        };