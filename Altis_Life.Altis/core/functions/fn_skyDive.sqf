//File:fn_imperialloadout.sqf Author:[midgetgrimm] 
private ["_loadoutName","_action","_guncost"];
if (vehicle player != player) exitWith { hint "Get out of your vehicle!" };
if(!alive player) exitWith {hint"You dead homie.. dafuq";};
_guncost = 25000;
if(life_OnHandCash < _guncost) exitWith {hint format[localize "STR_NOTF_LO_NoCash",_guncost];};
_loadoutName = "Skydiver Suit";
_action = [
			format["If you do not wish to lose your gear, store it in your vehicle and lock it up. Are you sure you wish to clear your current loadout for the :%1: It will cost you $%2 cash.",_loadoutName,[_guncost] call life_fnc_numberText],
			"Purchase SkyDive",
			"Yes",
			"No"
		] call BIS_fnc_guiMessage;
if(_action) then {
			
hint parseText format["You bought the %1 for <t color='#8cff9b'>$%2</t>",_loadoutName,[_guncost] call life_fnc_numberText];
life_OnHandCash =life_OnHandCash - 25000;

if(playerSide in [west,independent]) then {hint"You should really be working, but go have fun!";};			
titleText ["That'll be $25,000 - You'll be jumping from 3500 meters","PLAIN"];

sleep 1;
RemoveAllWeapons player;
{player removeMagazine _x;} foreach (magazines player);
removeUniform player;
removeVest player;
removeBackpack player;
removeGoggles player;
removeHeadGear player;
{
	player unassignItem _x;
	player removeItem _x;
} foreach (assignedItems player);
titleText[format["Now put on your %1 ...",_loadoutName],"PLAIN"];
sleep 3;

player addUniform "U_I_HeliPilotCoveralls";
player addHeadgear "H_PilotHelmetFighter_B";
player addItem "ItemGPS";
player assignItem "ItemGPS";
player addItem "ItemMap";
player assignItem "ItemMap";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addItem "NVGoggles_OPFOR";
player assignItem "NVGoggles_OPFOR";
sleep 2;
titleText ["...don't forget your bag...","PLAIN"];
sleep 2;
player addBackpack "B_Parachute"; 
sleep 2;
player setPos [getPos player select 0, getPos player select 1, 3500];
titleText ["Enjoy your skydive","PLAIN"];
} else {
			hint"Okay scaredy, move out of the way";
};