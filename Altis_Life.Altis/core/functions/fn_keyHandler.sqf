#include <macro.h>
/*
	File: fn_keyHandler.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Main key handler for event 'keyDown'
*/
private ["_handled","_shift","_alt","_code","_ctrl","_alt","_ctrlKey","_veh","_locked","_interactionKey","_mapKey","_interruptionKeys"];
_ctrl = _this select 0;
_code = _this select 1;
_shift = _this select 2;
_ctrlKey = _this select 3;
_alt = _this select 4;
_speed = speed cursorTarget;
_handled = false;

_interactionKey = if(count (actionKeys "User10") == 0) then {219} else {(actionKeys "User10") select 0};
_mapKey = actionKeys "ShowMap" select 0;
//hint str _code;
_interruptionKeys = [17,30,31,32]; //A,S,W,D

//Vault handling...
if((_code in (actionKeys "GetOver") || _code in (actionKeys "salute")) && {(player getVariable ["restrained",false])}) exitWith {
	true;
};

if(life_action_inUse) exitWith {
	if(!life_interrupted && _code in _interruptionKeys) then {life_interrupted = true;};
	_handled;
};
//Hotfix for Interaction key not being able to be bound on some operation systems.
if(count (actionKeys "User10") != 0 && {(inputAction "User10" > 0)}) exitWith {
	//Interaction key (default is Left Windows, can be mapped via Controls -> Custom -> User Action 10)
	if(!life_action_inUse) then {
		[] spawn 
		{
			private["_handle"];
			_handle = [] spawn life_fnc_actionKeyHandler;
			waitUntil {scriptDone _handle};
			life_action_inUse = false;
		};
	};
	true;
};

switch (_code) do
{
	//Space key for Jumping
	case 57:
	{
		if(isNil "jumpActionTime") then {jumpActionTime = 0;};
		if(_shift && {animationState player != "AovrPercMrunSrasWrflDf"} && {isTouchingGround player} && {stance player == "STAND"} && {speed player > 2} && {!life_is_arrested} && {(velocity player) select 2 < 2.5} && {time - jumpActionTime > 1.5}) then {
			jumpActionTime = time; //Update the time.
			[player,true] spawn life_fnc_jumpFnc; //Local execution
			[[player,false],"life_fnc_jumpFnc",nil,FALSE] call life_fnc_MP; //Global execution 
			_handled = true;
		};
	};
	
	//Map Key
	case _mapKey:
	{
		switch (playerSide) do 
		{
			case west: {if(!visibleMap) then {[] spawn life_fnc_copMarkers;}};
			case independent: {if(!visibleMap) then {[] spawn life_fnc_medicMarkers;}};
		};
	};
	
	//Holster / recall weapon.
	case 35:
	{
		if(_shift && !_alt && !_ctrlKey && currentWeapon player != "") then {
			life_curWep_h = currentWeapon player;
			player action ["SwitchWeapon", player, player, 100];
			player switchcamera cameraView;
		};
		
		if(!_shift && !_alt && _ctrlKey && !isNil "life_curWep_h" && {(life_curWep_h != "")}) then {
			if(life_curWep_h in [primaryWeapon player,secondaryWeapon player,handgunWeapon player]) then {
				player selectWeapon life_curWep_h;
			};
		};
		if(!_shift && !_ctrlKey && _alt) then {
			
			if(alive player && (damage player) > 0.05 && !(player getVariable "restrained")) then { 
				[] call life_fnc_selfbloodbag;
			};
		};
	};
	
	//Interaction key (default is Left Windows, can be mapped via Controls -> Custom -> User Action 10)
	case _interactionKey:
	{
		if(!life_action_inUse) then {
			[] spawn 
			{
				private["_handle"];
				_handle = [] spawn life_fnc_actionKeyHandler;
				waitUntil {scriptDone _handle};
				life_action_inUse = false;
			};
		};
	};
	
	//Restraining (Shift + R)
	case 19:
	{
		if(_shift) then {_handled = true;};
		if(_shift && playerSide == west && !isNull cursorTarget && cursorTarget isKindOf "Man" && (isPlayer cursorTarget) && (side cursorTarget in [east,independent]) && alive cursorTarget && cursorTarget distance player < 3.5 && !(cursorTarget getVariable "Escorting") && !(cursorTarget getVariable "restrained") && !(player getVariable["surrender",false]) && speed cursorTarget < 1) then
		{
			[] call life_fnc_restrainAction;
		};
		// Civs - > Civs
		if(_shift) then {_handled = true;};
		if(_shift && playerSide == east && !isNull cursorTarget && cursorTarget isKindOf "Man" && (isPlayer cursorTarget) && alive cursorTarget && cursorTarget distance player < 3.5 && isPlayer cursorTarget && animationState cursorTarget == "Incapacitated" && !(cursorTarget getVariable "Escorting") && !(cursorTarget getVariable "restrained") && !(player getVariable["surrender",false]) && speed cursorTarget < 1) then
		{
			[] call life_fnc_restrainAction;
		};
		if(_alt && !_shift && __GETC__(life_adminlevel > 4)) then {
					[] call life_fnc_adminStealth;
		};
	};
	
	// shift g Knock out, this is experimental and yeah...
	case 34:
	{
		if(_shift) then {_handled = true;};
		if(_shift && playerSide == east && !isNull cursorTarget && cursorTarget isKindOf "Man" && isPlayer cursorTarget && alive cursorTarget && cursorTarget distance player < 4 && speed cursorTarget < 1) then
		{
			if((animationState cursorTarget) != "Incapacitated" && (currentWeapon player == primaryWeapon player OR currentWeapon player == handgunWeapon player) && currentWeapon player != "" && !life_knockout && !(player getVariable["restrained",false]) && !life_istazed && !(player getVariable["surrender",false])) then
			{
				[cursorTarget] spawn life_fnc_knockoutAction;
			};
		};
	};

	//T Key (Trunk)
	case 20:
	{
		if(!_alt && !_ctrlKey) then
		{
			if(vehicle player != player && alive vehicle player) then
			{
				if((vehicle player) in life_vehicles) then
				{
					[vehicle player] call life_fnc_openInventory;
				};
			}
				else
			{
				if((cursorTarget isKindOf "Car" OR cursorTarget isKindOf "Air" OR cursorTarget isKindOf "Ship") && player distance cursorTarget < 7 && vehicle player == player && alive cursorTarget) then
				{
					if(cursorTarget in life_vehicles) then
					{
						[cursorTarget] call life_fnc_openInventory;
					};
				};
			};
		};
		if(!_alt && !_ctrlKey && _shift) then {
				
				if(vehicle player == player) then {
				
						[] spawn life_fnc_pickAxeUse;
				};
				
		};
		if(cursorTarget isKindOf "Land_TentA_F" && alive player && vehicle player == player) then {
			[] spawn life_fnc_openInventory;
		};
	};
	
	//L Key?
	case 38: 
	{
		if(_shift && !_alt && !_ctrlKey) then
		{
		   if (soundVolume != 1) then 
		   {
		   1 fadeSound 1;
			titleText ["EAR PROTECTION OFF", "PLAIN"];
		   }
			else
		   {
			 1 fadeSound 0.25;
			 titleText ["EAR PROTECTION ON", "PLAIN"];
		   };
		};
		/*
		//If cop run checks for turning lights on.
		if(_shift && playerSide in [west,independent]) then {
			if(vehicle player != player) then {
				if(!isNil {vehicle player getVariable "lights"}) then {
					if(playerSide == west) then {
						[vehicle player] call life_fnc_sirenLights;
					} else {
						[vehicle player] call life_fnc_medicSirenLights;
					};
					_handled = true;
				};
			};
		};
		
		if(!_alt && !_ctrlKey) then { [] call life_fnc_radar; };
		*/
	};
	
	//Y Player Menu
	case 21:
	{
		if(!_alt && !_ctrlKey && !_shift && !dialog) then
		{
			[] call life_fnc_p_openMenu;
		};
		if (_shift && !_alt && !_ctrlKey) then
		{
			if (vehicle player == player && !(player getVariable ["restrained", false]) && (animationState player) != "Incapacitated" && !life_istazed) then
			{
				if (player getVariable ["surrender", false]) then
				{
					player setVariable ["surrender", false, true];
				} else
				{
					[] spawn life_fnc_surrender;
				};
			};
		};
	};
	case 25:
	{
		if(_shift && !_alt && !_ctrlKey) then
		{
		   if (soundVolume != 1) then 
		   {
		   1 fadeSound 1;
			titleText ["Ear protection off", "PLAIN"];
		   }
			else
		   {
			 1 fadeSound 0.25;
			 titleText ["Ear protection on", "PLAIN"];
		   };
		};
	};
	/*
	//F Key
	case 33:
	{
		
		
		if(_shift && playerSide in [west,independent] && vehicle player != player && !life_siren_active && ((driver vehicle player) == player)) then
		{
			[] spawn
			{
				life_siren_active = true;
				sleep 7.7;
				life_siren_active = false;
			};
			_veh = vehicle player;
			if(isNil {_veh getVariable "siren"}) then {_veh setVariable["siren",false,true];};
			if((_veh getVariable "siren")) then
			{
				titleText [localize "STR_MISC_SirensOFF","PLAIN"];
				_veh setVariable["siren",false,true];
			}
				else
			{
				titleText [localize "STR_MISC_SirensON","PLAIN"];
				_veh setVariable["siren",true,true];
				if(playerSide == west) then {
					[[_veh],"life_fnc_copSiren",nil,true] spawn life_fnc_MP;
				} else {
					//I do not have a custom sound for this and I really don't want to go digging for one, when you have a sound uncomment this and change medicSiren.sqf in the medical folder.
					[[_veh],"life_fnc_medicSiren",nil,true] spawn life_fnc_MP;
				};
			};
		};
		
		if(_alt && playerSide in [west,independent] && vehicle player != player && !life_siren2_active && ((driver vehicle player) == player)) then
		{
            [] spawn
            {
                life_siren2_active = true;
                sleep 3;
                life_siren2_active = false;
            };
            _veh = vehicle player;
            if(isNil {_veh getVariable "siren2"}) then {_veh setVariable["siren2",false,true];};
            if((_veh getVariable "siren2")) then
            {
                titleText ["Horn Off","PLAIN"];
                _veh setVariable["siren2",false,true];
            }
                else
            {
                titleText ["Horn On","PLAIN"];
				_veh setVariable["siren2",true,true];
				if(playerSide == west) then {
					[[_veh],"life_fnc_copSiren2",nil,true] spawn life_fnc_MP;
				} else {
					//I do not have a custom sound for this and I really don't want to go digging for one, when you have a sound uncomment this and change medicSiren.sqf in the medical folder.
					[[_veh],"life_fnc_medicSiren2",nil,true] spawn life_fnc_MP;
				};
            };
        };
		
		
	};
	*/
	//U Key
	case 22:
	{
		if(!_alt && !_ctrlKey) then
		{
			if(vehicle player == player) then
			{
				_veh = cursorTarget;
			}
				else
			{
				_veh = vehicle player;
			};
			
			_locked = locked _veh;
			
			if(_veh in life_vehicles && player distance _veh < 8) then
			{
				if(_locked == 2) then
				{
					if(local _veh) then
					{
						_veh lock 0;
					}
						else
					{
						[[_veh,0], "life_fnc_lockVehicle",_veh,false] spawn life_fnc_MP;
					};
					systemChat localize "STR_MISC_VehUnlock";
					player say3d "vehicleLock";
					titleText["You have unlocked your vehicle","PLAIN"];
				}
					else
				{
					if(local _veh) then
					{
						_veh lock 2;
					}
						else
					{
						[[_veh,2], "life_fnc_lockVehicle",_veh,false] spawn life_fnc_MP;
					};
					systemChat localize "STR_MISC_VehLock";
					player say3d "vehicleLock";
				};
			};
		};
	};
	// O, police gate opener
        case 24:
	{
		if (!_shift && !_alt && !_ctrlKey && (playerSide in [west,independent])) then {
			[] call life_fnc_copOpener;
		};
		if (!_shift && !_alt && !_ctrlKey && (__GETC__(life_adminlevel) > 3 ) && (playerSide == east)) then {
			[] call life_fnc_copOpener;
		};
		if (!_shift && _alt && !_ctrlKey) then {
			[] call life_fnc_replaceOrgan;
		};
	};
	
	
	//tab
	case 15:
	{
		if (_alt && !_shift) then {
				
				[] call SOCK_fnc_updateRequest;
		}

		
	};
	//F4
	case 62:
	{
			if(_alt && !_shift) then {
				[] call SOCK_fnc_updateRequest;
			};
	};
	//debug tilde
	case 211:
	{
			hintSilent parseText format ["
	<t size='1.15' font='puristaLight' align='left'>Cash: </t><t size='1.15' font='puristaLight' align='right'>%1</t><br/>
	<t size='1.15' font='puristaLight' align='left'>Bank: </t><t size='1.15' font='puristaLight' align='right'>%2</t><br/>
	<t size='1.15' font='puristaLight' align='left'>Health: </t><t size='1.15' font='puristaLight' align='right'>%3</t><br/>
	<t size='1.15' font='puristaLight' align='left'>BAC Level: </t><t size='1.15' font='puristaLight' align='right'>%4</t><br/>
	<br/>
	<t size='1.15' font='puristaLight' align='left'>Police Online: </t><t size='1.15' font='puristaLight' align='right'>%5</t><br/>
	<t size='1.15' font='puristaLight' align='left'>Medics Online: </t><t size='1.15' font='puristaLight' align='right'>%6</t><br/>
	<t size='1.15' font='puristaLight' align='left'>Civilians Online: </t><t size='1.15' font='puristaLight' align='right'>%7</t><br/>
	<t size='1.15' font='puristaLight' align='left'>Score: </t><t size='1.15' font='puristaLight' align='right'>%8</t><br/>
	<br/>
	<t size='1.15' font='puristaLight' align='left'>Website: </t><t size='1.15' font='puristaLight' align='right'>www.GrimmLife.com</t><br/>
	<t size='1.15' font='puristaLight' align='left'>Teamspeak: </t><t size='1.15' font='puristaLight' align='right'>ts.grimmlife.com</t><br/>
	<br/>
	<br/>
	<t size='1.15' font='puristaLight' align='center' color='FFCC00'>Thank you for playing on<br/>A Grimm Life Altis Life Server<br/></t><br/>
	<br/>",
	[life_OnHandCash] call life_fnc_numberText,
	[life_OnBankCash] call life_fnc_numberText,
	round((1 - (damage player)) * 100),
	[life_drink] call life_fnc_numberText,
	west countSide playableUnits, 
	independent countSide playableUnits, 
	east countSide playableUnits,
	([(rating player)] call life_fnc_numberText)
];
	};
	case 4:
	{
		if (!_shift && !_alt && !_ctrlKey && (__GETC__(life_adminlevel) >= 5 )) then {
		
			if (isNil "admin_tpdirection_0n") exitWith {};
			if (!admin_tpdirection_0n) exitWith {};
		
			_distance = 10;
			_object = (vehicle player); 
			_dir = getdir _object;
			_pos = getPosATL _object;
			if (_object isKindOf "Air") then
			{
				if (count(crew _object)>1) then 
				{ 
					_distance = 10;
					_pos = [(_pos select 0)+_distance*sin(_dir),(_pos select 1)+_distance*cos(_dir),((getpos _object) select 2)+100];
					if (surfaceIsWater _pos) then {_object setPosASL _pos;} else {_object setPosATL _pos;};
				} 
				else
				{
					_distance = 50;
					_pos = [(_pos select 0)+_distance*sin(_dir),(_pos select 1)+_distance*cos(_dir),0];
					if (surfaceIsWater _pos) then {_pos = [(_pos select 0)+_distance*sin(_dir),(_pos select 1)+_distance*cos(_dir),2];};
					if ((getpos _object) select 2 > 6) then {_pos = [(_pos select 0)+_distance*sin(_dir),(_pos select 1)+_distance*cos(_dir),((getpos _object) select 2)+0.1]};
					if (surfaceIsWater _pos) then {_object setPosASL _pos;} else {_object setPosATL _pos;};
				};
			}
			else
			{
				_distance = 10;
				_pos = [(_pos select 0)+_distance*sin(_dir),(_pos select 1)+_distance*cos(_dir),0];
				if (surfaceIsWater _pos) then {_pos = [(_pos select 0)+_distance*sin(_dir),(_pos select 1)+_distance*cos(_dir),2];};
				if ((getpos _object) select 2 > 3) then {_pos = [(_pos select 0)+_distance*sin(_dir),(_pos select 1)+_distance*cos(_dir),((getpos _object) select 2)];};
				if (surfaceIsWater _pos) then {_object setPosASL _pos;} else {_object setPosATL _pos;};
			};
		};
		if(_alt && !_shift && !_ctrlKey) then {[radio_shoutMessage_4, radio_textImg_4] call life_fnc_shoutSpeech;};
	};
	case 5:
	{
		if (!_shift && !_alt && !_ctrlKey && (__GETC__(life_adminlevel) >= 5 )) then {
		
			if (isNil "admin_tpdirection_0n") exitWith {};
			if (!admin_tpdirection_0n) exitWith {};
				_vehicle = (vehicle player);
				_vel = velocity _vehicle;
				if ((vehicle player)==player) then
				{
					_vehicle setVelocity [(_vel select 0),(_vel select 1),8];
				}
				else
				{
					_vehicle setVelocity [(_vel select 0),(_vel select 1),20];
				};
		};
		if(_alt && !_shift && !_ctrlKey) then {[radio_shoutMessage_3, radio_textImg_3] call life_fnc_shoutSpeech;};
	};
	case 2:
	{
			if(_alt && !_shift && !_ctrlKey) then {[radio_shoutMessage_1, radio_textImg_1] call life_fnc_shoutSpeech;};
	};
	case 3:
	{
			if(_alt && !_shift && !_ctrlKey) then {[radio_shoutMessage_2, radio_textImg_2] call life_fnc_shoutSpeech;};
	};	
	case 6:
	{
			if(_alt && !_shift && !_ctrlKey) then {[radio_shoutMessage_5, radio_textImg_5] call life_fnc_shoutSpeech;};
	};	
	//NEXT ONE HERE [[player,"Acts_EpicSplit_out"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
};

if (_code in (actionKeys "Throw") && (player getVariable "restrained" OR player getVariable "transporting" OR player getVariable "surrender" or (animationState player == "Incapacitated"))) then {
	_handled = true;
};

if (_code in (actionKeys "TacticalView")) then
{
	//hint "Tactical View is disabled.";
	_handled = true;
};

_handled;