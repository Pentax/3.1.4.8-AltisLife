/*
	File: fn_hudSetup.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Setups the hud for the player?
*/
private["_display","_alpha","_version","_p","_pg","_moneyATM","_money","_ponline"];
disableSerialization;
_display = findDisplay 46;
_alpha = _display displayCtrl 1001;
_version = _display displayCtrl 1000;

2 cutRsc ["playerHUD","PLAIN"];
_version ctrlSetText format["BETA: 0.%1.%2",(productVersion select 2),(productVersion select 3)];
[] call life_fnc_hudUpdate;

[] spawn
{
	private["_dam","_money","_moneyATM"];
	while {true} do
	{
		_dam = damage player;
		_money = life_OnHandCash;
		_moneyATM = life_OnBankCash;
		//_ponline = (count playableUnits);
		//_fps = (round diag_fps);
		waitUntil {
			(damage player) != _dam || 
			(life_OnHandCash) != _money || 
			(life_OnBankCash) != _moneyATM
			//(count playableUnits) != _ponline
			//(round diag_fps) != _fps
		};
		[] call life_fnc_hudUpdate;
	};
};
