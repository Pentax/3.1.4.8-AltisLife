/*
	File: fn_hudUpdate.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Updates the HUD when it needs to.
*/
private["_ui","_food","_water","_health","_cash","_bank","_ponline"];
disableSerialization;

_ui = uiNameSpace getVariable ["playerHUD",displayNull];
if(isNull _ui) then {[] call life_fnc_hudSetup;};
_food = _ui displayCtrl 23500;
_water = _ui displayCtrl 23510;
_health = _ui displayCtrl 23515;
_cash = _ui displayCtrl 23520;
_bank = _ui displayCtrl 23525;
_ponline = _ui displayCtrl 23530;

//Update food
_food ctrlSetPosition [safeZoneX+safeZoneW-0.361,safeZoneY+safeZoneH-0.1];
_food ctrlSetText format["%1", life_hunger];
_food ctrlCommit 0;
//Update Water
_water ctrlSetPosition [safeZoneX+safeZoneW-0.492,safeZoneY+safeZoneH-0.1];
_water ctrlSetText format["%1", life_thirst];
_water ctrlCommit 0;
//Update Health
_health ctrlSetPosition [safeZoneX+safeZoneW-0.625,safeZoneY+safeZoneH-0.1];
_health ctrlSetText format["%1", round((1 - (damage player)) * 100)];
_health ctrlCommit 0;
//update money

//update player online
_ponline ctrlSetPosition [safeZoneX+safeZoneW-0.763,safeZoneY+safeZoneH-0.1];
_ponline ctrlSetText format["%1",(count playableUnits)];
_ponline ctrlCommit 0;

_cash ctrlSetPosition [safeZoneX+safeZoneW-0.845,safeZoneY+safeZoneH-0.1];
_cash ctrlSetText format["$%1",[life_OnHandCash] call life_fnc_numberText];
_cash ctrlCommit 0;

_bank ctrlSetPosition [safeZoneX+safeZoneW-0.975,safeZoneY+safeZoneH-0.1];
_bank ctrlSetText format["$%1",[life_OnBankCash] call life_fnc_numberText];
_bank ctrlCommit 0;
