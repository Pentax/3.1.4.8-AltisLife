/*
	File: fn_useItem.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Main function for item effects and functionality through the player menu.
*/
private["_item"];
disableSerialization;
if((lbCurSel 2005) == -1) exitWith {hint "You need to select an item first!";};
_item = lbData[2005,(lbCurSel 2005)];

switch (true) do
{
	
	case (_item == "boltcutter"): {
		[cursorTarget] spawn life_fnc_boltcutter;
		closeDialog 0;
	};
	
	case (_item == "blastingcharge"): {
		[cursorTarget] spawn life_fnc_blastingCharge;
		player reveal fed_bank;
		(group player) reveal fed_bank;
		closeDialog 0;
	};
	case (_item == "underwatercharge"): {
		player reveal gold_safe;
		(group player) reveal gold_safe;
		[cursorTarget] spawn life_fnc_underwaterCharge;
		closeDialog 0;
	};
	
	case (_item == "civdefusekit"): {
		[cursorTarget] spawn life_fnc_bombDefuseKit;
	};
	case (_item == "defusekit"): {
		[cursorTarget] spawn life_fnc_defuseKit;
	};
	case (_item == "water"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			life_thirst = 100;
			player setFatigue 0;
		};
	};
	case (_item == "coffee"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			life_drink = 0;
			life_thirst = 100;
			player setFatigue 0;
		};
	};
	
	case (_item == "tenta"):
	{
		[] spawn life_fnc_tent; closeDialog 0;
	};
	case (_item == "campfire"):
	{
		if(!isNull life_campfire) exitWith {hint "You already have a campfire lit somewhere else"};
		if((player getVariable ["fireplaced",FALSE])) exitWith {hint "You already have a fireplace, clean it up first";};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[] spawn life_fnc_campfire;closeDialog 0;
		};
		/*
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			titleText["You start building a campfire...","PLAIN"];
			_campfire = "Land_Campfire_F" createVehicle (getPos player);
			[[_campfire],"life_fnc_simDisable",false,false] spawn BIS_fnc_MP;
			_campfire setVariable ["campfire", player, true];
		};
		*/
	};
	case (_item == "redgull"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			life_thirst = 100;
			player setFatigue 0;
			[] spawn
			{
				life_redgull_effect = time;
				titleText["You can now run farther for 3 minutes","PLAIN"];
				//player enableFatigue false;
				waitUntil {!alive player OR ((time - life_redgull_effect) > (3 * 60))};
				//player enableFatigue true;
			};
		};
	};
	
	case (_item == "mtdew"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			life_thirst = 100;
			player setFatigue 0;
			[] spawn
			{
				life_redgull_effect = time;
				titleText["You're all jacked up on Mountain Dew...GO","PLAIN"];
				//player enableFatigue false;
				waitUntil {!alive player OR ((time - life_redgull_effect) > (1 * 60))};
				//player enableFatigue true;
			};
		};
	};
	
case (_item == "spikeStrip"):
	{
		if(!isNull life_spikestrip) exitWith {hint localize "STR_ISTR_SpikesDeployment"};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[] spawn life_fnc_spikeStrip;
		};
	};
	
	case (_item == "heroinp"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[] spawn life_fnc_heroine;closeDialog 0;
		};
	};
	
	case (_item =="bottledwhiskey"):
	{
		if(playerSide in [west,independent]) exitWith {hint localize "STR_MISC_WestIndNoNo";};
		if((player getVariable ["inDrink",FALSE])) exitWith {hint localize "STR_MISC_AlreadyDrinking";};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			if(isNil "life_drink") then {life_drink = 0;};
			life_drink = life_drink + 0.06;
			if (life_drink < 0.07) exitWith {};
			[] spawn life_fnc_drinkwhiskey;
		};
	};
	
	case (_item =="bottledshine"):
	{
		if(playerSide in [west,independent]) exitWith {hint localize "STR_MISC_WestIndNoNo";};
		if((player getVariable ["inDrink",FALSE])) exitWith {hint localize "STR_MISC_AlreadyDrinking";};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			if(isNil "life_drink") then {life_drink = 0;};
			life_drink = life_drink + 0.08;
			if (life_drink < 0.09) exitWith {};
			[] spawn life_fnc_drinkmoonshine;
		};
	};
	
	case (_item =="bottledbeer"):
	{
		
		if(playerSide in [west,independent]) exitWith {hint localize "STR_MISC_WestIndNoNo";};
		if((player getVariable ["inDrink",FALSE])) exitWith {hint localize "STR_MISC_AlreadyDrinking";};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			if(isNil "life_drink") then {life_drink = 0;};
			life_drink = life_drink + 0.02;
			if (life_drink < 0.06) exitWith {};
			[] spawn life_fnc_drinkbeer;
		};
	};
	
	case (_item == "fuelF"):
	{
		if(vehicle player != player) exitWith {hint "You can't refuel the vehicle while in it!"};
		[] spawn life_fnc_jerryRefuel;
	};
	
	case (_item == "marijuana"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[] spawn life_fnc_weed;
		};
	};
	case (_item == "methp" or _item == "cocainep"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[] spawn life_fnc_meth;
		};
	};
	
	case (_item == "krokp"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[] spawn life_fnc_krok;
		};
	};
	case (_item == "lockpick"):
	{
		[] spawn life_fnc_lockpick;
		closeDialog 0;
	};
	case (_item == "scalpel"):
	{
		[] spawn life_fnc_scalpel;
		closeDialog 0;
	};
	case (_item == "bloodbag"):
	{
		[] spawn life_fnc_selfbloodbag;
		closeDialog 0;
	};
	case (_item == "ivkit"):
	{
		[] spawn life_fnc_bloodbag;
		closeDialog 0;
	};
	case (_item == "organttk"):
	{
		[] spawn life_fnc_organTransplantKit;
		closeDialog 0;
	};
	case (_item == "kidney"):
	{
		[] spawn life_fnc_replaceOrgan;
		closeDialog 0;
	};
	case (_item in ["apple","rabbit","salema","ornate","mackerel","tuna","mullet","catshark","turtle","turtlesoup","donuts","tbacon","peach","gyros","burgers","pizza","chips","hen","goat","rooster","sheep","rabbit","henC","goatC","roosterC","sheepC","rabbitC","berries"]):
	{
		[_item] call life_fnc_eatFood;
	};	
	case "fishing":
	{
		[] spawn fnc_fishing;
	};
	
	case (_item == "pickaxe"):
	{
		[] spawn life_fnc_pickAxeUse;
	};
	
	case (_item == "demolitioncharge"):
	{
		
		player reveal prison_safe;
		(group player) reveal prison_safe;
		[cursorTarget] spawn life_fnc_wallCharge;
		closeDialog 0;
	};
	
	default
	{
		hint "This item isn't usable.";
	};
};
	
[] call life_fnc_p_updateMenu;
[] call life_fnc_hudUpdate;