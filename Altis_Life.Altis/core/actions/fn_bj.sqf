_store = blackjack;
_denied1 = false;
_price = 2500;

if(life_OnHandCash < _price) exitWith {hint "You do not have the minimum required wage $2,500";};
life_OnHandCash =life_OnHandCash - _price;
removeAllActions blackjack;



				hint "Let's Play'!";
				_number = ceil(random 10);
				_pplayer = _this select 0;
				if (_number == 0) then {robberyreward = 5000; hint "19! Great, you win $5,000!";};
				if (_number == 1) then {robberyreward = 0; hint "18! Lose";};
				if (_number == 2) then {robberyreward = 0; hint "8!Lost...";};						
				if (_number == 3) then {robberyreward = 0; hint "14! Maybe next time...";};
				if (_number == 4) then {robberyreward = 0; hint "16! Just missed!";};
				if (_number == 5) then {robberyreward = 0; hint "12! Amazing, my grandmother plays better than you!";};
				if (_number == 6) then {robberyreward = 0; hint "13! Lost, you were wrong ..!";};
				if (_number == 7) then {robberyreward = 7500; hint "20! Wow, you won $7,500!";};
				if (_number == 8) then {robberyreward = 0; hint "10! Even split !";};
				if (_number == 9) then {robberyreward = 0; hint "11! Loser!";};
				if (_number == 10) then {robberyreward = 10000; hint "!!!21!!!";};
				life_action_inUse = true;
	

sleep 2;
_robberycash = robberyreward;
_timer = time + (1 * 5);	
_toFar = false;
_vault = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;

while {true} do
{
	//Timer display (TO BE REPLACED WITH A NICE GUI LAYER)
	_countDown = if(round(_timer - time) > 60) then {format["%1 minute(s)",round(round(_timer - time) / 60)]} else {format["%1 second(s)",round(_timer - time)]};
	hintSilent format["You must stay at the table and wait your turn!\n\n Time Left:\n %1\n\n Distance: %2m",_countDown,round(player distance _vault)];

	if(player distance _vault > 5) exitWith {_toFar = true;};
	if((round(_timer - time)) < 1) exitWith {};
	if(!alive player) exitWith {};
	if(life_istazed) exitWith {hint "You were tazed, get off the table";};
};

switch(true) do
{
	case (_toFar):
	{
		hint "You are too far from the table.";
		_denied1 = false;
		life_action_inUse = false;
	};
	
	case (!alive player):
	{
		hint "The game went wrong, because you're dead ..";
		_denied1 = false;
		life_action_inUse = false;
	};
	
	case (life_istazed):
	{
		hint "You were tazed";
		_denied1 = false;
		life_action_inUse = false;
	};
	
	case ((round(_timer - time)) < 1):
	{
		hint format["You won $%1.", _robberycash];
		life_OnHandCash =life_OnHandCash + _robberycash;
		_denied1 = false;
		life_action_inUse = false;
	};	
};

sleep 10;
blackjack addAction["Play BlackJack ($2,500)",life_fnc_bj,"blackjack"];
life_action_inUse = false;