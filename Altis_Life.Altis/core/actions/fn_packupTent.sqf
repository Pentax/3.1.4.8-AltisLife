/*	File: fn_packupSpikes.sqf	Author: Bryan "Tonic" Boardwine*/
private["_tents"];
_tents = nearestObjects[getPos player,["Land_TentA_F"],8] select 0;

if(isNil "_tents") exitWith {};

if(([true,"tenta",1] call life_fnc_handleInv)) then {titleText[localize "STR_NOTF_campfire","PLAIN"];

player removeAction life_action_tentPickup;

life_action_tentPickup = nil;

deleteVehicle _tents;};
player setVariable["tentplaced",false,true];