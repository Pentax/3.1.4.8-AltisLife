#include <macro.h>
/*
	Author: Bryan "Tonic" Boardwine

	Description:
	Blah blah.
*/
private["_group","_hideout","_action","_cpRate","_cP","_progressBar","_title","_titleText","_ui","_flagTexture","_gangName","_marker","_markerText","_dataCiv","_addExpAmount","_time"];
_hideout = (nearestObjects[getPos player,["Land_cargo_house_slum_F","Land_Cargo_House_V2_F"],50]) select 0;
_group = _hideout getVariable ["gangOwner",grpNull];

//break up if civilian or other ///////////////////////////////////////////////////

if(playerSide == east) then {
		if(life_inv_cannabis < 1) exitWith {hint "You should probably have some pot plants on you before you try to take the area";};
		if((west countSide playableUnits) < cops_onlineLess) exitWith {hint format [localize "STR_Civ_NotEnoughCopsHO",cops_onlineLess];};
		if(isNil {grpPlayer getVariable "gang_name"}) exitWith {titleText[localize "STR_GNOTF_CreateGang","PLAIN"];};
		_dataCiv = missionNamespace getVariable "Weed_Prof";
		_addExpAmount = 25;_time = 0.005; _cpRate = 0.009;
		if((_dataCiv select 0) > 0) then {
				_addExpAmount = _addExpAmount + round(2.5 * (_dataCiv select 0));
		};
		switch ( _dataCiv select 0 ) do {
			case 0: { _time = 0.005; _cpRate = 0.009; };
			case 1: { _time = 0.006; _cpRate = 0.01; };
			case 2: { _time = 0.007; _cpRate = 0.01; };
			case 3: { _time = 0.008; _cpRate = 0.01; };
			case 4: { _time = 0.009; _cpRate = 0.01; };
			case 5: { _time = 0.01; _cpRate = 0.01; };
			case 6: { _time = 0.015; _cpRate = 0.02; };
			case 7: { _time = 0.02; _cpRate = 0.03; };
			case 8: { _time = 0.025; _cpRate = 0.04; };
			case 9: { _time = 0.03; _cpRate = 0.05; };
			case 10: { _time = 0.035; _cpRate = 0.07; };
			case 11: { _time = 0.04; _cpRate = 0.08; };
			case 12: { _time = 0.045; _cpRate = 0.09; };
			case 13: { _time = 0.05; _cpRate = 0.09; };
			case 14: { _time = 0.055; _cpRate = 0.09; };
			case 15: { _time = 0.06; _cpRate = 0.10; };
			case 16: { _time = 0.065; _cpRate = 0.11; };
			case 17: { _time = 0.07; _cpRate = 0.12; };
			case 18: { _time = 0.075; _cpRate = 0.12; };
			case 19: { _time = 0.08; _cpRate = 0.13; };
			case 20: { _time = 0.085; _cpRate = 0.14; };
		};
		if(_group == grpPlayer) exitWith {titleText[localize "STR_GNOTF_Controlled","PLAIN"];};
		if((_hideout getVariable ["inCapture",FALSE])) exitWith {hint localize "STR_GNOTF_Captured";};
		if(!isNull _group) then {
			_gangName = _group getVariable ["gang_name",""];
			_action = [
				format[localize "STR_GNOTF_AlreadyControlled",_gangName],
				localize "STR_GNOTF_CurrentCapture",
				localize "STR_Global_Yes",
				localize "STR_Global_No"
			] call BIS_fnc_guiMessage;
			_hideout setVariable["taken",false,true];
			_cpRate = _time;
			[[1,format["%1 and his gang: %2 are attempting to retake a local hideout",name player,(group player) getVariable "gang_name"]],"life_fnc_broadcast",true,false] spawn life_fnc_MP;
		} else {
			_cpRate = _cpRate;
		};

		if(!isNil "_action" && {!_action}) exitWith {titleText[localize "STR_GNOTF_CaptureCancel","PLAIN"];};
		life_action_inUse = true;
		//Setup the progress bar
		disableSerialization;
		_title = localize "STR_GNOTF_Capturing";
		5 cutRsc ["life_progress","PLAIN"];
		_ui = uiNamespace getVariable "life_progress";
		_progressBar = _ui displayCtrl 38201;
		_titleText = _ui displayCtrl 38202;
		_titleText ctrlSetText format["%2 (1%1)...","%",_title];
		_progressBar progressSetPosition 0.01;
		_cP = 0.01;

		while {true} do
		{
			if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
				[[player,"AinvPknlMstpSnonWnonDnon_medic_1",true],"life_fnc_animSync",true,false] call life_fnc_MP;
				//player switchMove "AinvPknlMstpSnonWnonDnon_medic_1";
				player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
			};
			sleep 0.26;
			if(isNull _ui) then {
				5 cutRsc ["life_progress","PLAIN"];
				_ui = uiNamespace getVariable "life_progress";
				_progressBar = _ui displayCtrl 38201;
				_titleText = _ui displayCtrl 38202;
			};
			_cP = _cP + _cpRate;
			_progressBar progressSetPosition _cP;
			_titleText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_title];
			_hideout setVariable["inCapture",true,true];
			if(_cP >= 1 OR !alive player) exitWith {_hideout setVariable["inCapture",false,true];};
			if(life_istazed) exitWith {_hideout setVariable["inCapture",false,true];}; //Tazed
			if(life_interrupted) exitWith {_hideout setVariable["inCapture",false,true];};
		};

		//Kill the UI display and check for various states
		5 cutText ["","PLAIN"];
		player playActionNow "stop";
		if(!alive player OR life_istazed) exitWith {life_action_inUse = false;_hideout setVariable["inCapture",false,true];};
		if((player getVariable["restrained",false])) exitWith {life_action_inUse = false;_hideout setVariable["inCapture",false,true];};
		if(life_interrupted) exitWith {life_interrupted = false; titleText[localize "STR_GNOTF_CaptureCancel","PLAIN"]; life_action_inUse = false;_hideout setVariable["inCapture",false,true];};
		life_action_inUse = false;

		titleText[localize "STR_GNOTF_CaptureSuccess1","PLAIN"];
		_flagTexture = [
				"\A3\Data_F\Flags\Flag_red_CO.paa",
				"\A3\Data_F\Flags\Flag_green_CO.paa",
				"\A3\Data_F\Flags\Flag_blue_CO.paa",
				"\A3\Data_F\Flags\Flag_white_CO.paa",
				"\A3\Data_F\Flags\flag_fd_red_CO.paa",
				"\A3\Data_F\Flags\flag_fd_green_CO.paa",
				"\A3\Data_F\Flags\flag_fd_blue_CO.paa",
				"\A3\Data_F\Flags\flag_fd_orange_CO.paa"
			] call BIS_fnc_selectRandom;
		_this select 0 setFlagTexture _flagTexture;
		//_gangName = _group getVariable ["gang_name",""];
		[[[0,1],"STR_GNOTF_CaptureSuccess",true,[name player,(group player) getVariable "gang_name"]],"life_fnc_broadcast",true,false] spawn life_fnc_MP;
		_hideout setVariable["inCapture",false,true];
		_hideout setVariable["gangOwner",grpPlayer,true];
		_hideout setVariable["taken",true,true];
		["Weed_Prof",_addExpAmount] call life_fnc_addExp;
		[[0,format["You have earned %1 XP for capturing the hideout", _addExpAmount]],"life_fnc_broadcast",player,false] spawn life_fnc_MP;
		_Pos = position _hideout;
		deleteMarker "Marker201";
		deleteMarker "MarkerText201";
		 _marker = createMarker ["Marker201", _Pos];
		"Marker201" setMarkerColor "ColorRed";
		"Marker201" setMarkerType "Empty";
		"Marker201" setMarkerShape "ELLIPSE";
		"Marker201" setMarkerSize [400,400];
		 _markerText = createMarker ["MarkerText201", _Pos];
		"MarkerText201" setMarkerColor "ColorBlack";
		"MarkerText201" setMarkerText format["%1 controls this area",(group player) getVariable "gang_name"];
		"MarkerText201" setMarkerType "mil_warning";

} else {

		//if cop ////////////////////////////////////////////////////////
		if((_hideout getVariable ["inRetake",FALSE])) exitWith {hint localize "STR_GNOTF_Captured";};
		if(!(_hideout getVariable ["taken",false])) exitWith {hint "This gang hideout is not under any gang control";};
		_cpRate = 0.0055;
		[[1,format["Policeman %1 is retaking a hideout and cleaning up the streets",name player]],"life_fnc_broadcast",true,false] spawn life_fnc_MP;
		life_action_inUse = true;

		//Setup the progress bar
		disableSerialization;
		_title = localize "STR_GNOTF_CapturingCop";
		5 cutRsc ["life_progress","PLAIN"];
		_ui = uiNamespace getVariable "life_progress";
		_progressBar = _ui displayCtrl 38201;
		_titleText = _ui displayCtrl 38202;
		_titleText ctrlSetText format["%2 (1%1)...","%",_title];
		_progressBar progressSetPosition 0.01;
		_cP = 0.01;

		while {true} do
		{
			if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
				[[player,"AinvPknlMstpSnonWnonDnon_medic_1",true],"life_fnc_animSync",true,false] call life_fnc_MP;
				//player switchMove "AinvPknlMstpSnonWnonDnon_medic_1";
				player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
			};
			sleep 0.26;
			if(isNull _ui) then {
				5 cutRsc ["life_progress","PLAIN"];
				_ui = uiNamespace getVariable "life_progress";
				_progressBar = _ui displayCtrl 38201;
				_titleText = _ui displayCtrl 38202;
			};
			_cP = _cP + _cpRate;
			_progressBar progressSetPosition _cP;
			_titleText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_title];
			_hideout setVariable["inRetake",true,true];
			if(_cP >= 1 OR !alive player) exitWith {_hideout setVariable["inRetake",false,true];};
			if(life_istazed) exitWith {_hideout setVariable["inRetake",false,true];}; //Tazed
			if(life_interrupted) exitWith {_hideout setVariable["inRetake",false,true];};
		};

		//Kill the UI display and check for various states
		5 cutText ["","PLAIN"];
		player playActionNow "stop";
		if(!alive player OR life_istazed) exitWith {life_action_inUse = false;_hideout setVariable["inRetake",false,true];};
		if((player getVariable["restrained",false])) exitWith {life_action_inUse = false;_hideout setVariable["inRetake",false,true];};
		if(life_interrupted) exitWith {life_interrupted = false; titleText[localize "STR_GNOTF_CaptureCancel","PLAIN"]; life_action_inUse = false;_hideout setVariable["inRetake",false,true];};
		life_action_inUse = false;

		titleText[localize "STR_GNOTF_CaptureSuccess1Cop","PLAIN"];
		_flagTexture = [
				"\A3\Data_F\Flags\Flag_red_CO.paa",
				"\A3\Data_F\Flags\Flag_green_CO.paa",
				"\A3\Data_F\Flags\Flag_blue_CO.paa",
				"\A3\Data_F\Flags\Flag_white_CO.paa",
				"\A3\Data_F\Flags\flag_fd_red_CO.paa",
				"\A3\Data_F\Flags\flag_fd_green_CO.paa",
				"\A3\Data_F\Flags\flag_fd_blue_CO.paa",
				"\A3\Data_F\Flags\flag_fd_orange_CO.paa"
			] call BIS_fnc_selectRandom;
		_this select 0 setFlagTexture _flagTexture;
		//_gangName = _group getVariable ["gang_name",""];
		[[[0,1],"STR_GNOTF_CaptureSuccessCop",true,name player],"life_fnc_broadcast",true,false] spawn life_fnc_MP;
		_hideout setVariable["inRetake",false,true];
		_hideout setVariable["gangOwner",false,true];
		_hideout setVariable["taken",false,true];
		deleteMarker "Marker201";
		deleteMarker "MarkerText201";
		life_OnHandCash = life_OnHandCash + 10000;
		titleText ["Good job cleaning up the streets, here's a bonus $10k","PLAIN"];

};