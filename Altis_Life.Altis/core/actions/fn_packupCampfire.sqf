/*	File: fn_packupSpikes.sqf	Author: Bryan "Tonic" Boardwine*/
private["_campfires"];
_campfires = nearestObjects[getPos player,["Land_Campfire_F"],8] select 0;

if(isNil "_campfires") exitWith {};

if(([true,"campfire",1] call life_fnc_handleInv)) then {titleText[localize "STR_NOTF_campfire","PLAIN"];

player removeAction life_action_campfirePickup;
player removeAction life_action_cookMeat;

life_action_campfirePickup = nil;
life_action_cookMeat = nil;

deleteVehicle _campfires;};
player setVariable["fireplaced",false,true];