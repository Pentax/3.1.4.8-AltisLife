/*
	File: fn_medicLoadout.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Loads the medic out with the default gear.
*/
removeAllContainers player;
removeAllWeapons player;
player forceAddUniform "U_Competitor";
player addItem "ItemGPS";
player assignItem "ItemGPS";
player addItem "ItemMap";
player assignItem "ItemMap";
player addItem "ItemRadio";
player assignItem "ItemRadio";
player addBackPack "B_Kitbag_cbr";
mybackpack = unitBackpack player;
mybackpack addItemCargoGlobal ["NVGoggles", 3];
mybackpack addItemCargoGlobal ["Medikit", 1];
mybackpack addItemCargoGlobal ["Toolkit", 1];
removeGoggles player;
removeHeadGear player;
if(hmd player != "") then {
	player unlinkItem (hmd player);
};
[[player,0,"textures\medic_uniform.jpg"],"life_fnc_setTexture",true,false] spawn life_fnc_MP;
[] call life_fnc_saveGear;