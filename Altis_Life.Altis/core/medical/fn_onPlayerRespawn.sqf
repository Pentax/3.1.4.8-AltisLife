#include <macro.h>
/*
	File: fn_onPlayerRespawn.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Does something but I won't know till I write it...
*/
private["_unit","_corpse"];
_unit = _this select 0;
_corpse = _this select 1;
life_corpse = _corpse;

//Set some vars on our new body.
_unit setVariable["restrained",FALSE,TRUE];
_unit setVariable["Escorting",FALSE,TRUE];
_unit setVariable["transporting",FALSE,TRUE];
_unit setVariable["missingOrgan",FALSE,TRUE]; 
_unit setVariable["hasOrgan",FALSE,TRUE]; 
_unit setVariable["bloodBagged",FALSE,TRUE]; 
_unit setVariable["inSurgery",FALSE,TRUE];
_unit setVariable["pet_owner",FALSE,TRUE];
_unit setVariable["steam64id",(getPlayerUID player),true];
_unit setVariable["realname",profileName,true];
_unit setVariable["fireplaced",false,true];
_unit setVariable["tentplaced",false,true];
_unit enableFatigue false;
life_frozen = false;
life_stealth = false;

_unit addRating 9999999999999999; //Set our rating to a high value, this is for a ARMA engine thing.
player playMoveNow "amovppnemstpsraswrfldnon";

[] call life_fnc_setupActions;
[[_unit,life_sidechat,playerSide],"TON_fnc_managesc",false,false] spawn life_fnc_MP;
player enableFatigue (__GETC__(life_enableFatigue));