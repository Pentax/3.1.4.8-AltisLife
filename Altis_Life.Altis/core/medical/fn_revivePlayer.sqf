/*
	File: fn_revivePlayer.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Starts the revive process on the player.
*/
private["_target","_revivable","_targetName","_ui","_progressBar","_titleText","_cP","_title"];
_target = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
if(isNull _target) exitWith {}; //DAFUQ?@!%$!R?EFFD?TGSF?HBS?DHBFNFD?YHDGN?D?FJH

_revivable = _target getVariable["Revive",FALSE];
if(_revivable) exitWith {};
if(_target getVariable ["Reviving",ObjNull] == player) exitWith {hint localize "STR_Medic_AlreadyReviving";};
if(player distance _target > 5) exitWith {}; //Not close enough.

//Fetch their name so we can shout it.
_targetName = _target getVariable["name","Unknown"];
_title = format[localize "STR_Medic_Progress",_targetName];
life_action_inUse = true; //Lockout the controls.
_time = 0.5; _cpUp = 0.01;_m = 0.05;_m1 = 0.1;
_data = missionNamespace getVariable "Revive_Prof";
switch ( _data select 0 ) do
	{
			case 0: { _time = 0.5; _cpUp = 0.01;_m = 0.05;_m1 = 0.1;};
			case 1: { _time = 0.4; _cpUp = 0.01;_m = 0.08;_m1 = 0.11;};
			case 2: { _time = 0.35; _cpUp = 0.01;_m = 0.1;_m1 = 0.12;};
			case 3: { _time = 0.3; _cpUp = 0.01;_m = 0.15;_m1 = 0.13;};
			case 4: { _time = 0.25; _cpUp = 0.01;_m = 0.17;_m1 = 0.14;};
			case 5: { _time = 0.2; _cpUp = 0.01;_m = 0.20;_m1 = 0.15;};
			case 6: { _time = 0.2; _cpUp = 0.02;_m = 0.22;_m1 = 0.16;};
			case 7: { _time = 0.2; _cpUp = 0.03;_m = 0.25;_m1 = 0.17;};
			case 8: { _time = 0.2; _cpUp = 0.04;_m = 0.28;_m1 = 0.18;};
			case 9: { _time = 0.15; _cpUp = 0.05;_m = 0.30;_m1 = 0.19;};
			case 10: { _time = 0.1; _cpUp = 0.07;_m = 0.33;_m1 = 0.20;};
			case 11: { _time = 0.1; _cpUp = 0.08;_m = 0.36;_m1 = 0.21;};
			case 12: { _time = 0.1; _cpUp = 0.09;_m = 0.4;_m1 = 0.22;};
			case 13: { _time = 0.09; _cpUp = 0.09;_m = 0.44;_m1 = 0.23;};
			case 14: { _time = 0.09; _cpUp = 0.09;_m = 0.46;_m1 = 0.24;};
			case 15: { _time = 0.09; _cpUp = 0.10;_m = 0.48;_m1 = 0.25;};
			case 16: { _time = 0.08; _cpUp = 0.11;_m = 0.50;_m1 = 0.26;};
			case 17: { _time = 0.08; _cpUp = 0.12;_m = 0.55;_m1 = 0.27;};
			case 18: { _time = 0.07; _cpUp = 0.12;_m = 0.60;_m1 = 0.28;};
			case 19: { _time = 0.06; _cpUp = 0.13;_m = 0.63;_m1 = 0.29;};
			case 20: { _time = 0.05; _cpUp = 0.14;_m = 0.65;_m1 = 0.3;};
	};
_target setVariable["Reviving",player,TRUE];
//Setup our progress bar
disableSerialization;
5 cutRsc ["life_progress","PLAIN"];
_ui = uiNamespace getVariable ["life_progress",displayNull];
_progressBar = _ui displayCtrl 38201;
_titleText = _ui displayCtrl 38202;
_titleText ctrlSetText format["%2 (1%1)...","%",_title];
_progressBar progressSetPosition 0.01;
_cP = 0.007;

//Lets reuse the same thing!
while {true} do
{
	if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
		//[[player,"AinvPknlMstpSnonWnonDnon_medic_1"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
		player switchMove "AinvPknlMstpSnonWnonDnon_medic_1";
		player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
	};
	sleep 0.15;
	_cP = _cP + _cpUp;
	_progressBar progressSetPosition _cP;
	_titleText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_title];
	if(_cP >= 1 OR !alive player) exitWith {};
	if(life_istazed) exitWith {}; //Tazed
	if(life_interrupted) exitWith {};
	if((player getVariable["restrained",false])) exitWith {};
	if(player distance _target > 4) exitWith {_badDistance = true;};
	if(_target getVariable["Revive",FALSE]) exitWith {};
	if(_target getVariable["Reviving",ObjNull] != player) exitWith {};
};

//Kill the UI display and check for various states
5 cutText ["","PLAIN"];
player playActionNow "stop";
if(_target getVariable ["Reviving",ObjNull] != player) exitWith {hint localize "STR_Medic_AlreadyReviving";};
_target setVariable["Reviving",NIL,TRUE];
if(!alive player OR life_istazed) exitWith {life_action_inUse = false;};
if(_target getVariable["Revive",FALSE]) exitWith {hint localize "STR_Medic_RevivedRespawned";};
if((player getVariable["restrained",false])) exitWith {life_action_inUse = false;};
if(!isNil "_badDistance") exitWith {titleText[localize "STR_Medic_TooFar","PLAIN"]; life_action_inUse = false;};
if(life_interrupted) exitWith {life_interrupted = false; titleText[localize "STR_NOTF_ActionCancel","PLAIN"]; life_action_inUse = false;};
_price = (call life_revive_fee);
_valBon = round(_price * _m1);//10,000 x 0.1 = 1000
diag_log format["valbon %1",_valBon];
_addExpAmount = round(_valBon * _m);//1000 x 0.05 = 50
[[0,format["You have earned %1 XP and a bonus of $%2 for this revive", _addExpAmount, [_valBon] call life_fnc_numberText]],"life_fnc_broadcast",player,false] spawn life_fnc_MP;
["Revive_Prof",_addExpAmount] call life_fnc_addExp;
life_OnBankCash = life_OnBankCash + _price + _valBon;
life_action_inUse = false;
_target setVariable["Revive",TRUE,TRUE];
[[profileName],"life_fnc_revived",_target,FALSE] spawn life_fnc_MP;
titleText[format[localize "STR_Medic_RevivePayReceive",_targetName,[(call life_revive_fee)] call life_fnc_numberText],"PLAIN"];

sleep 0.6;
player reveal _target;
[_target] call life_fnc_cop_skin;