/*
	File: fn_pickaxeUse.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Main functionality for pickaxe in mining.
*/
closeDialog 0;
private["_mine","_itemWeight","_diff","_itemName","_val"];
//_chance = round(random 1000);

//if (player distance life_lastAreaMined < 3) exitWith {[2,"You have already mined this area.. panhandle somewhere else"] call life_fnc_broadcast;};
switch (true) do
{
	case (player distance (getMarkerPos "lead_1") < 75): {_mine = "copperore"; _val = 2;};
	case (player distance (getMarkerPos "iron_1") < 75): {_mine = "ironore"; _val = 2;};
	case (player distance (getMarkerPos "salt_1") < 75) : {_mine = "salt"; _val = 2;};
	case (player distance (getMarkerPos "sand_1") < 75) : {_mine = "sand"; _val = 2;};
	case (player distance (getMarkerPos "diamond_1") < 75): {_mine = "diamond"; _val = 1;};
	case (player distance (getMarkerPos "oil_1") < 75) : {_mine = "oilu"; _val = 1;};
	case (player distance (getMarkerPos "oil_2") < 75) : {_mine = "oilu"; _val = 1;};
	case (player distance (getMarkerPos "rock_1") < 75): {_mine = "rock"; _val = 2;};
	default {_mine = "";};
};
//Mine check
_addExpAmount = 5;
_time = 3;
_profName = [_mine] call life_fnc_profType;
if( _profName != "" ) then {
		_data = missionNamespace getVariable (_profName);
		_time = ( 3 - (0.25 * (_data select 0)));
		if((_data select 0) > 0) then {
				_val = _val * (_data select 0);
				_addExpAmount = _addExpAmount + round(2.5 * (_data select 0));
		};
		//diag_log format["Amount to add to mining experience %1",_addExpAmount];
};
if(_mine == "") exitWith {hint localize "STR_ISTR_Pick_NotNear"};
if(vehicle player != player) exitWith {hint localize "STR_ISTR_Pick_MineVeh";};
_diff = [_mine,_val,life_carryWeight,life_maxWeight] call life_fnc_calWeightDiff;
if(_diff == 0) exitWith {hint localize "STR_NOTF_InvFull";};
life_action_inUse = true;

 
for "_i" from 0 to 2 do
{
		player playMove "AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon";
		waitUntil{animationState player != "AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon";};
		sleep _time;
};
 
 
if(([true,_mine,_diff] call life_fnc_handleInv)) then {
		_itemName = [([_mine,0] call life_fnc_varHandle)] call life_fnc_varToStr;
		titleText[format[localize "STR_ISTR_Pick_Success",_itemName,_diff],"PLAIN"];
		if( _profName != "" ) then {
				[_profName,_addExpAmount] call life_fnc_addExp;
		};
};
 
life_action_inUse = false;
/*
life_action_inUse = true;
//if(_mine =="diamond") then {_chance = round(random 2400); };
for "_i" from 0 to 1 do
{
	player playMove "AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon";
	waitUntil{animationState player != "AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon";};
	sleep 2.5;
};


		if(([true,_mine,_diff] call life_fnc_handleInv)) then
		{
			_itemName = [([_mine,0] call life_fnc_varHandle)] call life_fnc_varToStr;
			titleText[format[localize "STR_ISTR_Pick_Success",_itemName,_diff],"PLAIN"];
		};

life_action_inUse = false;
*/