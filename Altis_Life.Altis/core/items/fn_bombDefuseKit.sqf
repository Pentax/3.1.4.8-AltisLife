/*
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Defuses demolition charges.
*//*
private["_vault"];
_vault = cursorTarget;
if(isNull _vault) exitWith {};
if(!(_vault getVariable["pchargeplaced",false])) exitWith {hint localize "STR_ISTR_Defuse_Nothing"};
if(!([false,"civdefusekit",1] call life_fnc_handleInv)) exitWith { hint "You have no Defusal Kit"; }; //Error?

life_action_inUse = true;

if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
	//[[player,"AinvPknlMstpSnonWnonDnon_medic_1"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
	player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
	sleep 6;
};

if(!alive player) exitWith {life_action_inUse = false;};

life_action_inUse = false;
hint "Bomb Defused";
_vault setVariable["pchargeplaced",false,true];
_vault setVariable["pchargefound",false,true];


//

	Author: Bryan "Tonic" Boardwine

	Description:
	Defuses blasting charges for the cops?
*/
private["_vault","_ui","_title","_progressBar","_cP","_titleText"];
_vault = prison_safe;
if(isNull _vault) exitWith {systemChat "Vault is Null - Contact an admin";};
//if(typeOf _vault != "Land_CncWall4_F") exitWith {systemChat "not a vault";};
if(!(prison_safe getVariable["pchargeplaced",false])) exitWith {hint localize "STR_ISTR_Defuse_Nothing";};
if(!([false,"civdefusekit",1] call life_fnc_handleInv)) exitWith {hint "You don't have a defuse kit on you";}; //Error?

life_action_inUse = true;
//Setup the progress bar
disableSerialization;
_title = localize "STR_ISTR_Defuse_Process";
5 cutRsc ["life_progress","PLAIN"];
_ui = uiNamespace getVariable "life_progress";
_progressBar = _ui displayCtrl 38201;
_titleText = _ui displayCtrl 38202;
_titleText ctrlSetText format["%2 (1%1)...","%",_title];
_progressBar progressSetPosition 0.01;
_cP = 0.01;

while {true} do
{
	if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
		//[[player,"AinvPknlMstpSnonWnonDnon_medic_1"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
		player switchMove "AinvPknlMstpSnonWnonDnon_medic_1";
		player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
	};
	sleep 0.26;
	if(isNull _ui) then {
		5 cutRsc ["life_progress","PLAIN"];
		_ui = uiNamespace getVariable "life_progress";
		_progressBar = _ui displayCtrl 38201;
		_titleText = _ui displayCtrl 38202;
	};
	_cP = _cP + .025;
	_progressBar progressSetPosition _cP;
	_titleText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_title];
	if(_cP >= 1 OR !alive player) exitWith {};
	if(life_interrupted) exitWith {};
};

//Kill the UI display and check for various states
5 cutText ["","PLAIN"];
player playActionNow "stop";
if(!alive player) exitWith {life_action_inUse = false;};
if(life_interrupted) exitWith {life_interrupted = false; titleText[localize "STR_NOTF_ActionCancel","PLAIN"]; life_action_inUse = false;};
life_action_inUse = false;
_vault setVariable["pchargeplaced",false,true];
[[2,localize "STR_ISTR_Defuse_Success"],"life_fnc_broadcast",west,false] spawn life_fnc_MP;
life_OnBankCash = life_OnBankCash + 5000;
hint parseText format["<t color='#FFFFFF'><t align='center'><t size='.8'>YOU HAVE<br/><t color='#FF0000'><t size ='1.1'<t align='center'>DEFUSED THE BOMB<br/><t color='#00FF22'><t align='center'><t size='1'>You have earned a reward of $5000"];