/*
	Author: Bryan "Tonic" Boardwine

	Description:
	Breaks the lock on a single door (Closet door to the player).
*/
private["_building","_door","_doors","_cpRate","_title","_progressBar","_titleText","_cp","_ui"];
_building = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
if(isNull _building) exitWith {};
if(!(_building isKindOf "House_F")) exitWith {hint "You are not looking at a house door."};
//if((west countSide playableUnits) < 2) exitWith {hint "There needs to be 2 or more cops online to try and crack the safe."};
if((west countSide playableUnits) < cops_online) exitWith {hint format [localize "STR_Civ_NotEnoughCopsHO",cops_online];};
_data = missionNamespace getVariable "Thief_Prof";
_addExpAmount = 100;
_addExpAmount = _addExpAmount + (50 * (_data select 0));
if(isNil "life_boltcutter_uses") then {life_boltcutter_uses = 0;};

if((nearestObject [[12429.118,14102.525,2.687788],"Land_Research_house_V1_F"]) == _building) then {[[[1,2],"!!THE BANK OF ALTIS IS BEING ROBBED!!"],"life_fnc_broadcast",true,false] spawn life_fnc_MP;};

_doors = getNumber(configFile >> "CfgVehicles" >> (typeOf _building) >> "NumberOfDoors");

_door = 0;
//Find the nearest door
for "_i" from 1 to _doors do {
	_selPos = _building selectionPosition format["Door_%1_trigger",_i];
	_worldSpace = _building modelToWorld _selPos;
		if(player distance _worldSpace < 5) exitWith {_door = _i;};
};
if(_door == 0) exitWith {hint "You are not near a door!"}; //Not near a door to be broken into.
if((_building getVariable[format["bis_disabled_Door_%1",_door],0]) == 0) exitWith {hint "This door is already unlocked!"};
life_action_inUse = true;
_cpRate = 0.005;
switch ( _data select 0 ) do {
			case 0: {_cpRate = 0.005;};
			case 1: {_cpRate = 0.007;};
			case 2: {_cpRate = 0.009;};
			case 3: {_cpRate = 0.01;};
			case 4: {_cpRate = 0.01;};
			case 5: {_cpRate = 0.01;};
			case 6: {_cpRate = 0.02;};
			case 7: {_cpRate = 0.03;};
			case 8: {_cpRate = 0.04;};
			case 9: {_cpRate = 0.05;};
			case 10: {_cpRate = 0.07;};
			case 11: {_cpRate = 0.08;};
			case 12: {_cpRate = 0.09;};
			case 13: {_cpRate = 0.09;};
			case 14: {_cpRate = 0.09;};
			case 15: {_cpRate = 0.10;};
			case 16: {_cpRate = 0.11;};
			case 17: {_cpRate = 0.12;};
			case 18: {_cpRate = 0.12;};
			case 19: {_cpRate = 0.13;};
			case 20: {_cpRate = 0.14;};
};
//Setup the progress bar
disableSerialization;
_title = "Cutting lock on door";
5 cutRsc ["life_progress","PLAIN"];
_ui = uiNamespace getVariable "life_progress";
_progressBar = _ui displayCtrl 38201;
_titleText = _ui displayCtrl 38202;
_titleText ctrlSetText format["%2 (1%1)...","%",_title];
_progressBar progressSetPosition 0.01;
_cP = 0.009;

while {true} do
{
	if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
		//[[player,"AinvPknlMstpSnonWnonDnon_medic_1"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
		player switchMove "AinvPknlMstpSnonWnonDnon_medic_1";
		player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
	};
	sleep 0.26;
	if(isNull _ui) then {
		5 cutRsc ["life_progress","PLAIN"];
		_ui = uiNamespace getVariable "life_progress";
		_progressBar = _ui displayCtrl 38201;
		_titleText = _ui displayCtrl 38202;
	};
	_cP = _cP + _cpRate;
	_progressBar progressSetPosition _cP;
	_titleText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_title];
	if(_cP >= 1 OR !alive player) exitWith {};
	if(life_istazed) exitWith {}; //Tazed
	if(life_interrupted) exitWith {};
};

//Kill the UI display and check for various states
5 cutText ["","PLAIN"];
player playActionNow "stop";
if(!alive player OR life_istazed) exitWith {life_action_inUse = false;};
if((player getVariable["restrained",false])) exitWith {life_action_inUse = false;};
if(life_interrupted) exitWith {life_interrupted = false; titleText["Action cancelled","PLAIN"]; life_action_inUse = false;};
life_boltcutter_uses = life_boltcutter_uses + 1;
life_action_inUse = false;
if(life_boltcutter_uses >= 5) then {
	[false,"boltcutter",1] call life_fnc_handleInv;
	life_boltcutter_uses = 0;
};

_building setVariable[format["bis_disabled_Door_%1",_door],0,true];
if((_building getVariable["locked",false])) then {
	_building setVariable["locked",false,true];
};
[[getPlayerUID player,player getVariable["realname",name player],"1014"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
["Thief_Prof",_AddExpAmount] call life_fnc_addExp;
[[0,format["You have earned %1 XP for cracking the vault door", _addExpAmount]],"life_fnc_broadcast",player,false] spawn life_fnc_MP;