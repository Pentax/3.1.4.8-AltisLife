if(vehicle player != player) exitWith {hint"You cannot place a campfire from inside vehicles";};
/*	File: fn_campfire.sqf	Author: Bryan "Tonic" Boardwine*/
private["_position","_campfire"];
_campfire = "Land_Campfire_F" createVehicle [0,0,0];
_campfire attachTo[player,[0,4,0.5]];
_campfire setDir 90;
_campfire setVariable["item","campfire",true];
player setVariable["fireplaced",true,true];
life_action_campfireDeploy = player addAction["<t color='#FF7700'>Build Campfire</t>",{if(!isNull life_campfire) then {detach life_campfire; life_campfire = ObjNull;}; player removeAction life_action_campfireDeploy; life_action_campfireDeploy = nil;},"",999,false,false,"",'!isNull life_campfire'];
life_campfire = _campfire;
waitUntil {isNull life_campfire};
if(!isNil "life_action_campfireDeploy") then {player removeAction life_action_campfireDeploy;};
if(isNull _campfire) exitWith {life_campfire = ObjNull;};
_campfire setPos [(getPos _campfire select 0),(getPos _campfire select 1),0];
//_campfire setDamage 1;
life_action_campfirePickup = player addAction["<t color='#FF7700'>Pack up Campfire Kit</t>",life_fnc_packupCampfire,"",0,false,false,"",'_fire = nearestObjects[getPos player,["Land_Campfire_F"],8] select 0; !isNil "_fire" && !isNil {(_fire getVariable "item")}'];
life_action_cookMeat = player addAction["<t color='#FF7700'>Cook Raw Meat</t>",life_fnc_cookRawMeat,"",-1,false,false,"",'_fire = nearestObjects[getPos player,["Land_Campfire_F"],8] select 0; !isNil "_fire" && !isNil {(_fire getVariable "item")} && (life_inv_hen > 0 || life_inv_dog > 0 || life_inv_adog > 0 || life_inv_sheep > 0 || life_inv_goat > 0 || life_inv_rabbit > 0 || life_inv_rooster > 0)'];
//<t color='#FF7700'>Pick Up Items</t>










