/*
	Author: Bryan "Tonic" Boardwine
	life_fnc_safeGold
	Description:
	Blasting charge is used for the federal reserve vault and nothing  more.. Yet.
*/
if((west countSide playableUnits) < cops_onlineLess) exitWith {hint format [localize "STR_Civ_NotEnoughCopsHO",cops_onlineLess];};
private["_soundPath","_soundToPlay","_vault","_handle","_bomb","_marker"];
_soundPath = [(str missionConfigFile), 0, -15] call BIS_fnc_trimString;
_soundToPlay = _soundPath + "sounds\ticktock.ogg";
//_vault = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
_vault = prison_safe;
if(isNull _vault) exitWith {}; //Bad object
if(typeOf _vault != "Land_CncWall4_F") exitWith {hint localize "STR_ISTR_Blast_VaultOnlyPrison"};
if(prison_safe getVariable["pchargeplaced",false]) exitWith {hint localize "STR_ISTR_Blast_AlreadyPlacedPrison"};
if(prison_safe getVariable["prison_open",false]) exitWith {hint localize "STR_ISTR_Blast_AlreadyOpenPrison"};
if(!([false,"demolitioncharge",1] call life_fnc_handleInv)) exitWith {}; //Error?

prison_safe setVariable["pchargeplaced",true,true];
_handle = [] spawn life_fnc_prisonWallTimer;
[[],"life_fnc_prisonWallTimer",west,false] spawn life_fnc_MP;
playSound3D [_soundToPlay, _vault];
waitUntil {scriptDone _handle};
sleep 0.9;
if(!(prison_safe getVariable["pchargeplaced",false])) exitWith {hint localize "STR_ISTR_Blast_Disarmed"};

_bomb = "Bo_GBU12_LGB_MI10" createVehicle [getPosATL prison_safe select 0, getPosATL prison_safe select 1, (getPosATL prison_safe select 2)+0.5];
prison_safe setVariable["pchargeplaced",false,true];
prison_safe setVariable["prison_open",true,true];
prison_safe attachTo [prison_holder,[3,4,-0.4]];
prison_safe1 attachTo [prison_holder,[3,4,-0.4]];
prison_safe2 attachTo[prison_wallholder,[3.9,0.85,1.75]]; 
_marker = createMarker ["MarkerJailbreak", prison_safe2];
_marker setMarkerColor "ColorRed";
_marker setMarkerType "Empty";
_marker setMarkerShape "ELLIPSE";
_marker setMarkerSize [100,100];
_markerText = createMarker ["MarkerTextJailbreak", prison_safe2];
_markerText setMarkerColor "ColorBlack";
_markerText setMarkerText "!!JAILBREAK!!";
_markerText setMarkerType "KIA";
[[getPlayerUID player,player getVariable["realname",name player],"8"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
systemChat "You have been added to the wanted list for terrorism...";
[[4,"JAILBREAK - Rebels have seized the Kavala Prison in an escape attempt! Authorities are claiming it's another attack from one of the self-claimed Rebel groups"],"life_fnc_broadcast",true,false] spawn life_fnc_MP;
hint localize "STR_ISTR_Blast_OpenedPrison";

/*
prison_safe attachTo[prison_wallholder,[-0.1,3,0.8]];
_bomb = "Bo_GBU12_LGB_MI10" createVehicle [getPosATL prison_safe select 0, getPosATL prison_safe select 1, (getPosATL prison_safe select 2)+0.5];  prison_safe attachTo [prison_holder,[3,4,-0.4]];

Land_New_WiredFence_5m_F
Land_Mil_WiredFence_F
prison_safe attachTo[prison_wallholder,[3.7,0.85,1.4]]; 
prison_safe1 attachTo [prison_wallholder,[3.7,1.8,0.57]];

*/