#define SAFETY_ZONES    [["cop_safe_ant", 135],["cop_safe_pyr", 100],["civ_safe_ant", 175],["don_safe_comp", 80],["civ_safe_pyr", 225],["town_safe_agio", 100],["reb_safe_west", 100],["reb_safe_ghost", 150],["civ_safe_airport",100],["civ_safe_donorair",100],["cop_safe_airport",50],["civ_safe_kavala",100],["civ_safe_charkia",100],["cop_safe_kavala",150],["reb_safe_kavala",150],["event_safe_soccer",75],["lockpick_sz_1",100],["med_safe_pyrgos",50],["med_safe_pyrgos_1",50],["med_safe_pyrgos_2",50]]
if ({player distance getMarkerPos (_x select 0) < _x select 1} count SAFETY_ZONES > 0) exitWith {hint"You cannot place a tent inside of a safe zone";};
if(vehicle player != player) exitWith {hint"You cannot pitch a tent from inside vehicles";};
/*	File: fn_tent.sqf	Author: Bryan "Tonic" Boardwine*/
private["_position","_tent"];
_tent = "Land_TentA_F" createVehicle [0,0,0];
_tent attachTo[player,[0,4,0.5]];
_tent setDir 90;
//
_tent setVariable ["Trunk", [[],0], true];_tent setVariable["item","tenta",true];
//_tent setVariable["trunk_in_use",false,true];
[[_tent],"life_fnc_simDisable",nil,true] spawn life_fnc_MP;
//[[_tent],"life_fnc_simDisable",false,false] spawn BIS_fnc_MP;
//[[_tent,"trunk_in_use",false,true],"TON_fnc_setObjVar",false,false] spawn life_fnc_MP;
//[[_tent,"vehicle_info_owners",[[getPlayerUID player,profileName]],true],"TON_fnc_setObjVar",false,false] spawn life_fnc_MP;
player setVariable["tentplaced",true,true];
[false,"tenta",1] call life_fnc_handleInv;
life_action_tentDeploy = player addAction["<t color='#FF7700'>Pitch Tent</t>",{if(!isNull life_tent) then {detach life_tent; life_tent = ObjNull;}; player removeAction life_action_tentDeploy; life_action_tentDeploy = nil;},"",999,false,false,"",'!isNull life_tent'];

life_tent = _tent;
waitUntil {isNull life_tent};
if(!isNil "life_action_tentDeploy") then {player removeAction life_action_tentDeploy;};
if(isNull _tent) exitWith {life_tent = ObjNull;};
_tent setPos [(getPos _tent select 0),(getPos _tent select 1),0];
//_tent setDamage 1;
life_action_tentPickup = player addAction["<t color='#FF7700'>Pack Up Tent</t>",life_fnc_packupTent,"",0,false,false,"",
' _tents = nearestObjects[getPos player,["Land_TentA_F"],8] select 0; !isNil "_tents" && !isNil {(_tents getVariable "item")}'];