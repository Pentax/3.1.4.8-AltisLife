#define SAFETY_ZONES    [["cop_safe_ant", 135],["cop_safe_pyr", 100],["civ_safe_ant", 175],["don_safe_comp", 80],["civ_safe_pyr", 225],["town_safe_agio", 100],["reb_safe_west", 100],["reb_safe_ghost", 150],["civ_safe_airport",100],["civ_safe_donorair",100],["cop_safe_airport",50],["civ_safe_kavala",100],["civ_safe_charkia",100],["cop_safe_kavala",150],["reb_safe_kavala",150],["event_safe_soccer",75],["lockpick_sz_1",100],["med_safe_pyrgos",50],["med_safe_pyrgos_1",50],["med_safe_pyrgos_2",50]]
 /*
	File: fn_lockpick.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Main functionality for lock-picking.
*/
private["_curTarget","_distance","_isVehicle","_title","_progressBar","_cP","_titleText","_dice","_badDistance"];
_curTarget = cursorTarget;
 if ({_curTarget distance getMarkerPos (_x select 0) < _x select 1} count SAFETY_ZONES > 0) exitWith {hint"No lockpicking in safe zones";};

life_interrupted = false;
if(life_action_inUse) exitWith {};
if(isNull _curTarget) exitWith {}; //Bad type
_distance = ((boundingBox _curTarget select 1) select 0) + 2;
if(player distance _curTarget > _distance) exitWith {}; //Too far
_isVehicle = if((_curTarget isKindOf "LandVehicle") OR (_curTarget isKindOf "Ship") OR (_curTarget isKindOf "Air")) then {true} else {false};
if(_isVehicle && _curTarget in life_vehicles) exitWith {hint localize "STR_ISTR_Lock_AlreadyHave";};

//More error checks
if(!_isVehicle && !isPlayer _curTarget) exitWith {};
if(!_isVehicle && !(_curTarget getVariable["restrained",false])) exitWith {};
_data = missionNamespace getVariable ("Thief_Prof");
_addExpAmount = 25;
_addExpAmount = _addExpAmount + (10 * (_data select 0));
_cpRate = 0.005;
_dice = random(100);
_title = format[localize "STR_ISTR_Lock_Process",if(!_isVehicle) then {"Handcuffs"} else {getText(configFile >> "CfgVehicles" >> (typeOf _curTarget) >> "displayName")}];
life_action_inUse = true; //Lock out other actions
switch ( _data select 0 ) do {
			case 0: {_cpRate = 0.005;_dice = random(100);};
			case 1: {_cpRate = 0.007;_dice = random(95);};
			case 2: {_cpRate = 0.009;_dice = random(90);};
			case 3: {_cpRate = 0.01;_dice = random(85);};
			case 4: {_cpRate = 0.01;_dice = random(80);};
			case 5: {_cpRate = 0.01;_dice = random(79);};
			case 6: {_cpRate = 0.02;_dice = random(78);};
			case 7: {_cpRate = 0.03;_dice = random(75);};
			case 8: {_cpRate = 0.04;_dice = random(70);};
			case 9: {_cpRate = 0.05;_dice = random(69);};
			case 10: {_cpRate = 0.07;_dice = random(67);};
			case 11: {_cpRate = 0.08;_dice = random(65);};
			case 12: {_cpRate = 0.09;_dice = random(63);};
			case 13: {_cpRate = 0.09;_dice = random(62);};
			case 14: {_cpRate = 0.09;_dice = random(61);};
			case 15: {_cpRate = 0.10;_dice = random(60);};
			case 16: {_cpRate = 0.11;_dice = random(60);};
			case 17: {_cpRate = 0.12;_dice = random(60);};
			case 18: {_cpRate = 0.12;_dice = random(55);};
			case 19: {_cpRate = 0.13;_dice = random(54);};
			case 20: {_cpRate = 0.14;_dice = random(50);};
};
//Setup the progress bar
disableSerialization;
5 cutRsc ["life_progress","PLAIN"];
_ui = uiNamespace getVariable "life_progress";
_progressBar = _ui displayCtrl 38201;
_titleText = _ui displayCtrl 38202;
_titleText ctrlSetText format["%2 (1%1)...","%",_title];
_progressBar progressSetPosition 0.01;
_cP = 0.01;

while {true} do
{
	if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
		//[[player,"AinvPknlMstpSnonWnonDnon_medic_1"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
		player switchMove "AinvPknlMstpSnonWnonDnon_medic_1";
		player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
	};
	sleep 0.26;
	if(isNull _ui) then {
		5 cutRsc ["life_progress","PLAIN"];
		_ui = uiNamespace getVariable "life_progress";
		_progressBar = _ui displayCtrl 38201;
 	_titleText = _ui displayCtrl 38202;
	};
	_cP = _cP + _cpRate;
	_progressBar progressSetPosition _cP;
	_titleText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_title];
	if(_cP >= 1 OR !alive player) exitWith {};
	if(life_istazed) exitWith {}; //Tazed
	if(life_interrupted) exitWith {};
	if((player getVariable["restrained",false])) exitWith {};
	if(player distance _curTarget > _distance) exitWith {_badDistance = true;};
};

//Kill the UI display and check for various states
5 cutText ["","PLAIN"];
player playActionNow "stop";
if(!alive player OR life_istazed) exitWith {life_action_inUse = false;};
if((player getVariable["restrained",false])) exitWith {life_action_inUse = false;};
if(!isNil "_badDistance") exitWith {titleText[localize "STR_ISTR_Lock_TooFar","PLAIN"]; life_action_inUse = false;};
if(life_interrupted) exitWith {life_interrupted = false; titleText[localize "STR_NOTF_ActionCancel","PLAIN"]; life_action_inUse = false;};
if(!([false,"lockpick",1] call life_fnc_handleInv)) exitWith {life_action_inUse = false;};
life_action_inUse = false;

if(!_isVehicle) then {
	_curTarget setVariable["restrained",false,true];
	_curTarget setVariable["Escorting",false,true];
	_curTarget setVariable["transporting",false,true];
	["Thief_Prof",_AddExpAmount] call life_fnc_addExp;
	[[0,format["You have earned %1 XP for lockpicking handcuffs", _addExpAmount]],"life_fnc_broadcast",player,false] spawn life_fnc_MP;
} else {
	
	if(_dice < 30) then {
		titleText[localize "STR_ISTR_Lock_Success","PLAIN"];
		life_vehicles pushBack _curTarget;
		[[getPlayerUID player,profileName,"487"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
		["Thief_Prof",_AddExpAmount] call life_fnc_addExp;
		[[0,format["You have earned %1 XP for lockpicking a vehicle", _addExpAmount]],"life_fnc_broadcast",player,false] spawn life_fnc_MP;	
	} else {
		[[getPlayerUID player,profileName,"215"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
		[[0,"STR_ISTR_Lock_FailedNOTF",true,[profileName]],"life_fnc_broadcast",west,false] spawn life_fnc_MP;
		titleText["The lockpick broke.","PLAIN"];
	};
};