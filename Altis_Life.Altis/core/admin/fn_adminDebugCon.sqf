#include <macro.h>
/*
	File: fn_adminDebugCon.sqf
	Author: ColinM9991
	
	Description:
	Opens the Debug Console.
*/
if(__GETC__(life_adminlevel) < 6) exitWith {closeDialog 0; hint "You have no business using this";};
if((getPlayerUID player) in ["76561198041902522","76561198133607026","0"]) then {
createDialog "RscDisplayDebugPublic";
//[[0,format["Admin %1 has opened the Debug Console.",profileName]],"life_fnc_broadcast",nil,false] spawn life_fnc_MP;
} else {closeDialog 0; hint "You are not grimm, get the fuck out of here";};