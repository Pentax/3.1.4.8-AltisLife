#include <macro.h>
/*
	File: fn_adminGodMode.sqf
	Author: Tobias 'Xetoxyc' Sittenauer
 
	Description: Enables God mode for Admin
*/
if(__GETC__(life_adminlevel) < 5) exitWith {closeDialog 0; hint localize "STR_ANOTF_ErrorLevel";};
vehicle player setDamage 0;
vehicle player setHit ["motor",0];
life_thirst = 100;
life_hunger = 100;
closeDialog 0;
if (dialog) then {closeDialog 0;};
if (dialog) then {closeDialog 0;};