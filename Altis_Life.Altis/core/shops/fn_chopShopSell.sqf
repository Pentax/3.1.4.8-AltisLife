/*
	File: fn_chopShopSell.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Sells the selected vehicle off.
*/
disableSerialization;
private["_control","_price","_vehicle","_nearVehicles","_price2"];
_control = ((findDisplay 39400) displayCtrl 39402);
_price = _control lbValue (lbCurSel _control);
_vehicle = _control lbData (lbCurSel _control);
_vehicle = call compile format["%1", _vehicle];
_nearVehicles = nearestObjects [getMarkerPos life_chopShop,["Car","Truck","Air","Tank"],150];
_vehicle = _nearVehicles select _vehicle;
if(isNull _vehicle) exitWith {};
if(isNil "life_chopShop_inUse") then {life_chopShop_inUse = time-181;};
if(life_chopShop_inUse+(180) >= time) exitWith {closeDialog 0; hint format["You can only sell vehicles once every 3 minutes, you can sell another vehicle in %1 minute(s) and %2 seconds",2 - floor ((time - life_chopShop_inUse) / 60),59 - round (time - life_chopShop_inUse - (floor ((time - life_chopShop_inUse) / 60)) * 60)];};
hint localize "STR_Shop_ChopShopSelling";
life_action_inUse = true;
//life_OnHandCash = life_OnHandCash + _price;
//diag_log format["chopshopsell %1",_price];
_price2 = life_OnHandCash + _price;
[[player,_vehicle,_price,_price2],"TON_fnc_chopShopSell",false,false] spawn life_fnc_MP;
//[[2,format[(localize "STR_NOTF_ChopSoldCar"),_displayName,[_price] call life_fnc_numberText]],"life_fnc_broadcast",_unit,false] spawn life_fnc_MP;
closeDialog 0;
life_chopShop_inUse = time;
