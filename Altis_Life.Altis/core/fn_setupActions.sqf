/*
	File: fn_setupActions.sqf
	
	Description:
	Master addAction file handler for all client-based actions.
*/
switch (playerSide) do
{
	//COP FUNCTIONS
	case west:
	{
		
			/*//Seize weapons
			life_actions = life_actions + [player addAction["<t color='#FFFF00'>Seize weapons</t>+",life_fnc_seizePlayerWeapon,cursorTarget,0,false,false,"",'!isNull cursorTarget && (player distance cursorTarget) < 6 && speed cursorTarget < 2 && cursorTarget isKindOf "Man" && (isPlayer cursorTarget) && (side cursorTarget == east) && (cursorTarget getVariable "restrained")']];
			*/
		//seize Objects
		life_actions = life_actions + [player addAction["<t color='#FFFF00'>Seize objects</t>",life_fnc_seizeObjects,cursorTarget,0,false,false,"",'count(nearestObjects [player,["weaponholder"],3])>0']];
		
		//lights for cars
		life_actions = life_actions + [player addAction["<t color='#FFFF00'>LIGHTS</t>",life_fnc_policeLights,"",6,false,false,"",'vehicle player != player']];
		//lights for cars
		life_actions = life_actions + [player addAction["<t color='#FFFF00'>YELP</t>",life_fnc_policeSiren2,"",6,false,false,"",'vehicle player != player']];
		//lights for cars
		life_actions = life_actions + [player addAction["<t color='#FFFF00'>SIRENS</t>",life_fnc_policeSiren,"",6,false,false,"",'vehicle player != player']];
		//radar for cars
		life_actions = life_actions + [player addAction["<t color='#FFFF00'>RADAR</t>",life_fnc_radar,"",6,false,false,"",'vehicle player != player']];
		
		// Organ Transplant
		life_actions = life_actions + [player addAction["<t color='#FFCC00'>Organ Transplant</t>",life_fnc_organTransplantKit,"",0,false,false,"",'!isNull cursorTarget && cursorTarget isKindOf "Man" && (isPlayer cursorTarget) && alive cursorTarget && cursorTarget distance player < 3.5 && cursorTarget getVariable "missingOrgan" && !(player getVariable "restrained") ']];
		// bloodbag
		life_actions = life_actions + [player addAction["<t color='#FF0000'>Give Bloodbag</t>",life_fnc_bloodbag,"",0,false,false,"",'!isNull cursorTarget && cursorTarget isKindOf "Man" && (isPlayer cursorTarget) && alive cursorTarget && (damage cursorTarget) > 0.05 && cursorTarget distance player < 3.5 && !(player getVariable "restrained") ']];
		
		// self bloodbag
		life_actions = life_actions + [player addAction["<t color='#FF0000'>Self Bloodbag</t>",life_fnc_selfbloodbag,"",0,false,false,"",' player isKindOf "Man" && alive player && (damage player) > 0.05 && !(player getVariable "restrained") ']];
		
		// self transplant
		life_actions = life_actions + [player addAction["<t color='#FFCC00'>Give Self Transplant</t>",life_fnc_replaceOrgan,"",0,false,false,"",' player isKindOf "Man" && alive player && player getVariable "missingOrgan" && !(player getVariable "restrained") ']];
		
		//Defuse Bombs
		life_actions = life_actions + [player addAction["<t color='#00FF00'>Defuse Explosives</t>", life_fnc_bombDefuseKit,"",6,false,false,"",'player distance cursorTarget < 3 && (cursorTarget getVariable["pchargeplaced",FALSE])']];	
		//pickup shit since windows just doesnt alwasy work
		life_actions = life_actions + [player addAction["<t color='#FF7700'>Pick Up Items</t>",{createDialog "life_pickup_items"},"",0,false,false,"",' !isNull cursorTarget && count (cursorTarget getVariable ["item",[]]) > 0 && player distance cursorTarget < 6 ']];
		
	};
	case east:
	{
		
		//Drop fishing net
		life_actions = [player addAction["Drop Fishing Net",life_fnc_dropFishingNet,"",0,false,false,"",'
		(surfaceisWater (getPos vehicle player)) && (vehicle player isKindOf "Ship") && life_carryWeight < life_maxWeight && speed (vehicle player) < 2 && speed (vehicle player) > -1 && !life_net_dropped ']];
		//Rob person
		life_actions = life_actions + [player addAction["<t color='#FFFF00'>Rob the unconscious</t>",life_fnc_robAction,"",0,false,false,"",'
		!isNull cursorTarget && player distance cursorTarget < 3.5 && isPlayer cursorTarget && animationState cursorTarget == "Incapacitated" && !(cursorTarget getVariable["robbed",FALSE]) ']];
		//Rob person
		life_actions = life_actions + [player addAction["<t color='#FFFF00'>Rob the helpless</t>",life_fnc_robAction,"",0,false,false,"",'
		!isNull cursorTarget && player distance cursorTarget < 3.5 && isPlayer cursorTarget && animationState cursorTarget == "amovpercmstpsnonwnondnon_amovpercmstpssurwnondnon" && !(cursorTarget getVariable["robbed",FALSE]) ']];
		
		// Harvest Organs
		life_actions = life_actions + [player addAction["<t color='#FFFF00'>Steal Organs</t>",life_fnc_scalpel,"",0,false,false,"",'!isNull cursorTarget && cursorTarget isKindOf "Man" && (isPlayer cursorTarget) && alive cursorTarget && cursorTarget distance player < 3.5 && cursorTarget getVariable "restrained" && !(player getVariable "restrained") ']];
		// Organ Transplant
		life_actions = life_actions + [player addAction["<t color='#FFCC00'>Organ Transplant</t>",life_fnc_organTransplantKit,"",0,false,false,"",'!isNull cursorTarget && cursorTarget isKindOf "Man" && (isPlayer cursorTarget) && alive cursorTarget && cursorTarget distance player < 3.5 && cursorTarget getVariable "missingOrgan" && !(player getVariable "restrained") ']];
		// bloodbag
		life_actions = life_actions + [player addAction["<t color='#FF0000'>Give Bloodbag</t>",life_fnc_bloodbag,"",0,false,false,"",'!isNull cursorTarget && cursorTarget isKindOf "Man" && (isPlayer cursorTarget) && alive cursorTarget && (damage cursorTarget) > 0.05 && cursorTarget distance player < 3.5 && !(player getVariable "restrained") ']];
		
		// self bloodbag
		life_actions = life_actions + [player addAction["<t color='#FF0000'>Self Bloodbag</t>",life_fnc_selfbloodbag,"",0,false,false,"",' player isKindOf "Man" && alive player && (damage player) > 0.05 && !(player getVariable "restrained") ']];
		
		// self transplant
		life_actions = life_actions + [player addAction["<t color='#FFCC00'>Give Self Transplant</t>",life_fnc_replaceOrgan,"",0,false,false,"",' player isKindOf "Man" && alive player && player getVariable "missingOrgan" && !(player getVariable "restrained") ']];
		//Defuse Bombs
		life_actions = life_actions + [player addAction["<t color='#00FF00'>Defuse Explosives</t>", life_fnc_bombDefuseKit,"",99,false,false,"",' player distance cursorTarget < 5 && (cursorTarget getVariable["dchargefound",FALSE]) && ((cursorTarget isKindOf "Car") || (cursorTarget isKindOf "Building")) ']];
		//hunting tracking
		life_actions = life_actions + [player addAction["<t color='#FFCC00'>Search Area</t>",life_fnc_trackAnimal,"",-1,false,false,"",'player distance (getMarkerPos "hunting_zone") < 399 && alive player']];
		//pickup shit since windows just doesnt alwasy work
		life_actions = life_actions + [player addAction["<t color='#FF7700'>Pick Up Items</t>",{createDialog "life_pickup_items"},"",3,false,false,"",' !isNull cursorTarget && count (cursorTarget getVariable ["item",[]]) > 0 && player distance cursorTarget < 6 ']];
		
		
	};
	case independent:
	{
		// Organ Transplant
		life_actions = life_actions + [player addAction["<t color='#FFCC00'>Organ Transplant</t>",life_fnc_organTransplantKit,"",0,false,false,"",'!isNull cursorTarget && cursorTarget isKindOf "Man" && (isPlayer cursorTarget) && alive cursorTarget && cursorTarget distance player < 3.5 && cursorTarget getVariable "missingOrgan" && !(player getVariable "restrained") ']];
		// bloodbag
		life_actions = life_actions + [player addAction["<t color='#FF0000'>Give Bloodbag</t>",life_fnc_bloodbag,"",2,false,false,"",'!isNull cursorTarget && cursorTarget isKindOf "Man" && (isPlayer cursorTarget) && alive cursorTarget && (damage cursorTarget) > 0.05 && cursorTarget distance player < 3.5 && !(player getVariable "restrained") ']];
		
		// self bloodbag
		life_actions = life_actions + [player addAction["<t color='#FF0000'>Self Bloodbag</t>",life_fnc_selfbloodbag,"",0,false,false,"",' player isKindOf "Man" && alive player && (damage player) > 0.05 && !(player getVariable "restrained") ']];
		
		// self transplant
		life_actions = life_actions + [player addAction["<t color='#FFCC00'>Give Self Transplant</t>",life_fnc_replaceOrgan,"",0,false,false,"",' player isKindOf "Man" && alive player && player getVariable "missingOrgan" && !(player getVariable "restrained") ']];
		//pickup shit since windows just doesnt alwasy work
		life_actions = life_actions + [player addAction["<t color='#FF7700'>Pick Up Items</t>",{createDialog "life_pickup_items"},"",3,false,false,"",' !isNull cursorTarget && count (cursorTarget getVariable ["item",[]]) > 0 && player distance cursorTarget < 6 ']];
		
		//earplugs
		
		//lights for cars
		life_actions = life_actions + [player addAction["<t color='#FFFF00'>LIGHTS</t>",life_fnc_policeLights,"",6,false,false,"",'vehicle player != player']];
		//lights for cars
		life_actions = life_actions + [player addAction["<t color='#FFFF00'>YELP</t>",life_fnc_policeSiren2,"",6,false,false,"",'vehicle player != player']];
		//lights for cars
		life_actions = life_actions + [player addAction["<t color='#FFFF00'>SIRENS</t>",life_fnc_policeSiren,"",6,false,false,"",'vehicle player != player']];
	};
};
