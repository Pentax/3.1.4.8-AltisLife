/*
	Author: Bryan "Tonic" Boardwine

	Description:
	Turns on and displays a security cam like feed via PiP to the laptop display.
*/
_laptop = _this select 0;
_mode = _this select 3;

if(!isPiPEnabled) exitWith {hint localize "STR_Cop_EnablePiP";};
if(isNil "life_fed_scam") then {
	life_fed_scam = "camera" camCreate [0,0,0];
	life_fed_scam camSetFov 0.5;
	life_fed_scam camCommit 0;
	"rendertarget0" setPiPEffect [0];
	life_fed_scam cameraEffect ["INTERNAL", "BACK", "rendertarget0"];
	_laptop setObjectTexture [0,"#(argb,256,256,1)r2t(rendertarget0,1.0)"];
};

switch (_mode) do {
	case "side": {
		life_fed_scam camSetPos [12347.635,14022.28,40];
		life_fed_scam camSetTarget [12428.758,14102.886,0];
		life_fed_scam camCommit 0;
	};

	case "vault": {
		life_fed_scam camSetPos [12428.533,14097.09,15];
		life_fed_scam camSetTarget [12431.715,14101.085,12];
		life_fed_scam camCommit 0;
	};

	case "front": {
		life_fed_scam camSetPos [12448.812,14028.59,35];
		life_fed_scam camSetTarget [12428.758,14102.886,0];
		life_fed_scam camCommit 0;
	};

	case "back": {
		life_fed_scam camSetPos [12346.13,14160.68,40];
		life_fed_scam camSetTarget [12428.758,14102.886,0];
		life_fed_scam camCommit 0;
	};

	case "jail_1": {
		life_fed_scam camSetPos [2709.6414,11523.658,40];
		life_fed_scam camSetTarget [2795.6707,11571.254,0.1];
		life_fed_scam camCommit 0;
	};

	case "jail_2": {
		life_fed_scam camSetPos [2963.6931,11714.666,40];
		life_fed_scam camSetTarget [2795.6707,11571.254,0.1];
		life_fed_scam camCommit 0;
	};

	case "off" :{
		life_fed_scam cameraEffect ["terminate", "back"];
		camDestroy life_fed_scam;
		_laptop setObjectTexture [0,""];
		life_fed_scam = nil;
	};
};