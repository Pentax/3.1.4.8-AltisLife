/*
	File: fn_questionDealer.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Questions the drug dealer and sets the sellers wanted.
*/
private["_sellers","_names"];
_sellers = (_this select 0) getVariable["sellers",[]];
if(count _sellers == 0) exitWith {localize "STR_Cop_DealerQuestion"};
life_action_inUse = true;
_names = "";
{
	if(_x select 2 > 150000) then
	{
		_val = round((_x select 2) / 16);
	};
	[[_x select 0,_x select 1,"483",_val],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
	_names = _names + format["%1<br/>",_x select 1];
} foreach _sellers;

hint parseText format[localize "STR_Cop_DealerMSG",_names];
(_this select 0) setVariable["sellers",[],true];
life_action_inUse = false;
_rN = round(random 50);
if(_rN > 49) then {
_bonusCash = 5000 + round(random 5000);
titleText [format["You were given a $%1 bonus for questioning suspected dealers in your area",[_bonusCash] call life_fnc_numberText],"PLAIN"];
life_onBankCash = life_OnBankCash + _bonusCash;
} else {
		titleText["Good work questioning suspects, keep it up","PLAIN"];
};