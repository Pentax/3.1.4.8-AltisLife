/*
	File: fn_copLoadout.sqf
	Author: Bryan "Tonic" Boardwine
	Edited: Itsyuka
	
	Description:
	Loads the cops out with the default gear.
*/
private["_handle"];
_handle = [] spawn life_fnc_stripDownPlayer;
waitUntil {scriptDone _handle};

//Load player with default cop gear.
player addUniform "U_Rangemaster";
player addHeadgear "H_Cap_police";
player addVest "V_TacVest_blk_POLICE";
player addMagazine "16Rnd_9x21_Mag";
player addMagazine "16Rnd_9x21_Mag";
player addMagazine "16Rnd_9x21_Mag";
player addWeapon "hgun_Rook40_snds_F";
player selectWeapon "hgun_Rook40_snds_F";
player addItem "ItemRadio";
player assignItem "ItemRadio";
player addItem "ItemGPS";
player assignItem "ItemGPS";
player addItem "ItemMap";
player assignItem "ItemMap";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addBackPack "B_Carryall_khk";
mybackpack = unitBackpack player;
mybackpack addItemCargoGlobal ["NVGoggles", 1];
//mybackpack addItemCargoGlobal ["FirstAidKit", 2];
mybackpack addItemCargoGlobal ["Toolkit", 1];
reload player;
//[player] execVM "scripts\fn_cop_skin.sqf";
[player] call life_fnc_cop_skin;
[] call life_fnc_saveGear;