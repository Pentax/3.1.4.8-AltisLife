private["_driver","_speed","_ticket","_limit"];
_driver = _this select 0;
_speed = _this select 1;
_limit = _this select 2;
//_ticket = 1500;set amount ticket
_ticket = (_speed - _limit) * 100;
diag_log format["Fine: $%1",[_ticket] call life_fnc_numberText];
diag_log format["Dr%1 :: Sp%2 :: Lmt%3",name _driver,round _speed,_limit];
player say3d "PhotoSound";
sleep 0.05;
"colorCorrections" ppEffectEnable true;   
"colorCorrections" ppEffectAdjust [1, 15, 0, [0.5, 0.5, 0.5, 0], [0.0, 0.5, 0.0, 1],[0.3, 0.3, 0.3, 0.05]];    
"colorCorrections" ppEffectCommit 0;  
sleep 0;   
"colorCorrections" ppEffectAdjust [1, 1, 0, [1, 1, 1, 0.0], [1, 1, 1, 1],  [1, 1, 1, 1]];    
"colorCorrections" ppEffectCommit 0.05;   
sleep 0.05;   
"colorCorrections" ppEffectEnable false;
sleep 0.1;
"colorCorrections" ppEffectEnable true;   
"colorCorrections" ppEffectAdjust [1, 15, 0, [0.5, 0.5, 0.5, 0], [0.0, 0.5, 0.0, 1],[0.3, 0.3, 0.3, 0.05]];    
"colorCorrections" ppEffectCommit 0;  
sleep 0;   
"colorCorrections" ppEffectAdjust [1, 1, 0, [1, 1, 1, 0.0], [1, 1, 1, 1],  [1, 1, 1, 1]];    
"colorCorrections" ppEffectCommit 0.05;   
sleep 0.05;   
"colorCorrections" ppEffectEnable false;

if(life_OnHandCash <= _ticket) then
{
	if(life_OnBankCash <= _ticket) exitWith 
	{
		hint parseText format ["<t color='#ffffff'><t size='2'><t align='center'>Speed Radar<br/><t color='#ff0000'><t align='center'><t size='1'>Speed: %1 km/h<br/><t color='#ffffff'><t align='center'><t size='1'>Speed Limit: %2 km/h<br/><t color='#ffffff'><t align='center'><t size='1'>Driver: %3<br/><t color='#ffffff'><t align='center'><t size='1'>Fine: $%4<br/>Since you are broke and cannot pay, you now have a warrant out for you instead",round _speed,_limit,name _driver,[_ticket] call life_fnc_numberText];
		[[getPlayerUID player,name player,"120S"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
	};
	life_OnBankCash = life_OnBankCash - _ticket;
	hint parseText format ["<t color='#ffffff'><t size='2'><t align='center'>Speed Radar<br/><t color='#ff0000'><t align='center'><t size='1'>Speed: %1 km/h<br/><t color='#ffffff'><t align='center'><t size='1'>Speed Limit: %2 km/h<br/><t color='#ffffff'><t align='center'><t size='1'>Driver: %3<br/><t color='#ffffff'><t align='center'><t size='1'>Fine: $%4",round _speed,_limit,name _driver,[_ticket] call life_fnc_numberText];
	diag_log "Ticket paid from player bank";
} else {
						
	life_OnHandCash = life_OnHandCash - _ticket;
	hint parseText format ["<t color='#ffffff'><t size='2'><t align='center'>Speed Radar<br/><t color='#ff0000'><t align='center'><t size='1'>Speed: %1 km/h<br/><t color='#ffffff'><t align='center'><t size='1'>Speed Limit: %2 km/h<br/><t color='#ffffff'><t align='center'><t size='1'>Driver: %3<br/><t color='#ffffff'><t align='center'><t size='1'>Fine: $%4",round _speed,_limit,name _driver,[_ticket] call life_fnc_numberText];	
	diag_log "Ticket paid from player cash";
};