#include <macro.h>
/*
	File: fn_initCop.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Cop Initialization file.
*/
private["_end"];
player addRating 9999999;
waitUntil {!(isNull (findDisplay 46))};
_end = false;
if(life_blacklisted) exitWith
{
	["Blacklisted",false,true] call BIS_fnc_endMission;
	sleep 30;
};

if((__GETC__(life_coplevel) < 1) && (__GETC__(life_adminlevel) < 1)) then {
		["NotWhitelisted",false,true] call BIS_fnc_endMission;
		sleep 35;
};
player setVariable["rank",(__GETC__(life_coplevel)),true];
switch(__GETC__(life_coplevel)) do
{
	case 1: {life_paycheck = life_paycheck + 100;};//cadet
	case 2: {life_paycheck = life_paycheck + 200;};//patrolman
	case 3: {life_paycheck = life_paycheck + 300;};//sergeant
	case 4: {life_paycheck = life_paycheck + 400;};//lt
	case 5: {life_paycheck = life_paycheck + 500;};//
	case 6: {life_paycheck = life_paycheck + 600;};
	case 7: {life_paycheck = life_paycheck + 700;};
};

[] call life_fnc_spawnMenu;
waitUntil{!isNull (findDisplay 38500)}; //Wait for the spawn selection to be open.
waitUntil{isNull (findDisplay 38500)}; //Wait for the spawn selection to be done.
[] spawn
{
	private["_texture"];
	while {true} do
	{
		waitUntil{uniform player == "U_Rangemaster"};
		_texture =
		switch (__GETC__(life_coplevel)) do
		{
			case 1: {"textures\cop\cadet_1.jpg"};
			case 2: {"textures\cop\patrol_1.jpg"};
			case 3: {"textures\cop\sergeant_1.jpg"};
			case 4: {"textures\cop\lt_1.jpg"};
			case 5: {"textures\cop\captain_1.jpg"};
			case 6: {"textures\cop\chief_1.jpg"};
			case 7: {"textures\cop\commissioner_1.jpg"};
		};
		
		player setObjectTextureGlobal [0,_texture];
		waitUntil{uniform player != "U_Rangemaster"};
	};
};
/*
[] spawn
{
while {true} do
	{
		waitUntil {uniform player == "U_Rangemaster"};
		player setObjectTextureGlobal [0,"textures\cop\police_shirt.paa"];
		waitUntil {uniform player != "U_Rangemaster"};
	
	};
};
[] spawn
{
while {true} do
	{
				
		waitUntil {uniform player == "U_BG_Guerilla2_3"};
		player setObjectTextureGlobal [0,"textures\cop\hwaypatrol_U_BG_Guerilla2_2_by_MrKraken.jpg"];
		waitUntil {uniform player != "U_BG_Guerilla2_3"};
		
		
	};
};
*/