private ["_animal","_nearAnimals","_roughly","_dir","_bearing","_comp","_distance","_msg","_licensed","_distanceBetween"];

if (player distance (getMarkerPos "hunting_zone") > 1000) exitWith {};
if (player distance life_lastAreaTracked < 50) exitWith {[2,"You have already searched around this area... move around and keep looking"] call life_fnc_broadcast;};
_licensed = 0;_addExpAmount = 5;
_data = missionNamespace getVariable "Hunting_Prof";
_trackDistance = 50;
if((_data select 0) > 0) then {_trackDistance = 50 + ((_data select 0) * 25);if(_trackDistance > 400) then {_trackDistance = 400;};};
life_lastAreaTracked = getPos player;
if(license_civ_hunting) then {_licensed = 1;};
_nearAnimals = nearestObjects[getPos player,["Animal"],_trackDistance];
//diag_log format ["Tracking nearestObjects: %1",count _nearAnimals];//DEBUG
if(count _nearAnimals <= 0) exitWith { [2,"Hmm, I don't see anything around here ... keep looking"] call life_fnc_broadcast;};



_time = ( 3 - (0.25 * (_data select 0)));
if((_data select 0) > 0) then {
		_val = _val * (_data select 0);
		_addExpAmount = _addExpAmount + round(2.5 * (_data select 0));
};
{if(player distance (getPos _x) < 100) then {_animal = _x;};} forEach _nearAnimals;
diag_log format ["Animal type for displayname: %1",_animal];
_displayName = "Unknown";
switch(typeOf _animal) do {
	case "Hen_random_F": {_displayName = "Chicken";};
	case "Cock_random_F": {_displayName = "Rooster";};
	case "Goat_random_F": {_displayName = "Goat";};
	case "Sheep_random_F": {_displayName = "Sheep";};
	case "Rabbit_F": {_displayName = "Rabbit";};
	case "Fin_random_F": {_displayName = "Dog";};//not used but why not
	case "Alsatian_Random_F": {_displayName = "Alsatian Dog";};//not used but why not
	default {_displayName = "Unknown";};
};
_distanceBetween = getPosASL _animal vectorDiff getPosASL player;
_dir = (_distanceBetween select 0) atan2 (_distanceBetween select 1);
if (_dir < 0) then {_dir = 360 + _dir}; 
_comp = ["North", "North east", "East", "South East", "South", "South West", "West", "North West", "North"];
_bearing = _comp select (round (_dir / 45));
if (_licensed > 0) then
{
	
	_roughly = floor (random 25);
	_addExpAmount = round(_addExpAmount + (_roughly / 2));
	_distance = floor ((player distance _animal) + _roughly);
	_msg = format["You've picked up the tracks of a %1 approximately %2 meters to the %3.", _displayName, _distance, _bearing];
	["Hunting_Prof",_addExpAmount] call life_fnc_addExp;
}
else
{
	_roughly = floor (random 50);
	_distance = floor ((player distance _animal) + _roughly);
	_msg = format["You've picked up the tracks of an unknown animal heading %2 roughly %1 meters", _distance, _bearing];
	["Hunting_Prof",_addExpAmount] call life_fnc_addExp;
};

[[0,2], _msg] call life_fnc_broadcast;