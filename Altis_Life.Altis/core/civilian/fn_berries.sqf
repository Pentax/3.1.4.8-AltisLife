private["_gather","_diff","_itemName","_val","_berriesZones","_zone"];
_berriesZones = ["berries_1","berries_2","berries_3","berries_4","berries_5","berries_6","berries_7","berries_8","berries_9","berries_10","berries_11","berries_12","berries_13","berries_14","berries_15","berries_16","berries_17","berries_18","berries_19","berries_20"];
_zone = "";
if(life_action_inUse) exitWith {}; 
if(life_action_gather) exitWith {}; 
//Find out what zone we're near
{
	if(player distance (getMarkerPos _x) < 15) exitWith {_zone = _x;};
} foreach _berriesZones;

if(_zone == "") exitWith {
	//hint localize "STR_NOTF_notNearResource";
	life_action_inUse = false;
	life_action_gather = false;
};

//Get the resource that will be gathered from the zone name...
/*
switch(true) do {
	case (_zone in ["berries_1","berries_2","berries_3","berries_4","berries_5","berries_6","berries_7","berries_8","berries_9","berries_10","berries_11","berries_12","berries_13","berries_14","berries_15","berries_16","berries_17","berries_18","berries_19","berries_20"]): {_gather = "berries"; _val = 3 + round(random 3);};
	default {""};
};*/
//gather check??
if(vehicle player != player) exitWith {};

_data = missionNamespace getVariable "Fruit_Prof";
_chance = round(random 1000);
_gather = "berries";
_val = 5 + round(random 10);
_addExpAmount = round(_val / 2);
_time = 3;
if((_data select 0) > 0) then {
			_addExpAmount = _addExpAmount * (_data select 0);
			_chance = _chance - ((_data select 0) * 100 );
			_time = ( 3 - (0.25 * (_data select 0)));
};
_diff = [_gather,_val,life_carryWeight,life_maxWeight] call life_fnc_calWeightDiff;
if(_diff == 0) exitWith {hint localize "STR_NOTF_InvFull"};
life_action_inUse = true;
life_action_gather = true;
for "_i" from 0 to 2 do
	{
		player playMove "AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon";
		waitUntil{animationState player != "AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon";};
		sleep _time;
	};

if (_chance < 600) then {
		
		if(([true,_gather,_diff] call life_fnc_handleInv)) then
		{
			_itemName = [([_gather,0] call life_fnc_varHandle)] call life_fnc_varToStr;
			titleText[format[localize "STR_NOTF_Gather_Success",_itemName,_diff],"PLAIN"];
			["Fruit_Prof",_addExpAmount] call life_fnc_addExp;
		};
} else {
	titleText ["You did not find anything in these bushes","PLAIN"];
};

life_action_inUse = false;
life_action_gather = false;
