private["_rawMeats","_rawMeatTypes","_amount"];
_rawMeats = 0;//set to 0 for meatcheck
_rawMeats = _rawMeats + life_inv_dog + life_inv_sheep + life_inv_goat + life_inv_hen + life_inv_rooster + life_inv_rabbit + life_inv_dog + life_inv_adog;//any meats plus the zero is more than zero
if (_rawMeats == 0) exitWith { hint localize "STR_NOTF_NoRawMeats"; };//but if zero, than no

[2, format["You begin cooking %1 raw meat.", _rawMeats]] call life_fnc_broadcast;

for "_i" from 1 to _rawMeats do
{
	//[[player,"AinvPknlMstpSnonWnonDnon_medic_2"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
	player switchMove "AinvPknlMstpSnonWnonDnon_medic_2";
	player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_2";
	sleep 2.3;
};

if (!alive player) exitWith {};

_rawMeatTypes = ["hen","dog","adog","sheep","goat","rooster","rabbit"];

{
	_amount = missionNamespace getVariable ([_x,0] call life_fnc_varHandle);
	if (_amount > 0) then
	{
		[false,_x,_amount] call life_fnc_handleInv;
		[true,format["%1C",_x],_amount] call life_fnc_handleInv;
	};
} forEach _rawMeatTypes;
_addExpAmount = _rawMeats * 5;
["Hunting_Prof",_addExpAmount] call life_fnc_addExp;
[2, format["You've successfully cooked %1 raw meat.", _rawMeats]] call life_fnc_broadcast;
[[0,format["You have earned %1 XP for cooking raw meat", _addExpAmount]],"life_fnc_broadcast",player,false] spawn life_fnc_MP;
/*
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Guts the animal?

private["_rawMeats","","","","","",];
life_interrupted = false;
if(life_inv_knife < 1) exitWith{hint"You need a knife to cut this animal";};
life_action_inUse = true;
_rawMeats = 0;//set to 0 for meatcheck
_rawMeats = _rawMeats + life_inv_dog + life_inv_adog + life_inv_sheep + life_inv_goat + life_inv_hen + life_inv_rooster + life_inv_rabbit;//any meats plus the zero is more than zero
if (_rawMeats == 0) exitWith { hint localize "STR_NOTF_NoRawMeats"; };
_rawMeatTypes = ["hen","dog","adog","sheep","goat","rooster","rabbit"];
_upp = format["Cooking %1 steaks",[_rawMeats] call life_fnc_numberText;];
//Setup our progress bar.
disableSerialization;
5 cutRsc ["life_progress","PLAIN"];
_ui = uiNameSpace getVariable "life_progress";
_progress = _ui displayCtrl 38201;
_pgText = _ui displayCtrl 38202;
_pgText ctrlSetText format["%2 (1%1)...","%",_upp];
_progress progressSetPosition 0.01;
_cP = 0.01;

while{true} do {
	if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
		[[player,"AinvPknlMstpSnonWnonDnon_medic_1"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
		player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
	};
	uiSleep 0.15;
	_cP = _cP + 0.01;
	_progress progressSetPosition _cP;
	_pgText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_upp];
	if(_cP >= 1) exitWith {};
	if(!alive player) exitWith {};
	if(player != vehicle player) exitWith {};
	if(life_interrupted) exitWith {};
};
		
life_action_inUse = false;
5 cutText ["","PLAIN"];
player playActionNow "stop";
if(isNull _animalCorpse) exitWith {life_action_inUse = false;};
if(life_interrupted) exitWith {life_interrupted = false; titleText[localize "STR_NOTF_ActionCancel","PLAIN"]; life_action_inUse = false;};
if(player != vehicle player) exitWith {titleText[localize "STR_NOTF_RepairingInVehicle","PLAIN"];};

{
	_amount = missionNamespace getVariable ([_x,0] call life_fnc_varHandle);
	if (_amount > 0) then
	{
		[false,_x,_amount] call life_fnc_handleInv;
		[true,format["%1C",_x],_amount] call life_fnc_handleInv;
	};
} forEach _rawMeatTypes;
titleText [format["You have cooked %1 raw meat",_displayName],"PLAIN"];

*/