private["_berries"];
_berries = ["berries_1","berries_2","berries_3","berries_4","berries_5","berries_6","berries_7","berries_8","berries_9","berries_10","berries_11","berries_12","berries_13","berries_14","berries_15","berries_16","berries_17","berries_18","berries_19","berries_20"];

//Create apple zones
{
	_zone = createTrigger ["EmptyDetector",(getMarkerPos _x)];
	_zone setTriggerArea[15,15,0,false];
	_zone setTriggerActivation["EAST","PRESENT",true];
	_zone setTriggerStatements["player in thislist","LIFE_Action_berries = player addAction[ ""Search for Berries"",life_fnc_berries,'',0,false,false,'','!life_action_inUse'];","player removeAction LIFE_Action_berries;"];
} foreach _berries;
