/*
	File: fn_jail.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Starts the initial process of jailing.
*/
private["_bad","_unit","_handle"];
_unit = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
hint format["%1", _unit];
if(isNull _unit) exitWith {}; //Dafuq?
if(_unit != player) exitWith {}; //Dafuq?
if(life_is_arrested) exitWith {}; //Dafuq i'm already arrested
_bad = [_this,1,false,[false]] call BIS_fnc_param;
player setVariable["restrained",false,true];
player setVariable["Escorting",false,true];
player setVariable["transporting",false,true];

titleText[localize "STR_Jail_Warn","PLAIN"];
hint localize "STR_Jail_LicenseNOTF";
player setPos (getMarkerPos "jail_marker" vectorAdd [0,0,0]);
_handle = [] spawn life_fnc_stripDownPlayer;
waitUntil {scriptDone _handle};
player forceAddUniform "U_Competitor";
[[player,0,"textures\prisoner.jpg"],"life_fnc_setTexture",true,false] spawn life_fnc_MP;

if(_bad) then
{
	waitUntil {alive player};
	sleep 1;
};

if(player distance (getMarkerPos "jail_marker") > 40) then
{
	//player setPos (getMarkerPos "jail_marker");
	player setPos (getMarkerPos "jail_marker" vectorAdd [0,0,0]);
	_handle = [] spawn life_fnc_stripDownPlayer;
	waitUntil {scriptDone _handle};
	player forceAddUniform "U_Competitor";
	[[player,0,"textures\prisoner.jpg"],"life_fnc_setTexture",true,false] spawn life_fnc_MP;
	//player setPos (getMarkerPos "jail_marker");
	//player setPos [getPos player select 0, getPos player select 1, (getPos player select 2) + 0.99];
	
};

[1] call life_fnc_removeLicenses;
if(life_inv_heroinu > 0) then {[false,"heroinu",life_inv_heroinu] call life_fnc_handleInv;};
if(life_inv_heroinp > 0) then {[false,"heroinp",life_inv_heroinp] call life_fnc_handleInv;};
if(life_inv_coke > 0) then {[false,"cocaine",life_inv_coke] call life_fnc_handleInv;};
if(life_inv_cokep > 0) then {[false,"cocainep",life_inv_cokep] call life_fnc_handleInv;};
if(life_inv_turtle > 0) then {[false,"turtle",life_inv_turtle] call life_fnc_handleInv;};
if(life_inv_cannabis > 0) then {[false,"cannabis",life_inv_cannabis] call life_fnc_handleInv;};
if(life_inv_marijuana > 0) then {[false,"bloodbag",life_inv_marijuana] call life_fnc_handleInv;};
if(life_inv_bloodbag > 0) then {[false,"bloodbag",life_inv_bloodbag] call life_fnc_handleInv;};
if(life_inv_ivkit > 0) then {[false,"ivkit",life_inv_ivkit] call life_fnc_handleInv;};
if(life_inv_scalpel > 0) then {[false,"scalpel",life_inv_scalpel] call life_fnc_handleInv;};
if(life_inv_organttk > 0) then {[false,"organttk",life_inv_organttk] call life_fnc_handleInv;};
if(life_inv_kidney > 0) then {[false,"kidney",life_inv_kidney] call life_fnc_handleInv;};
if(life_inv_goldbar > 0) then {[false,"kidney",life_inv_goldbar] call life_fnc_handleInv;};
if(life_inv_transporter > 0) then {[false,"transporter",life_inv_transporter] call life_fnc_handleInv;};
life_is_arrested = true;

[[player,_bad],"life_fnc_jailSys",false,false] spawn life_fnc_MP;
//[] call SOCK_fnc_updateRequest;
[5] call SOCK_fnc_updatePartial;