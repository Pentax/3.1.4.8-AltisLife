/*
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Guts the animal?
*/
private["_animalCorpse","_upp","_ui","_progress","_pgText","_cP","_displayName","_item","_steaks"];
_animalCorpse = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
if(isNull _animalCorpse) exitWith {}; //Object passed is null?
life_interrupted = false;
if(!((typeOf _animalCorpse) in ["Sheep_random_F","Goat_random_F","Hen_random_F","Cock_random_F","Fin_random_F","Alsatian_Random_F","Rabbit_F"])) exitWith {};
if(player distance _animalCorpse > 2.5) exitWith {};
if(life_inv_knife < 1) exitWith{hint"You need a knife to cut this animal";};
life_action_inUse = true;
 switch(typeOf _animalCorpse) do {
	case "Hen_random_F": {_displayName = "Chicken"; _item = "hen";_steaks = 2;};
	case "Cock_random_F": {_displayName = "Rooster"; _item = "rooster";_steaks = 2;};
	case "Goat_random_F": {_displayName = "Goat"; _item = "goat";_steaks = 2;};
	case "Sheep_random_F": {_displayName = "Sheep"; _item = "sheep";_steaks = 2;};
	case "Rabbit_F": {_displayName = "Rabbit"; _item = "rabbit";_steaks = 1;};
	case "Fin_random_F": {_displayName = "Dog"; _item = "dog";_steaks = 2;};
	case "Alsatian_Random_F": {_displayName = "Alsatian Dog"; _item = "adog";_steaks = 2;};
	default {_displayName = "Unknown"; _item = "";_steaks = 1;};
};

if(_displayName == "") exitWith {life_action_inUse = false;};
_m = 0.009;
_addExpAmount = 5 * _steaks;
_profName = [_item] call life_fnc_profType;
if( _profName != "" ) then {
		_data = missionNamespace getVariable (_profName);
		switch ( _data select 0 ) do {
			case 0: {_m = 0.009;};
			case 1: {_m = 0.0095;};
			case 2: {_m = 0.01;};
			case 3: {_m = 0.015;};
			case 4: {_m = 0.017;};
			case 5: {_m = 0.020;};
			case 6: {_m = 0.022;};
			case 7: {_m = 0.025;};
			case 8: {_m = 0.028;};
			case 9: {_m = 0.030;};
			case 10: {_m = 0.033;};
			case 11: {_m = 0.036;};
			case 12: {_m = 0.04;};
			case 13: {_m = 0.044;};
			case 14: {_m = 0.046;};
			case 15: {_m = 0.048;};
			case 16: {_m = 0.050;};
			case 17: {_m = 0.055;};
			case 18: {_m = 0.060;};
			case 19: {_m = 0.063;};
			case 20: {_m = 0.065;};
		};
		if((_data select 0) > 0) then {
				_steaks = _steaks * (_data select 0);
				if(_steaks > 8) then {_steaks = 8;};
		};
};
_upp = format["Skinning a %1",_displayName];
//Setup our progress bar.
disableSerialization;
5 cutRsc ["life_progress","PLAIN"];
_ui = uiNameSpace getVariable "life_progress";
_progress = _ui displayCtrl 38201;
_pgText = _ui displayCtrl 38202;
_pgText ctrlSetText format["%2 (1%1)...","%",_upp];
_progress progressSetPosition 0.01;
_cP = 0.05;

while{true} do {
	if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
		//[[player,"AinvPknlMstpSnonWnonDnon_medic_1"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
		player switchMove "AinvPknlMstpSnonWnonDnon_medic_1";
		player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
	};
	uiSleep 0.15;
	_cP = _cP + _m;
	_progress progressSetPosition _cP;
	_pgText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_upp];
	if(_cP >= 1) exitWith {};
	if(!alive player) exitWith {};
	if(isNull _animalCorpse) exitWith {};
	if(player != vehicle player) exitWith {};
	if(life_interrupted) exitWith {};
};
		
life_action_inUse = false;
5 cutText ["","PLAIN"];
player playActionNow "stop";
if(isNull _animalCorpse) exitWith {life_action_inUse = false;};
if(life_interrupted) exitWith {life_interrupted = false; titleText[localize "STR_NOTF_ActionCancel","PLAIN"]; life_action_inUse = false;};
if(player != vehicle player) exitWith {titleText[localize "STR_NOTF_RepairingInVehicle","PLAIN"];};

if(([true,_item,_steaks] call life_fnc_handleInv)) then {
	deleteVehicle _animalCorpse;
	titleText [format["You have collected %2 raw %1 meat steaks",_displayName,_steaks],"PLAIN"];
	[[0,format["You have earned %1 XP for skinning this animal", _addExpAmount]],"life_fnc_broadcast",player,false] spawn life_fnc_MP;
	[_profName,_addExpAmount] call life_fnc_addExp;
} else {
	titleText ["Your inventory is full","PLAIN"];
};