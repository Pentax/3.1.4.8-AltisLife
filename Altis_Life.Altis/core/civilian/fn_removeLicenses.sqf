/*
	File: fn_removeLicenses.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Used for stripping certain licenses off of civilians as punishment.
*/
private["_state"];
_state = [_this,0,1,[0]] call BIS_fnc_param;

switch (_state) do
{
	//Death while being wanted
	case 0:
	{
		license_civ_rebel = false;
		license_civ_driver = false;
		license_civ_bus = false;
		license_civ_heroin = false;
		license_civ_marijuana = false;
		license_civ_coke = false;
		license_civ_krok = false;
		license_civ_meth = false;
		license_civ_lawyer = false;
		license_civ_gun = false;
		license_civ_rifle = false;
		license_civ_gang = false;
		license_civ_truck = false;
		license_civ_diamond = false;
		license_civ_stiller = false;
		license_civ_liquor = false;
		license_civ_bottler = false;
	};
	
	//Jail licenses
	case 1:
	{
		license_civ_rebel = false;
		license_civ_heroin = false;
		license_civ_marijuana = false;
		license_civ_coke = false;
		license_civ_krok = false;
		license_civ_meth = false;
		license_civ_lawyer = false;
		license_civ_gun = false;
	};
	
	//Remove motor vehicle licenses
	case 2:
	{
		
		if(license_civ_driver OR license_civ_air OR license_civ_truck OR license_civ_boat OR license_civ_bus) then {
			if(isNil "life_vdmurders_count") then {life_vdmurders_count = 0;};
			life_vdmurders_count = life_vdmurders_count + 1;
			if(life_vdmurders_count >= 2) then {
				license_civ_driver = false;
				license_civ_bus = false;
				license_civ_truck = false;
				hint localize "STR_Civ_LicenseRemove_1";
				life_vdmurders_count = 0;
			};
		};
	};
	
	//Killing someone while owning a gun license
	case 3:
	{
		
		if(license_civ_gun) then {
			if(isNil "life_murders_count") then {life_murders_count = 0;};
			life_murders_count = life_murders_count + 1;
			if(life_murders_count >= 3) then {	
					license_civ_gun = false;
					license_civ_lawyer = false;
					hint localize "STR_Civ_LicenseRemove_2";
					life_murders_count = 0;
			};
			
		};
	};
	//ADDED by grimm, for removing licenses on radar script
	case 4:
	{
		if(license_civ_driver OR license_civ_air OR license_civ_truck OR license_civ_boat) then {
			license_civ_driver = false;
			license_civ_bus = false;
			license_civ_air = false;
			license_civ_truck = false;
			license_civ_boat = false;
			systemChat localize "STR_Civ_LicenseRemove_1a";
		};
	};
};