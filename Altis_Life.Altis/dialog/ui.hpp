	class playerHUD
   	{
		idd=-1;
		movingEnable=0;
	  	fadein=0;
		duration = 99999999999999999999999999999999999999999999;
	  	fadeout=0;
		name="playerHUD";
		onLoad="uiNamespace setVariable ['playerHUD',_this select 0]";
		objects[]={};
		
		class controlsBackground 
		{
			class foodHIcon : Life_RscText 
			{
			
				idc = -1;
				text = "HUNGER";
				x = safeZoneX+safeZoneW-0.14; y = safeZoneY+safeZoneH-0.20;
				w = 0.12; h = 0.12 * 4 / 3;
				colorBackground[] = {0,0,0,0};
				colorText[] = {1,0.616,0,1};
			};
			
			class waterHIcon : Life_RscText 
			{
			
				idc = -1;
				text = "THIRST";
				x = safeZoneX+safeZoneW-0.27; y = safeZoneY+safeZoneH-0.20;
				w = 0.12; h = 0.12 * 4 / 3;
				colorBackground[] = {0,0,0,0};
				colorText[] = {1,0.616,0,1};
			};
			
			class healthHIcon : Life_RscText
			{
				
				idc = -1;
				text = "HEALTH";
				x = safeZoneX+safeZoneW-0.40; y = safeZoneY+safeZoneH-0.20;
				w = 0.12; h = 0.12 * 4 / 3;
				colorBackground[] = {0,0,0,0};
				colorText[] = {1,0,0,1};
			};
			class ponlineHIcon : Life_RscText
			{
				
				idc = -1;
				text = "PLAYERS";
				x = safeZoneX+safeZoneW-0.53; y = safeZoneY+safeZoneH-0.20;
				w = 0.12; h = 0.12 * 4 / 3;
				colorBackground[] = {0,0,0,0};
				colorText[] = {1,0.616,0,1};
			};
			class cashHIcon : Life_RscText
			{
				
				idc = -1;
				text = "CASH";
				x = safeZoneX+safeZoneW-0.64; y = safeZoneY+safeZoneH-0.20;
				w = 0.12; h = 0.12 * 4 / 3;
				colorBackground[] = {0,0,0,0};
				colorText[] = {0,1,0,1};
			};
			class bankHIcon : Life_RscText
			{
				
				idc = -1;
				text = "BANK";
				x = safeZoneX+safeZoneW-0.77; y = safeZoneY+safeZoneH-0.20;
				w = 0.12; h = 0.12 * 4 / 3;
				colorBackground[] = {0,0,0,0};
				colorText[] = {0,1,0,1};
			};
		};
		
		class controls
		{
			class foodtext
			{
				type=0;
				idc=23500;
				style=1;
				x = -1;
				y = -1;
				w = 0.3;
				h = 0.05;
				sizeEx=0.042;
				size=1;
				font="PuristaLight";
				colorBackground[]={0,0,0,0};
				colorText[] = { 1 , 1 , 1 , 1 };
				shadow=true;
				text="";
			};
			
			class watertext
			{
				type=0;
				idc=23510;
				style=1;
				x=-1;
				y=-1;
				w=0.3;
				h=0.05;
				sizeEx=0.042;
				size=1;
				font="PuristaLight";
				colorBackground[]={0,0,0,0};
				colorText[] = { 1 , 1 , 1 , 1 };
				shadow=true;
				text="";
			};
			
			class healthtext
			{
				type=0;
				idc=23515;
				style=1;
				x=-1;
				y=-1;
				w=0.3;
				h=0.05;
				sizeEx=0.042;
				size=1;
				font="PuristaLight";
				colorBackground[]={0,0,0,0};
				colorText[] = { 1 , 1 , 1 , 1 };
				shadow=true;
				text="";
			};
			class cashtext
			{
				type=0;
				idc=23520;
				style=1;
				x=-1;
				y=-1;
				w=0.3;
				h=0.05;
				sizeEx=0.038;
				size=1;
				font="PuristaLight";
				colorBackground[]={0,0,0,0};
				colorText[] = { 1 , 1 , 1 , 1 };
				shadow=true;
				text="";
			};
			class banktext
			{
				type=0;
				idc=23525;
				style=1;
				x=-1;
				y=-1;
				w=0.3;
				h=0.05;
				sizeEx=0.038;
				size=1;
				font="PuristaLight";
				colorBackground[]={0,0,0,0};
				colorText[] = { 1 , 1 , 1 , 1 };
				shadow=true;
				text="";
			};
			class ponlinetext
			{
				type=0;
				idc=23530;
				style=1;
				x=-1;
				y=-1;
				w=0.3;
				h=0.05;
				sizeEx=0.042;
				size=1;
				font="PuristaLight";
				colorBackground[]={0,0,0,0};
				colorText[] = { 1 , 1 , 1 , 1 };
				shadow=true;
				text="";
			};
		};   
 	};