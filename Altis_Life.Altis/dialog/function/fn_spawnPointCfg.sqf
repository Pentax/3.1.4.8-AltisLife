#include <macro.h>
/*
	File: fn_spawnPointCfg.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master configuration for available spawn points depending on the units side.
	
	Return:
	[Spawn Marker,Spawn Name,Image Path]
*/
private["_side","_ret","_arrayPusta","_arrayBerezino","_randomPusta","_randomBerezino","_check","_randomZeleno","_randomRebel"];
_side = [_this,0,east,[east]] call BIS_fnc_param;
_ret = [];
_randomPusta = ["pusta_1_1", "pusta_1_2", "pusta_1_3", "pusta_1_4","pusta_1_5", "pusta_1_6", "pusta_1_7", "pusta_1_8","pusta_1_9", "pusta_1_10", "pusta_1_11", "pusta_1_12"] call BIS_fnc_selectRandom;
_randomBerezino = ["berezino_1_1", "berezino_1_2", "berezino_1_3", "berezino_1_4","berezino_1_5", "berezino_1_6", "berezino_1_7", "berezino_1_8","berezino_1_9", "berezino_1_10", "berezino_1_11", "berezino_1_12"] call BIS_fnc_selectRandom;
_randomZeleno = ["zeleno_1_1", "zeleno_1_2", "zeleno_1_3", "zeleno_1_4","zeleno_1_5", "zeleno_1_6", "zeleno_1_7", "zeleno_1_8"] call BIS_fnc_selectRandom;
_randomRebel = ["reb_spawn_1_1","reb_spawn_1_2","reb_spawn_1_3","reb_spawn_1_4","reb_spawn_1_5","reb_spawn_1_6","reb_spawn_1_7","reb_spawn_1_8","reb_spawn_1_9"] call BIS_fnc_selectRandom;
_randomRebelOp = ["rebelOp_1","rebelOp_2","rebelOp","rebelOp_4","rebelOp_5","rebelOp_6"] call BIS_fnc_selectRandom;
_randomSupporter = ["supporter1_1","supporter1_2","supporter1_3","supporter1_4","supporter1_5","supporter1_6","supporter1_7","supporter1"] call BIS_fnc_selectRandom;

_check = 0;
//Spawn Marker, Spawn Name, PathToImage
switch (_side) do
{
	case west:{_ret pushBack ["cop_spawn_2","Kavala",nil];_ret pushBack ["cop_spawn_3","Athira",nil];_ret pushBack ["cop_spawn_5","Zaros",nil];};
	
	case independent: {_ret pushBack ["medic_spawn_1","North Hospital",nil];_ret pushBack ["medic_spawn_3","Eastern Regional",nil];};
	
	case east:
	{
			if(!license_civ_rebel  && _check == 0) then {_ret pushBack [_randomZeleno,"Kavala",nil];_ret pushBack [_randomBerezino,"Athira",nil];_ret pushBack [_randomPusta,"Zaros",nil];};
			 if(license_civ_rebel  && _check == 0) then {_ret pushBack [_randomRebel,"Rebel Base",nil];_ret pushBack [_randomRebelOp,"Rebel OP",nil];};//_ret pushBack ["reb_spawn_4","Rebel Isle",nil];
			//if(__GETC__(life_adminlevel) > 0) then {_ret pushBack ["grimm_spawn_2","Admin Base",nil];};
			if(__GETC__(life_donator) > 0 && _check == 0) then { _ret pushBack [_randomSupporter,"Supporter Spawn",nil];};
	};
};
_ret;