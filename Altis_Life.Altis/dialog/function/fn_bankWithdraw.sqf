/*
	COPY PASTE TIME
*/
private["_val"];
_val = parseNumber(ctrlText 2702);
if(_val > 999999) exitWith {hint localize "STR_ATM_WithdrawMax";};
if(_val < 0) exitwith {};
if(!([str(_val)] call life_fnc_isnumeric)) exitWith {hint localize "STR_ATM_notnumeric"};
if(_val > life_OnBankCash) exitWith {hint localize "STR_ATM_NotEnoughFunds"};
if(_val < 100 && life_OnBankCash > 20000000) exitWith {hint localize "STR_ATM_WithdrawMin"}; //Temp fix for something.
if(isNil "life_grimmwithd_inUse") then {life_grimmwithd_inUse = time-11;};
if(life_grimmwithd_inUse+(10) >= time) exitWith {
hint format["You can use this ATM in %1 seconds",9 - round (time - life_grimmwithd_inUse - (floor ((time - life_grimmwithd_inUse) / 60)) * 60)];};
life_OnHandCash =life_OnHandCash + _val;
life_OnBankCash = life_OnBankCash - _val;
hint format [localize "STR_ATM_WithdrawSuccess",[_val] call life_fnc_numberText];
[] call life_fnc_atmMenu;
[6] call SOCK_fnc_updatePartial;
life_grimmwithd_inUse = time;
//[] call SOCK_fnc_updateRequest; //Silent Sync