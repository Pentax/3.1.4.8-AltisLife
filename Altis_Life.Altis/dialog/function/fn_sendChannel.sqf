#include <macro.h>

private["_message"];
disableSerialization;

waitUntil {!isnull (findDisplay 9000)};

if (__GETC__(life_adminlevel) <= 2 ) exitWith { hint "You are not a proper rank admin for this feature"; }; // Hint if not civilian
//if !( life_channel_send ) exitWith { systemChat "Wait ten minutes before sending another message please"; }; //Gives Player the Hint to Wait 10 mins


_message = ctrlText 9001;
[[4,format ["%1 reporting in from Channel 7 News: %2",name player,_message]],"life_fnc_broadcast",true,false] call life_fnc_MP;
