class Socket_Reciever
{
	tag = "SOCK";
	class SQL_Socket
	{
		file = "core\session";
		class requestReceived {};
		class dataQuery {};
		class insertPlayerInfo {};
		class updateRequest {};
		class syncData {};
		class updatePartial {};
	};
};

class SpyGlass
{
	tag = "SPY";
	class Functions
	{
		file = "SpyGlass";
		class cookieJar{};
		class notifyAdmins{};
		class observe{};
		class initSpy {preInit=1;};
	};
};

class Life_Client_Core
{
	tag = "life";
	
	class Master_Directory
	{
		file = "core";
		class setupActions {};
		class setupEVH {};
		class initCiv {};
		class initCop {};
		class initMedic {};
		class welcomeNotification {};
	};
	
	class Admin
	{
		file = "core\admin";
		class admininfo {};
		class adminid {};
		class admingetID {};
		class adminMenu {};
		class adminQuery {};
		class adminSpectate {};
		class adminTeleport {};
		class adminTpHere {};
		class adminUnrestrain {};
		class adminRestrain {};
		class tpToPlayer {};
		class adminDeleteVehicle {};
		class adminDebugCon {};
		class adminCompensate {};
		class adminGodMode {};
		class adminFreeze {};
		class adminMarkers {};
		class adminATM {};
		class adminRevive {};
		class adminStealth {};
		class adminTpInFront {};
		class adminRide {};
		class adminFlyRide {};
	};
	
	class Medical_System
	{
		file = "core\medical";
		class onPlayerKilled {};
		class onPlayerRespawn {};
		class respawned {};
		class revivePlayer {};
		class revived {};
		class medicMarkers {};
		class requestMedic {};
		class medicRequest {};
		class deathScreen {};
		class medicLoadout {};
		class medicSirenLights {};
		class medicLights {};
		class medicSiren {};
		class medicSiren2 {};
	};
	
	class Actions
	{
		file = "core\actions";
		class buyLicense {};
		class healHospital {};
		class pushVehicle {};
		class repairTruck {};
		class serviceChopper {};
		class catchFish {};
		class catchTurtle {};
		class dpFinish {};
		class dropFishingNet {};
		class getDPMission {};
		class postBail {};
		class processAction {};
		class suicideBomb {};
		class arrestAction {};
		class escortAction {};
		class impoundAction {};
		class pulloutAction {};
		class putInCar {};
		class stopEscorting {};
		class restrainAction {};
		class searchAction {};
		class searchVehAction {};
		class unrestrain {};
		class robShops {};
		class robShops2 {};
		class shopState {};
		class pickupItem {};
		class pickupItem1 {};
		class pickupMoney {};
		class ticketAction {};
		class packupSpikes {};
		class storeVehicle {};
		class robAction {};
		class removeWeaponAction {};
		class surrender{};
		class captureHideout {};
		class captureHideout1 {};
		class captureHideout2 {};
		class captureHideout3 {};
		class captureHideout4 {};
		class bj {};
		class gather {};
		class bombDetect {};
		class packupCampfire {};
		class packupTent {};
	};
	
	class Config
	{
		file = "core\config";
		class licensePrice {};
		class varHandle {};
		class varToStr {};
		class profType {};
	};

	class Player_Menu
	{
		file = "core\pmenu";
		class wantedList {};
		class wantedInfo {};
		class wantedMenu {};
		class wantedAddP {};
		class pardon {};
		class giveItem {};
		class giveMoney {};
		class p_openMenu {};
		class p_updateMenu {};
		class removeItem {};
		class useItem {};
		class cellphone {};
		class keyMenu {};
		class keyGive {};
		class keyDrop {};
		class s_onSliderChange {};
		class updateViewDistance {};
		class settingsMenu {};
		class settingsInit {};
		class AAN {};
		class smartphone {};
		class newMsg {};
		class showMsg {};
	};
	
	class Functions
	{
		file = "core\functions";
		class calWeightDiff {};
		class fetchCfgDetails {};
		class handleInv {};
		class hudSetup {};
		class hudUpdate {};
		class tazeSound {};
		class animSync {};
		class simDisable {};
		class keyHandler {};
		class dropItems {};
		class handleDamage {};
		class numberText {};
		class handleItem {};
		class accType {};
		class receiveItem {};
		class giveDiff {};
		class receiveMoney {};
		class playerTags {};
		class clearVehicleAmmo {};
		class pulloutVeh {};
		class nearUnits {};
		class actionKeyHandler {};
		class playerCount {};
		class fetchDeadGear {};
		class loadDeadGear {};
		class isnumeric {};
		class escInterupt {};
		class onTakeItem {};
		class fetchVehInfo {};
		class pushObject {};
		class autoSave {};
		class failedRobbery {};
		class failedRobbery2 {};
		class onFired {};
		class revealObjects {};
		class isUIDActive {};
		class inventoryOpened {};
		class imperialloadout {};
		class pressLoadout {};
		class underwaterLoadout {};
		class skyDive {};
		class saveGear {};
		class loadGear {};
		class stripDownPlayer {};
		class toCallThisOnADialog {};
		class randomWeather {};
		class unFlip {};
		class zoneLeave {};
		class shoutSpeech {};
		class paytheman {};
		class teleTaxi {};
		class debugMonitor {};
		class policeSiren {};
		class policeSiren2 {};
		class policeLights {};
		class profSetup {};
		class addExp {};
		class grabSkills {};
		
	};
	
	class Network
	{
		file = "core\functions\network";
		class addAction {};
		class broadcast {};
		class MP {};
		class MPexec {};
		class netSetVar {};
		class corpse {};
		class jumpFnc {};
		class soundDevice {};
		class setFuel {};
		class setTexture {};
		class say3D {};
	};
	
	class Civilian
	{
		file = "core\civilian";
		class jailMe {};
		class jail {};
		class tazed {};
		class knockedOut {};
		class knockoutAction {};
		class robReceive {};
		class robPerson {};
		class removeLicenses {};
		class removeWeapons {};
		class civInteractionMenu {};
		class demoChargeTimer {};
		class teleTimer {};
		class waterChargeTimer {};
		class helicrashTimer {};
		class manChargeTimer {};
		class freezePlayer {};
		class teleportPlayer {};
		class civLoadout {};
		class cookRawMeat {};
		class gutAnimal {};
		class trackAnimal {};
		class newZoneCreator {};
		class berries {};
		class prisonWallTimer {};
	};
	
	class Vehicle
	{
		file = "core\vehicle";
		class colorVehicle {};
		class openInventory {};
		class lockVehicle {};
		class vehicleOwners {};
		class vehStoreItem {};
		class vehTakeItem {};
		class vehInventory {};
		class vInteractionMenu {};
		class vehicleWeight {};
		class deviceMine {};
		class addVehicle2Chain {};
		class radarFlashnSound {};
	};
	
	class Cop
	{
		file = "core\cop";
		class copMarkers {};
		class copLights {};
		class copOpener {};
		class vehInvSearch {};
		class copSearch {};
		class bountyReceive {};
		class searchClient {};
		class restrain {};
		class ticketGive {};
		class ticketPay {};
		class ticketPrompt {};
		class copSiren {};
		class copSiren2 {};
		class spikeStripEffect {};
		class radar {};
		class questionDealer {};
		class copInteractionMenu {};
		class sirenLights {};
		class licenseCheck {};
		class licensesRead {};
		class seizeObjects {};
		class seizePlayerWeapon {};
		class seizePlayerWeaponAction {};
		class repairDoor {};
		class doorAnimate {};
		class fedCamDisplay {};
		class breathalyzer {};
		class copLoadout {};
		class ticketPaid {};
		class wantedGrab {};
		class radarCam {};
		class speedTicket {};
		class cop_skin {};
	};
	
	class Gangs
	{
		file = "core\gangs";
		class initGang {};
		class createGang {};
		class gangCreated {};
		class gangMenu {};
		class gangKick {};
		class gangLeave {};
		class gangNewLeader {};
		class gangUpgrade {};
		class gangInvitePlayer {};
		class gangInvite {};
		class gangDisband {};
		class gangDisbanded {};
	};
	
	class Shops
	{
		file = "core\shops";
		class atmMenu {};
		class buyClothes {};
		class changeClothes {};
		class clothingMenu {};
		class clothingFilter {};
		class vehicleShopMenu {};
		class vehicleShopLBChange {};
		class vehicleShopBuy {};
		class weaponShopFilter {};
		class weaponShopMenu {};
		class weaponShopSelection {};
		class weaponShopBuySell {};
		class virt_buy {};
		class virt_menu {};
		class virt_update {};
		class virt_sell {};
		class chopShopMenu {};
		class chopShopSelection {};
		class chopShopSell {};
		class blackjack {};
		class BJbet {};
		class BJhit {};
		class BJstay {};
		class slotmachine {};
		class slotSpin {};
	};
	
	class Items
	{
		file = "core\items";
		class pickaxeUse {};
		class lockpick {};
		class spikeStrip {};
		class jerryRefuel {};
		class weed {};
		class meth {};
		class krok {};
		class heroine {};
		class flashbang {};
		class boltcutter {};
		class blastingCharge {};
		class defuseKit {};
		class drinkbeer {};
		class drinkmoonshine {};
		class drinkwhiskey {};
		class underwaterCharge {};
		class demolitionCharge {};
		class bombDefuseKit {};
		class campfire {};
		class tent{};
		class wallCharge {};
	};
	class Dialog_Controls
	{
		file = "dialog\function";
		class setMapPosition {};
		class displayHandler {};
		class spawnConfirm {};
		class spawnMenu {};
		class spawnPointCfg {};
		class spawnPointSelected {};
		class progressBar {};
		class impoundMenu {};
		class unimpound {};
		class sellGarage {};
		class bankDeposit {};
		class bankWithdraw {};
		class bankTransfer {};
		class garageLBChange {};
		class safeInventory {};
		class safeOpen {};
		class safeTake {};
		class safeFix {};
		class gangDeposit {};
		class calldialog {};
		class sendChannel {};
		class safeInvGold {};
		class safeGold {};
		class goldTake {};
		class safeHeliGold {};
		class safeInvHeliGold {};
		class heligoldTake {};
		class pickupItems {};
		class prisonFix {};
		class cellPhoneCheck {};
	};
	class Cam
	{
	   file ="core";
	   class IntroCam {};
	};
	
};